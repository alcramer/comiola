# README #

COMIOLA is a animation tool for turning comics and other graphic art
into .mp4 videos.

### Installation ###
To download Comiola, go this url:

		https://bitbucket.org/alcramer/comiola/src/main


#### Windows ####
Click Downloads in the pane to the left and download the windows
installer.
It will be named "comiola_win_install_1.0.2".
When the download is complete, double click the downloaded file
to install Comiola. 

Security note: unless you've configured your machine to run unrecognized
apps, double clicking the installation file will cause a warning
dialog to appear:

		Microsoft Defender SmartScreen prevented an unrecogizied app
		from starting. Running this app might put your PC at risk.

Click the "More info" link below the warning message, then click
"Run anyway".

#### Mac ####
On Mac, Comiola runs as a Python script, 
so you should install the lastest version of Python 3 on your
machine.
If you need to install it, here's a 
[link to python.org](python.org/downloads). 

Once you've installed Python 3, open a terminal and enter the command  

		python3 -V

to verify that you've installed python3.

Next, click Downloads in the pane to the left and download the 
file "install_scripts.py". Move that file to your home folder, 
and enter the command

		python3 install_scripts.py

Comiola is now installed. To start the app, enter the command

		python3 comiola

Tip: that command only works if (in Terminal) you're in 
your home folder. To start the app from some other folder,
type  
		python3 ~/comiola

#### Linux etc. ####
If your machine can run Python scripts, you can run Comiola: just
follow the MAC instructions. Depending on your configuration, you 
may have to type "python" or "py" (rather than "python3") to invoke
python.


#### Dev Notes: Windows Installer ####
We use "pyinstaller" to create a standalone app for Windows,
and "Inno Setup"  
[download here](https://inno-setup.en.uptodown.com/windows)
to create the installer program. Steps are:

1. In ./comiola, run pyinstaller: "pyinstaller wincomiola.spec".
This writes the standalone executable "./comiola/dist/comiola".

2. From root directory ("."), double click the Inno Setup script 
"innosetup.iss". That will bring up Inno Setup. From top menu,
select Build/Compile. That will create the installer in 
"Output/comiola_win0.1.exe". 

3. If you go the project's Download page on BitBucket, and try to upload
the installer using the Add files button, it will timeout (file is too 
large). So instead we upload using BB's Rest Api. If you're working in
Windows, an easy way to this is to run Insomnia for windows:
[download here](insomnia.rest.download). Create a new request with the URI
"https://api.bitbucket.org/2.0/repositories/alcramer/comiola/downloads".
The request should be a POST, multipart. In the Insomnia GUI, type in
"files" where it says "New name", then select the installer file from 
the Choose File dialog.  
Note: in the "Auth" section of Insomnia GUI, select "Basic". For "username",
it's unclear exactly what bitbucket's new app password scheme wants (my
user name definitely works). For the app password, contact me.



### Who do I talk to? ###

Al Cramer ac2.71828@gmail.com
