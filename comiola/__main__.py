# Copyright Al Cramer 2021 ac2.71828@gmail.com
import tkinter as tk
import os
import sys

if getattr(sys, 'frozen', False):
    app_path = os.path.dirname(sys.executable)
elif __file__:
    app_path = os.path.dirname(__file__)
sys.path.append(app_path)

import scripts
import controls
import animate
import resources
from tkalt import *

import uiglobals
import display

# ui components
import uibanner
import uimenu
import uishots
import uitext
import uisequences
import uilighting
import uitimeline
import uiimgedit
import uiclips

# list of above
ui_components = [uibanner,uimenu,uishots,uitext,
    uisequences,uilighting,uitimeline,uiimgedit,uiclips]

def can_close_window():
    if scripts.script_changed():
        action  = askyesnocancel('Save project?')
        if action is None:
            return False
        if action:
            if do_commit():
                scripts.save_script(scripts.proj_name)
                return True
            else:
                return False
    return True

# View management

cur_view_id = 'display'
def get_cur_view_id():
    return cur_view_id

def show_view(name,fn=''):
    global cur_view_id
    cur_view_id = name
    ui.topviewcntrl.show(name)
    if name == 'imgedit':
        uiimgedit.init()
    else:
        validate_view()

cur_tab_id = None
def get_cur_tab_id():
    return cur_tab_id

def show_tab(name):
    ui.tabcntrl.show(name)

def validate_view():
    for uic in ui_components:
        if hasattr(uic,'validate_view'):
            uic.validate_view()
    display.draw_edit()

def do_commit():
    if scripts.script_open():
        for uic in ui_components:
            if hasattr(uic,'do_commit'):
                if not uic.do_commit():
                    return False
    return True

def on_tab_select(name):
    # called when used selects a tab: return False to cancel
    # the event
    global cur_tab_id
    if not do_commit():
        return False
    # unset selections as needed
    if name == 'Shots':
        display.sel_te = None
    elif name == 'Text':
        display.sel_ani = None
        display.sel_pt = None
    elif name == 'Cut/Copy/Paste':
        display.sel_te = None
        display.sel_ani = None
        display.sel_pt = None
    cur_tab_id = name
    validate_view()
    return True

# This dictionary gives the colors used in various parts of
# the ui.
#col_cntrl_fg = '#DEDEDE'
col_cntrl_fg = '#EDEDED'
col_cntrl_bg = '#303030'
col_display_bg = '#454545'
col_tabbar = '#757575'
col_label_fg = '#959595'
col_cntrl_bg_light = '#CFCFCF'
col_select = '#919191'

styles = {
    'menu': {
        'bg':'black',
        'fg':'white',
        'fg.label':'white',
        'bg.entry':'white',
        'fg.entry':'black'
    },
    'aux': {
        'bg':col_cntrl_bg_light,
        'fg':'black',
        'bg.canvas':'#757575',
        'bg.entry':'white',
        'fg.radiobutton':'black',
        'bg.radiobutton':'white',
        },
    'timeline': {
        'bg.canvas':col_display_bg,
        'fg':col_cntrl_fg
    },
    'colorcontrol': {
        'bg':col_cntrl_bg_light,
        'fg':'black',
    },
    '*': {
        'bg':col_cntrl_bg,
        'fg':col_cntrl_fg,
        'border':False,
        'bg.tabbar':col_tabbar,
        'bg.canvas':col_display_bg,
        'bg.entry':'white',
        'fg.entry':'black',
        'bg.textbox':'white',
        'fg.textbox':'black',
        'fg.label':col_label_fg,
        'fg.hr':'#bebebe',
        'border.optionmenu':True,
        'bg.colorcontrol':col_cntrl_bg_light,
        'fg.colorcontrol':'black',
        # start dev code: enable this when creating demo videos
        #'fontsize':14,
        # end dev code
    },
}

platform = sys.platform.lower()
if platform == 'darwin':
    fnicon = 'comiola.icns'
elif platform == 'linux':
    fnicon = 'comicon.png'
else:
    # windows
    fnicon = 'comiola.ico'
fpicon = os.path.join(resources.get_assets_dir('res'),fnicon)
tkalt_init('Comiola %s' % scripts.version,
        styles,can_close_window,fpicon)

def make_gui():
    Layout(None,None,w=1000,h=600,
        N = Frame('banner','menu',uibanner.get_ui),
        C = ViewSet('topviewcntrl',None,None,
            Layout('main',None,
                N = Frame('menu','menu',uimenu.get_ui),
                E = ViewSet('tabcntrl',None,
                    ["Shots","Text","Lighting","Cut/Copy/Paste"],
                    Frame('shots',None,uishots.get_ui),
                    Frame('text',None,uitext.get_ui),
                    Frame('lighting',None,uilighting.get_ui),
                    Frame('sequences',None,uisequences.get_ui),
                ),
                C = Layout(None,None,
                    N = Canvas('can_tl','timeline',None,16,weight=0,
                        on_mousedown = uitimeline.on_mousedown,
                        on_mouseup = uitimeline.on_mouseup,
                        on_mousemove = uitimeline.on_mousemove,
                    ),
                    C = Canvas('can_main',None,weight=1,
                        on_mousedown = display.on_mousedown,
                        on_mouseup = display.on_mouseup,
                        on_mousemove = display.on_mousemove,
                        on_doubleclick = display.on_doubleclick,
                    )
                )
            ),
            Layout('imgedit','aux',
                W = Frame(None,None,uiimgedit.get_ui),
                C = Layout(None,None,
                    N = Frame(None,None,uiimgedit.get_ui_menu),
                    C = Canvas('can_imgedit',None,weight=1,
                        on_mousedown = uiimgedit.on_mousedown,
                        on_mouseup = uiimgedit.on_mouseup,
                        on_mousemove = uiimgedit.on_mousemove,
                        )
                    )
                ),
            Layout('clipsedit','aux',
                W = Frame('frame_clipsmerge',None,uiclips.get_ui),
                C = Layout(None,None,
                    N = Frame('frame_clipsedit',None,uiclips.get_ui_menu),
                    C = Canvas('can_clipsedit',None,weight=1)
                ),
            )
        )
        ).make()

    # set canvases
    display.can = ui.can_main
    uiimgedit.can = ui.can_imgedit
    #ui.can_imgedit['cursor'] = 'crosshair'
    uitimeline.can = ui.can_tl

    # set event handlers.
    ui.tabcntrl.can_show = on_tab_select
    display.sel_change_handlers.append(uimenu.on_sel_change)

    # set the globals
    uiglobals.do_commit = do_commit
    uiglobals.validate_view = validate_view
    uiglobals.show_tab = show_tab
    uiglobals.show_view = show_view
    uiglobals.get_cur_view_id = get_cur_view_id
    uiglobals.get_cur_tab_id = get_cur_tab_id

usage = \
"""
usage:
    python __main__.py [project_filepath_option]
"""

def main():
    global styles
    if len(sys.argv) > 2:
        print('%s\n%s' % ('unrecognized arguments',usage))
        exit(1)
    make_gui()
    if len(sys.argv) == 2:
        if sys.argv[1] == '-dev':
            fp = 'C:/Users/Al/tmp/test.cprj'
            show_view('imgedit')
        else:
            fp = sys.argv[1]
        scripts.open_project(*os.path.split(fp),False)
        ui.proj_dir.set(scripts.proj_filepath)
        display.edit_shot(0)
    validate_view()
    mainloop()

if __name__ == '__main__':
    main()

