# Copyright Al Cramer 2021 ac2.71828@gmail.com
import os
from PIL import ImageTk,Image,ImageDraw,ImageFont,ImageOps,ImageFilter
import imageio
import numpy
import images
import io
import math
import time
import webbrowser
import random
import resources as res
import scripts
from scripts import get_shot,cnt_shots,Pt
import xfrmpersp
import fxjiggle

class Tween:
    # A tween gives size (w,h) and position (x,y) for an
    # animation element. This can be the camera, a sprite,
    # or a text element. If a sprite, the "img_pil" attribute
    # gives the loaded image. "prv_img_pil" is is the previous
    # image: we mix the two to get better animation.
    #
    # Constructor wants:
    # ani -- the animation 
    # nframes_shot -- number of frames in the shot
    # cycles -- number of cycles for the animation
    def __init__(self,ani,nframes_shot,cycles):
        self.ani = ani
        self.cycles = cycles
        # ixfrmS: frame index at which animation starts
        # ixfrmE: frame index at which animation end
        self.ixfrmS = int(ani.tS * nframes_shot)
        self.ixfrmE = int(ani.tE * nframes_shot)

        self.img_pil = None
        self.prv_img_pil = None

        # set for frame0, then save initial values for (x,y): 
        # needed for tracking
        self.update(0)
        self.xS = self.x
        self.yS = self.y

    def update(self,ixframe):
        ani = self.ani
        if ixframe == 0:
            self.ixcell = 0
            # Create a jiggle effect if needed
            self.fxjig = None
            params = ani.fxdct.get('jiggle')
            if params is not None:
                self.fxjig = fxjiggle.FXjiggle(
                    params['speed'],
                    params['amount'],
                    params['smear'])

        # reset (x,y,w,h) for the image (or text element) 
        # as needed.
        if ixframe > 0 and len(ani.path) == 1:
            # no update needed if this is a static element
            if ani.kind == 'txt' or ani.kind == 'cam':
                return
            if (ani.kind == 'spr' and
                    len(ani.fnlst) == 1 and 
                    self.fxjig is None):
                return

        # get parameter corresponding to "ixframe"
        param = 0.0
        ixfrmS = self.ixfrmS
        ixfrmE = self.ixfrmE
        if ixframe <= ixfrmS or ixfrmE == ixfrmS:
            param = 0.0
        elif ixframe >= ixfrmE:
            param = 1.0
        else:
            nfrms_ani = ixfrmE - ixfrmS + 1
            nfrms_cycle = int(.5 + nfrms_ani/self.cycles)
            param = ((ixframe-ixfrmS) % nfrms_cycle)/float(nfrms_cycle)
            param = max(0,param)
            param = min(1.0,param)
        # For test/dev purposes: see unit-test
        self._param = param

        # get pt, interpolated for "param"
        pt = ani.interpolate_pt(param)
        self.z = pt.z
        whalf = pt.w/2
        hhalf = pt.h/2
        self.x = int(.5 + pt.x - whalf)
        self.y = int(.5 + pt.y - hhalf)
        self.w = int(pt.w)
        self.h = int(pt.h)
        if ani.kind == 'spr':
            # Bookkeeping for jiggle: did we change the cell
            # in cell animation?
            changed_cell = False
            # get file name for image. authoring tools allow user
            # to explicity set the image file, in which case it's in
            # "pt.fnspr". Otherwise it's an element of ani.fnlst
            fnspr = pt.fnspr
            ncells = len(self.ani.fnlst)
            if fnspr == '':
                if ixframe <= self.ixfrmS:
                    ix = 0
                elif ixframe >= self.ixfrmE:
                    ix = ncells-1 
                else:
                    ix = int( (ixframe-self.ixfrmS) /
                            self.ani.frames_per_cell) % ncells
                fnspr = ani.fnlst[ix]
                # save current image if we advanced cell index;
                # note that we reset
                if self.ixcell != ix:
                    self.prv_img_pil = self.img_pil
                    changed_cell = True
                self.ixcell = ix
            # get image (xfrm as needed)
            try:
                (w,h) = (pt.w,pt.h)
                if pt.has_pxfrm():
                    img = res.get_img_pxfrm(fnspr,
                        pt.pxfrm_w, pt.pxfrm_h, pt.pxfrm_fall, h)
                else:
                    #img = res.get_img(fnspr).resize(
                            #(int(self.w),int(self.h)),Image.ANTIALIAS)
                    img = res.get_img_spr(fnspr,self.w,self.h)
                if pt.flip == 1:
                    img = ImageOps.mirror(img)
                if pt.rot != 0.0:
                    img = img.rotate(pt.rot,expand=True)
                # transforms may change image dims: tweak x and y
                (w,h) = img.size
                self.x -= int( (w - self.w)/2 )
                self.y -= int( (h - self.h)/2 )
                # Apply jiggle as needed. Must init if we changed cell; or if 
                # this is frame0
                if self.fxjig is not None:
                    img = self.fxjig.add_jiggle(img,
                            must_init=changed_cell or ixframe==0)
                self.img_pil = img
            except FileNotFoundError:
                self.img_pil = None

            # Do we blend image with previous image?
            # "blend_param" is the opacity (0..1) for previous
            # image. 0 means no trace of prv_img; .25 means opacity
            # of prv_img is .25.
            # Note: here we just compute the param; the actual blend is
            # done when we paste the image onto the bg.
            self.blend_param = 0
            if ani.reverb > 0 and self.prv_img_pil is not None:
                # "reverb" is number of frames in which both images
                # are present.
                # "nreps" is number of times we've shown img_pil
                fpc = ani.frames_per_cell
                nreps = ixframe % fpc
                cur_img_opacity = (nreps + .5)/ani.reverb
                self.blend_param = max(0, 1.0 - cur_img_opacity)

    def close_images(self):
        if self.prv_img_pil is not None:
            self.prv_img_pil.close()
        if self.img_pil is not None:
            self.img_pil.close()

    def track(self,targ):
        # reset (x,y) so that we track "targ"
        self.x = self.xS + (targ.x - targ.xS)
        self.y = self.yS + (targ.y - targ.yS)

class FXfade:
    # the fade effect
    def __init__(self,state,secs,fps):
        self.state = state
        self.nframes = int(secs*fps)
        # want minimum of 1 frame
        self.nframes = max(1,self.nframes)
        self.ixframe = 0
        s = get_shot(state.ixshot)
        self.img = get_frame_pil(state,s,s.nframes-1)
        s = get_shot(state.ixshot+1)
        self.bg = get_frame_pil(state,s,0).convert('RGB')

    def advance(self):
        if self.ixframe >= self.nframes:
            return False
        self.ixframe += 1
        alpha = 255.0 * (self.nframes - self.ixframe)/float(self.nframes)
        tmp = self.img.copy()
        tmp.putalpha(int(alpha))
        bg = self.bg.copy()
        bg.paste(tmp,(0,0),mask=tmp)
        self.state.img_pil = bg.convert('RGBA')
        return True

class AniState:
    # state of animation
    def __init__(self,can,ixS,ixE,fps,on_animate_fini):
        self.can = can
        self.ixshot = ixS
        self.ixE = ixE
        self.fps = fps
        self.msecs_frame = int( 1000.0/fps)
        self.on_animate_fini = on_animate_fini
        self.ixframe = 0
        self.fx = None
        # has user requested that the animation halt?
        self.halted = False
        # mp4 io
        self.writer = None
        self.reader = None
        # construct first frame
        s = get_shot(ixS)
        s.preload()
        self.prv_img_pil = None
        self.img_pil = get_frame_pil(self,s,0)

    # advance to next frame: returns False if we reached the end
    def advance(self):
        s = get_shot(self.ixshot)
        # do we kick off a fade or dissolve?
        fadespec = s.fxdct.get('fade')
        if (fadespec is not None and 
                self.fx is None and
                self.ixshot + 1 < scripts.cnt_shots() and
                self.ixframe + 1 == s.nframes):
            secs = fadespec['secs']
            self.fx = FXfade(self,float(secs),self.fps)
            if self.fx.advance():
                return True
        # are we in a fade or dissolve?
        if self.fx is not None:
            if self.fx.advance():
                return True
            # completed the sequence: kill fx and continue to
            # code below
            self.fx = None
        if self.ixframe + 1 < s.nframes:
            self.ixframe += 1
        else:
            # advance to next shot
            self.ixshot += 1
            self.ixframe = 0
            if self.ixshot == self.ixE:
                # done!
                return False
        # draw upcoming frame 
        s = get_shot(self.ixshot)
        s.preload()
        self.prv_img_pil = self.img_pil
        self.img_pil = get_frame_pil(self,s,self.ixframe)
        return True

    # mp4 io. If making an mp4, we open a writer. If bg of
    # shot is a clip, we open a reader.
    def close_writer(self):
        try:
            self.writer.close()
        except:
            pass
        self.writer = None

    def open_writer(self,fp):
        self.close_writer()
        self.writer = imageio.get_writer(fp, fps = 24, mode='I')

    def close_reader(self):
        try:
            self.reader.close()
        except:
            pass
        self.reader = None

    def open_reader(self,fp):
        self.close_reader()
        try:
            self.reader = imageio.get_reader(fp)
        except:
            # clip was deleted?
            pass

    def close_mp4_io(self):
        self.close_reader()
        self.close_writer()

    def halt(self):
        self.halted = True
        self.on_animate_fini(self)

def set_tween(ani,shot,ixframe):
    # set tween for an animation.
    # The tween gives size (w,h) and position (x,y) for an
    # animation element. If ani is a sprite (image, as opposed
    # to text), it also loads & adjusts the image.

    # create (or update) the tween
    if ixframe == 0:
        ani.tween = Tween(ani,shot.nframes,shot.get_ani_cycles(ani))
    else:
        ani.tween.update(ixframe)

def get_bg_pil(shot):
    bgspec = shot.bgspec
    (w,h) = (res.w_shot,res.h_shot)
    if bgspec.startswith('#'):
        return Image.new("RGBA", (w,h),bgspec)
    else:
        return res.get_img(bgspec).copy()


def get_frame_pil(state,shot,ixframe):
    # create a pil image for the frame

    if ixframe == 0:
        # First frame for the shot. If bg is a clip, open an mp4 reader.
        state.close_reader()
        if shot.has_bg_clip():
            try:
                state.open_reader(res.get_fpart(shot.bgspec)) 
            except:
                # this is ok: see "get_bg_pil"
                pass

    # img_dst: PIL image which we draw upon (it's the bg for
    # the shot).
    reader = state.reader
    if reader is not None:
        try:
            img_dst = Image.fromarray(reader.get_next_data())
        except IndexError:
            # Can happen in some special effects: get last frame
            reader.set_image_index(reader.count_frames()-1)
            img_dst = Image.fromarray(reader.get_next_data())
    else:
        img_dst = get_bg_pil(shot)
    (xmax,ymax) = img_dst.size
    xmax -= 1
    ymax -= 1

    # set tween for camera and get (x,y,w,h) for camera
    set_tween(shot.cam,shot,ixframe)
    x_cam = shot.cam.tween.x
    y_cam = shot.cam.tween.y
    w_cam = shot.cam.tween.w
    h_cam = shot.cam.tween.h

    for ani in shot.anis:
        #ani.dump()
        set_tween(ani,shot,ixframe)
    # Adjust for tracking
    if shot.cam.lockto is not None:
        shot.cam.tween.track(shot.cam.lockto.tween)
    for ani in shot.anis:
        if ani.lockto is not None:
            ani.tween.track(ani.lockto.tween)

    # constrain camera so it's fully contained in image
    t = shot.cam.tween
    (xmax,ymax) = img_dst.size
    t.x = max(t.x,0)
    t.x = min(t.x, xmax- t.w)
    t.y = max(t.y,0)
    t.y = min(t.y, ymax- t.h)

    # If this is frame0, and shot has lighting, create the mighting mask
    if ixframe == 0:
        params = shot.fxdct.get('lighting')
        if params is not None:
            shot.mask = res.get_lighting_mask(xmax+1,ymax+1,params)
            if params['lock_to_cam']:
                # crop down to the region covered by the camera
                shot.mask = shot.mask.crop((t.x, t.y, t.x+t.w,
                        t.y+t.h))
        else:
            shot.mask = None


    # Collect the tweens for all the anis and sort by Z order
    tweens = []
    for ani in shot.anis:
        tweens.append(ani.tween)
    tweens.sort(key=lambda e:e.z)
    draw_pil = ImageDraw.Draw(img_dst)
    for t in tweens:
        if t.ani.kind == 'spr':
            img_pil = t.img_pil
            if img_pil is not None:
                if t.blend_param > 0:
                    try:
                        #print('t.blend_param: %.2f' % t.blend_param)
                        # create a blended image
                        # PIL blend functions requires images to be exact same
                        # size.
                        prv_img_pil = t.prv_img_pil.resize(
                            img_pil.size)
                        blended = Image.blend(img_pil, t.prv_img_pil,
                            alpha=t.blend_param)
                        # paste onto dst
                        img_dst.paste(blended,(t.x,t.y),mask=blended)
                        prv_img_pil.close()
                        blended.close()
                    except ValueError:
                        # blend failed: treat as standard case
                        img_dst.paste(img_pil,(t.x,t.y),mask=img_pil)
                else:
                    img_dst.paste(img_pil,(t.x,t.y),mask=img_pil)
        else:
            # text element
            te = t.ani.te
            x = t.x
            y = t.y
            # bg. 
            (x0,y0,x1,y1) = te.get_bb_bg(x,y)
            if te.bgspec != 'null':
                bgspec = te.bgspec
                if bgspec.startswith('#'):
                    draw_pil.rectangle((x0,y0,x1,y1), fill=bgspec)
                else:
                    img = res.get_res(bgspec)
                    img = img.resize((int(te.w_bg),int(te.h_bg)),
                            Image.ANTIALIAS)
                    img_dst.paste(img,(x0,y0),mask=img)
                    img.close()
            # text
            (x0,y0,x1,y1) = te.get_bb_text(x,y)
            draw_pil.text( (x0,y0),
                te.get_text(), fill=te.fontcolor, 
                font=res.get_font(te.fontname,te.fontsize) )

    # How we procede depends on whether we're using a lighting mask
    params = shot.fxdct.get('lighting')
    if params is not None:
        mask = shot.mask
        (w,h) = mask.size
        if not params['lock_to_cam']:
            # mask applies to entire image
            img_dst.paste(mask,(0,0,w,h),mask)
        else:
            # paste mask to region shown by  camera
            t = shot.cam.tween
            mask = mask.resize((t.w,t.h))
            img_dst.paste(mask,(t.x, t.y, t.x+t.w,
                t.y+t.h),mask=mask)

    # crop to region covered by canvas abd resize to view dims
    #cropped = img_dst.crop((x_cam, y_cam, x_cam+w_cam-1,
                        #y_cam+h_cam-1))
    t = shot.cam.tween
    cropped = img_dst.crop((t.x, t.y, t.x+t.w, t.y+t.h))
    img_pil = cropped.resize(
        (res.w_shot,res.h_shot),Image.ANTIALIAS)
    cropped.close()
    img_dst.close()
    return img_pil

def animate_shot(state):
    global cur_ani_state
    if state.halted:
        # ignore scheduled event from dead thread
        return

    s = get_shot(state.ixshot)
    # mark the time
    t_start = time.time()
    # blit the pre-drawn frame
    state.can.delete('all')
    s.cam.im_tk = ImageTk.PhotoImage(state.img_pil)
    state.can.create_image(20,20, anchor='nw',image=s.cam.im_tk)
    state.img_pil.close()
    # set up for next shot
    if not state.advance():
        # done!
        #state.can.after(1, state.on_animate_fini)
        cur_ani_state = None
        state.on_animate_fini(state)
        return
    # schedule the next frame
    t_end = time.time()
    t_elapsed = int( 1000 * (t_end - t_start))
    t_wait = max(1, state.msecs_frame - t_elapsed)
    state.can.after( t_wait, animate_shot, state)

def set_shot_nframes(fps, speed, ixS,ixE):
    # for each shot in S..E, compute number of frames.
    # "speed" is play-back speed: 1.0 means normal, 2.0 means
    # play twice as fast.
    # fps -> frames per second
    total_actual = 0
    for i in range(ixS, ixE):
        s = get_shot(i)
        s.nframes = int(.5 + fps * s.secs/speed)
        total_actual += s.nframes
        fadespec = s.fxdct.get('fade')
        if fadespec is not None:
            # add fade/dissolve frames
            secs = fadespec['secs'] 
            total_actual += int(.5 + fps * secs/speed)
    # if last shot is fade/dissolve, need to set nframes
    # for successor.
    if ixE < scripts.cnt_shots():
        s = get_shot(ixE)
        s.nframes = int(.5 + fps * s.secs/speed)
    return total_actual

# state for currently running animation
cur_ani_state = None
def animate_shots(can,ixS,ixE,speed,on_animate_fini):
    # animate shot sequence ixS..ixE
    # S & E follow python style range conventions
    global cur_ani_state
    if cur_ani_state is not None:
        # already running an animation: kill the current
        # thread.
        halt_play()
    # set number of frames for each shot
    fps = 24
    set_shot_nframes(fps, speed, ixS,ixE)
    cur_ani_state = AniState(can,ixS,ixE,fps,on_animate_fini)
    # kick off the animation
    animate_shot(cur_ani_state)

def halt_play():
    global cur_ani_state
    if cur_ani_state is not None:
        cur_ani_state.halt()
        cur_ani_state = None


html = \
'''
<!DOCTYPE html>
<html>
<body style='text-align:center;background-color:#454545' >
<p>
<video width="_w_" height="_h_" controls>
  <source src="__fn__.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video>
</body>
</html>
'''

def make_mp4(can,post_status):
    global cur_ani_state
    if cur_ani_state is not None:
        # already running an animation: kill the current
        # thread.
        halt_play()
    # set number of frames for each shot
    fps = 24
    N = cnt_shots()
    nframes = set_shot_nframes(fps, 1.0, 0, N)
    nwritten = 0
    # save shot width and height, then reset for mp4: we want smaller
    # dim to be 608.
    wh_shot_save = (res.w_shot,res.h_shot)
    (res.w_shot, res.h_shot) = res.get_mp4_dims()
    proj_dir = scripts.proj_dir
    fnroot = scripts.proj_name[:-5]

    def on_make_fini(state):
        # close writer
        state.close_mp4_io()
        # restore saved w & h for shots
        (res.w_shot,res.h_shot) = wh_shot_save
        # clear status & show the video if we were not halted
        # prematurly
        post_status('')
        if not state.halted:
            fp = '%s/%s.htm' % (proj_dir,fnroot)
            with open(fp,"w") as f:
                _html = html.replace('__fn__',fnroot)
                _html = _html.replace('_w_',str(res.w_shot))
                _html = _html.replace('_h_',str(res.h_shot))
                f.write(_html)
            webbrowser.open_new_tab('file://' + fp)   

    def write_blk_mp4(state):
        nonlocal nwritten
        if state.halted or state.writer is None:
            # ignore scheduled event from halted (or completed) thread
            return
        blocksize = 24
        for cnt in range(0,blocksize):
            # add pre-written frame
            img_pil = state.img_pil.convert('RGBA')
            try:
                state.writer.append_data(numpy.asarray(img_pil))
            except AttributeError:
                # can happen when action halted
                pass
            nwritten += 1
            if nwritten % 10 == 0:
                post_status(
                'wrote frame %d of %d ...' % (nwritten,nframes))
            if not state.advance():
                # done!
                cur_ani_state = None
                on_make_fini(state)
                return
        # schedule nxt set
        state.can.after( 1, write_blk_mp4, state)

    cur_ani_state = AniState(can,0,N,fps,on_make_fini)
    cur_ani_state.open_writer( '%s/%s.mp4' % (proj_dir,fnroot) )
    write_blk_mp4(cur_ani_state)


# unit test
def ut():
    # Set some parameters

    # length of shot
    secs = 3.0
    # frames_per_cell
    frames_per_cell = 4
    # reverb
    reverb = 2
    # cyles for animation
    cycles = 2
    # .tS and .tE for animation
    tS = 0.25
    tE = 0.75

    # Create a shot with 4-cell, 1 pt animation
    shot = scripts.Shot(secs)
    # add 4-cell ani containing one point
    ani = scripts.Ani('spr',tS,tE,cycles,frames_per_cell,reverb)
    ani.fnlst = ['x0','x1','x2','x3']
    #ani.fnlst = ['x0']
    shot.anis.append(ani)
    pt = scripts.Pt(100,100,0.0, 0.0,0.0,50,50)
    ani.add_pt(pt)

    # sync shot length to ani
    if len(ani.fnlst) > 1:
        shot.sync_secs(ani)

    # animate
    shot.nframes = int(shot.secs * 24)
    for ixframe in range(0,shot.nframes):
        set_tween(ani,shot,ixframe)
        tw = ani.tween
        print('%d (%.2f). param: %.2f ixcell:%d blend:%.2f' % 
            (ixframe, float(ixframe)/shot.nframes,
                tw._param, tw.ixcell, tw.blend_param))

if __name__ == '__main__':
    ut()


