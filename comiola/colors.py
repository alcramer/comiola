# Copyright Al Cramer 2021 ac2.71828@gmail.com
import re

# is 'color' and HTML hex code?
def isHexColor(color):
    return re.search(r'^#(?:[0-9a-fA-F]{3}){1,2}$', color) is not None

# convert hexcode str ("ff0000") to (r,g,b) tuple
def hexcode_to_bytes(hexcode):
    tmp = hexcode.lstrip('#')
    lv = len(tmp)
    return tuple(
            int(tmp[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))

# create various shades of primary colors
def col_interpolate(color,ix,N):
    rgb = [0,0,0]
    delta = 0 if N == 1 else 126.0/(N-1)
    if color == 'red':
        rgb[0] = 126 + int(delta*ix)
    elif color == 'green':
        rgb[1] = 126 + int(delta*ix)
    elif color == 'blue':
        rgb[2] = 126 + int(delta*ix)
    return "#%02x%02x%02x" % (rgb[0],rgb[1],rgb[2])

ONE_THIRD = 1.0/3.0
ONE_SIXTH = 1.0/6.0
TWO_THIRD = 2.0/3.0

# HLS: Hue, Luminance, Saturation
# H: position in the spectrum
# L: color lightness
# S: color saturation

def rgb_to_hls(r, g, b):
    r /= 255.0
    g /= 255.0
    b /= 255.0

    maxc = max(r, g, b)
    minc = min(r, g, b)
    # XXX Can optimize (maxc+minc) and (maxc-minc)
    l = (minc+maxc)/2.0
    if minc == maxc:
        return 0.0, l, 0.0
    if l <= 0.5:
        s = (maxc-minc) / (maxc+minc)
    else:
        s = (maxc-minc) / (2.0-maxc-minc)
    rc = (maxc-r) / (maxc-minc)
    gc = (maxc-g) / (maxc-minc)
    bc = (maxc-b) / (maxc-minc)
    if r == maxc:
        h = bc-gc
    elif g == maxc:
        h = 2.0+rc-bc
    else:
        h = 4.0+gc-rc
    h = (h/6.0) % 1.0
    return h, l, s

def hls_to_rgb(h, l, s):
    if s == 0.0:
        return (int(255.0*l),int(255.0*l),int(255.0*l))
    if l <= 0.5:
        m2 = l * (1.0+s)
    else:
        m2 = l+s-(l*s)
    m1 = 2.0*l - m2
    (r,g,b) = (_v(m1, m2, h+ONE_THIRD), _v(m1, m2, h), _v(m1, m2, h-ONE_THIRD))
    return (int(255.0*r),int(255.0*g),int(255.0*b))

def _v(m1, m2, hue):
    hue = hue % 1.0
    if hue < ONE_SIXTH:
        return m1 + (m2-m1)*hue*6.0
    if hue < 0.5:
        return m2
    if hue < TWO_THIRD:
        return m1 + (m2-m1)*(TWO_THIRD-hue)*6.0
    return m1

def hex_to_hls(hexstr):
    hexstr = hexstr.lstrip('#')
    lv = len(hexstr)
    (r,g,b) = tuple(
        int(hexstr[i:i + lv // 3], 16) for i in range(0, lv, lv // 3)
        )
    return rgb_to_hls(r,g,b)

def hls_to_hex(h,l,s):
    (r,g,b) = hls_to_rgb(h, l, s)
    return '#%02x%02x%02x' % (r,g,b)
   

