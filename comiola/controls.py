# Copyright Al Cramer 2021 ac2.71828@gmail.com
from PIL import ImageDraw
from scripts import Pt
from colors import *
import uitimeline
import display
import resources as res
import polygon

# z ordering for handles. Given 2 handles h1 and h2, h2.z > h1.z means
# h2 was after h1. If h2 and h1 overlap, and both are hits on a
# mousedown, we use this fact to select h2 over h1.
hand_z = 0

# A handle is a small, clickable shape shown in
# the display. It's owned by a controller, which handles
# its mouse events. 
class Handle:
    def __init__(self,cntrl,pt=None,dim=None):
        self.cntrl = cntrl
        self.pt = pt
        if pt is not None:
            pt.hand = self
        cntrl.vc.handles.append(self)
        self.visible = True
        self.x = -1.0
        self.y = -1.0
        # Used in selection: if h1 and h2 are both hit by a mousedown,
        # we select the one with higher z
        self.z= 0

    def __str__(self):
        return 'Handle. x:%.1f y:%.1f' % (self.x,self.y)


    def set(self,x,y,visible=True):
        self.x = x
        self.y = y
        self.visible = visible

    def draw(self,color,shape):
        global hand_z
        if not self.visible:
            return
        # "z" attribute records draw-order for handles: see note at
        # top
        self.z = hand_z
        hand_z += 1
        # draw to PIL image
        self.cntrl.vc.draw_handle(self.x,self.y,color,shape)

# Control for a point in an Ani path, for sprite and camera
# animations.
class SprPtCntrl:
    def __init__(self,vc,ani,pt):
        self.vc = vc
        self.pt = pt
        self.pt.cntrl = self
        self.ani = ani

        # resize handle
        self.resize_hand = Handle(self)
        # position handle
        self.pos_hand = Handle(self,pt)
        self.pos_hand.set(pt.x,pt.y)

        # 4 handles: (upper|lower)(left|right) for perspective xfrms
        self.ul = Handle(self)
        self.ur = Handle(self)
        self.ll = Handle(self)
        self.lr = Handle(self)
        self.set_persp_handles()

        self.all_h = [self.pos_hand, self.resize_hand,
               self.ul,self.ur,self.ll,self.lr]
        
        # mode: "spr" or "persp"
        self.mode = None
        self.set_mode('spr')

        # clone of "pt" at start of mousemove
        self.ptS = None
        self.visible = True

    def set_persp_handles(self):
        (xc,yc,w,h) = (self.pt.x,self.pt.y,self.pt.w,self.pt.h)
        _w = int(w/2)
        _h = int(h/2)
        self.ul.set(xc - _w, yc - _h)
        self.ur.set(xc + _w, yc - _h)
        self.ll.set(xc - _w, yc + _h)
        self.lr.set(xc + _w, yc + _h)
         
    def persp_is_visible(self):
        return self.visible and self.mode == 'persp'

    def set_visible(self,vbool):
        self.visible = vbool
        for h in self.all_h:
            h.visible = vbool

    def set_mode(self,mode):
        self.mode = mode
        for h in self.all_h:
            h.visible = False
        if mode == 'spr':
            for h in [self.resize_hand,self.pos_hand]:
                h.visible = True
        else:
            for h in [self.ul,self.ur,self.lr,self.ll]:
                h.visible = True
            self.set_persp_handles()

    def draw_spr(self,color,is_selected,show_resize=True):
        # draw cntrl for spr mode
        if not show_resize:
            return
        # draw a rectangle showing size
        p = self.pt
        w = int(p.w/2) - 2
        h = int(p.h/2) - 2
        (x,y) = (p.x,p.y)
        self.vc.draw_rect(color,
                x-w, y-h, x+w, y+h)
        # position and draw the resize handle
        hand = self.resize_hand
        #hand.set( x + p.w/2, y, is_selected)
        hand.set( x + w, y, is_selected)
        hand.draw(color,'tri')

    def draw_persp(self,color):
        # draw cntrl for persp mode
        (ul,ur,lr,ll) = (
            self.ul, self.ur, self.lr, self.ll)
        ul.draw(color,'tri')
        ur.draw(color,'tri')
        ll.draw(color,'tri')
        lr.draw(color,'tri')
        # draw the polygon
        (x0,y0) = (ul.x,ul.y)
        (x1,y1) = (ur.x,ur.y)
        (x2,y2) = (lr.x,lr.y)
        (x3,y3) = (ll.x,ll.y)
        self.vc.draw_poly(color, (x0,y0,x1,y1,x2,y2,x3,y3))

    def draw(self,color,is_selected,show_resize=True):
        if not self.visible:
            return
        # if we're not selected, set our mode to "spr" as needed
        if not is_selected:
            if self.mode != 'spr':
                self.set_mode('spr')
        p = self.pt
        shape = 'oval' if is_selected else 'rect'
        hand = self.pos_hand
        hand.set(self.pt.x,self.pt.y)
        hand.draw(color,shape)
        if self.mode == 'spr':
            self.draw_spr(color,is_selected,show_resize)
        elif self.mode == 'persp':
            self.draw_persp(color)

    def on_h_selected(self,h):
        if h is not None:
            # clone starting point
            self.ptS = self.pt.clone()
            # save start positions of all handles
            for hx in self.all_h:
                hx.xS = hx.x
                hx.yS = hx.y

    def on_h_move(self,src,x,y,xS,yS):
        dx = x - xS
        dy = y - yS
        if src == self.pos_hand:
            # moved position handle: translate all
            for h in self.all_h: 
                h.set(h.xS + dx, h.yS + dy)
            # reset pt (x,y)
            (self.pt.x,self.pt.y) = (self.pos_hand.x,self.pos_hand.y)
            # if we're a pt in animation path, reset path params
            if self.ani is not None:
                self.ani.set_path_params()
            return
        if src == self.resize_hand:
            # resizing 
            (w,h) = (self.ptS.w,self.ptS.h)
            ar = float(h)/w
            wnew = w +2*dx
            hnew = ar * wnew
            if self.ani is not None and self.ani.kind == 'cam':
                # camera dim's can't exceed bg size. Allowing small
                # margin reduces jankiness
                if (wnew > display.w_img +6  or 
                    hnew > display.h_img + 6):
                    return
            # minimum dim is 12pix
            if wnew >= 12 and hnew >= 12:
                self.pt.w = wnew
                self.pt.h = hnew
                self.set_persp_handles()
            return
        # corner handles for perspective xfrm
        (xnew,ynew) = (src.xS+dx,src.yS+dy)
        (xc,yc) = (self.pt.x,self.pt.y)
        pad = 16
        if src == self.ul:
            if (xnew > xc - pad) or (ynew > yc - pad):
                return
            (src.x,src.y) = (xnew,ynew) 
            self.ll.x = xnew
        elif src == self.ur:
            if (xnew < xc + pad) or (ynew > yc - pad):
                return
            (src.x,src.y) = (xnew,ynew) 
            self.lr.x = xnew
        elif src == self.ll:
            if (xnew > xc - pad) or (ynew < yc + pad):
                return
            (src.x,src.y) = (xnew,ynew) 
            self.ul.x = xnew
        elif src == self.lr:
            if (xnew < xc + pad) or (ynew < yc + pad):
                return
            (src.x,src.y) = (xnew,ynew) 
            self.ur.x = xnew

    def get_persp_params(self):
        (ul,ur,lr,ll) = ( self.ul,self.ur,self.lr,self.ll)
        # heights, left side and right side of polygon
        hl = ll.y - ul.y
        hr = lr.y - ur.y
        # width of polygon
        wpoly = ur.x - ul.x
        # width of image
        wimg = self.pt.w

        # the perspective parameters
        #wparam = wpoly/float(hl)
        wparam = wpoly/wimg
        hparam = hr/float(hl)
        fall = (ur.y - ul.y)/float(hl)

        dim = max(hl,hr)
        #dim = hl

        return (wparam,hparam,fall,dim)

class AniCntrl():
    def __init__(self,vc,ani):
        self.ani = ani
        self.ptcntrls = []
        for p in ani.path:
            if ani.kind == 'txt':
                self.ptcntrls.append(TxtPtCntrl(vc,ani,p,ani.te))
            else:
                self.ptcntrls.append(SprPtCntrl(vc,ani,p))

    def draw(self,sel_pt,show_resize=True):
        ani = self.ani
        basecolor = 'green' if ani.kind == 'cam' else 'red'
        for i in range(0,len(ani.path)):
            p = ani.path[i]
            if ani.kind == 'cam':
                color = col_interpolate('green',i,len(ani.path))
            elif ani.kind == 'spr':
                color = col_interpolate('red',i,len(ani.path))
            else:
                # text element: doesn't change
                color = '#00ffff'
            self.ptcntrls[i].draw(color,p == sel_pt,show_resize)

# Control for a point in an Ani path, for txt animations.
class TxtPtCntrl:
    def __init__(self,vc,ani,pt,te):
        self.vc = vc
        self.pt = pt
        self.ani = ani
        self.te = te
        # position control
        self.pos_hand = Handle(self,pt)
        # resize control bg w and h
        self.bgsize_hand = Handle(self)
        # y offset control for bg
        self.yoff_bg_hand = Handle(self)
        # data at start of mousemove
        self.ptS = None
        self.teS = None

    def draw(self,color,is_selected,show_resize=True):
        # draw pt handle
        pt = self.pt
        shape = 'oval' if is_selected else 'rect'
        hand = self.pos_hand
        hand.set(pt.x,pt.y, True)
        hand.draw(color,shape)
        # draw text: if text is short, the center handle may cover
        te = self.te
        vc = self.vc
        if te.get_text() != '':
            (x0,y0,x1,y1) = te.get_bb_text(pt.x,pt.y)
            self.vc.draw_text(
                te.fontcolor,
                x0,y0,
                te.get_text(),
                res.get_font(te.fontname,te.fontsize))

        if not show_resize:
            return

        # get bounding box for bg and 
        # draw a rectangle showing size
        self.vc.draw_rect(color,
                *self.te.get_bb_bg(self.pt.x,self.pt.y))
        # position handles
        #  bgsize is at lower right corner
        hand = self.bgsize_hand
        hand.set(x1,y1,is_selected)
        hand.draw(color,'tri')
        # yoff is top center
        hand = self.yoff_bg_hand
        hand.set( (x0+x1)/2, y0, is_selected )
        hand.draw(color,'tri')

    def on_h_selected(self,h):
        self.ptS = self.pt.clone()
        self.teS = self.te.clone()

    def on_h_move(self,src,x,y,xS,yS):
        xdelta = x - xS
        ydelta = y - yS
        if src == self.bgsize_hand:
            self.te.w_bg = self.teS.w_bg + 2*xdelta
            self.te.h_bg = self.teS.h_bg + 2*ydelta
        elif src == self.yoff_bg_hand:
            # resizing offset 
            self.te.yoff_bg = self.teS.yoff_bg + ydelta
        elif src == self.pos_hand:
            # moving (x,y) of point
            self.pt.set_xy( self.ptS.x + xdelta, self.ptS.y + ydelta )

# BoundingBox Path control
class BBPathCntrl:
    def __init__(self,vc):
        self.vc = vc
        # visibility 
        self.visible = False
        # handles for center, topleft, and bottom right
        self.center_hand = Handle(self)
        self.resize_hand = Handle(self)
        # padding around bb
        self.pad = 20
        # bb: width and height
        self.w_bb = 0
        self.h_bb = 0

    def set_visible(self,state):
        self.visible = state
        self.center_hand.visible = state
        self.resize_hand.visible = state

    def draw(self):
        if (not self.visible) or display.sel_ani is None:
            return
        # refs to handles
        center = self.center_hand
        resize = self.resize_hand
        # get bounding box
        path = display.sel_ani.path
        (x0,y0) = (path[0].x, path[0].y)
        (x1,y1) = (path[0].x, path[0].y)
        for pt in path:
            x0 = min(x0,pt.x)
            y0 = min(y0,pt.y)
            x1 = max(x1,pt.x)
            y1 = max(y1,pt.y)
        center.x = (x0+x1)/2
        center.y = (y0+y1)/2
        self.w_bb = (x1-x0+1)
        self.h_bb = (y1-y0+1)
        # resize handle is at top-right corner (plus padding)
        pad = self.pad
        resize.x = center.x + self.w_bb/2 + pad
        resize.y = y0 - pad
        color = '#800080'
        center.draw(color,'rect')
        resize.draw(color,'tri')
        display.vc.draw_rect(color,
                x0-pad, y0-pad, x1+pad, y1+pad)

    # coord transforms, bb <-> image
    def im_to_bb_coords(self,x,y):
        (xc,yc) = (self.center_hand.x,self.center_hand.y)
        return ( x-xc, y-yc )

    def bb_to_im_coords(self,x,y):
        (xc,yc) = (self.center_hand.x,self.center_hand.y)
        return ( x+xc, y+yc )

    def on_h_selected(self,h):
        if h is None:
            return
        (self.w_bbS, self.h_bbS) = (self.w_bb,self.h_bb)
        for pt in display.sel_ani.path:
            (pt.xS,pt.yS) = (pt.x,pt.y)

    def on_h_move(self,src,x,y,xS,yS):
        dx = x - xS
        dy = y - yS
        if src == self.resize_hand:
            w_new = self.w_bbS + 2*dx
            h_new = self.h_bbS - 2*dy
            w_scale = float(w_new)/self.w_bbS
            h_scale = float(h_new)/self.h_bbS
            for pt in display.sel_ani.path:
                (x,y) = self.im_to_bb_coords(pt.xS,pt.yS)
                (pt.x, pt.y) = self.bb_to_im_coords(
                    x*w_scale, y*h_scale)
        elif src == self.center_hand:
            for pt in display.sel_ani.path:
                (pt.x, pt.y) = (pt.xS + dx, pt.yS + dy)

# Control for light position (x,y) parameters
class LightCntrl:
    def __init__(self,vc,dim,params,on_drag_end=None):
        self.vc = vc
        self.params = params
        self.on_drag_end = on_drag_end
        self.h = Handle(self,dim=dim)
        self.h.set(
                params['xor']*vc.w_img,
                params['yor']*vc.h_img,
                )
        self.xyS = None

    def draw(self,color):
        self.h.draw(color,'ellipse')

    def on_h_selected(self,h):
        if h is not None:
            self.xyS = (h.x,h.y)

    def on_drag_end(self):
        if self.on_drag_end is not None:
            self.on_drag_end(src)

    def on_h_move(self,src,x,y,xS,yS):
        src.x = self.xyS[0] + x - xS
        src.y = self.xyS[1] + y - yS
        self.params['xor'] = src.x/self.vc.w_img
        self.params['yor'] = src.y/self.vc.h_img

    def set_visible(self,vbool):
        self.h.visible = vbool

# Control for a (selection) rectangle
class RectCntrl:
    def __init__(self,vc,x0,y0,x1,y1):
        self.vc = vc
        # 2 handles: one for xy0, one for xy1
        self.xy0 = Handle(self)
        self.xy0.set(x0,y0)
        self.xy1 = Handle(self)
        self.xy1.set(x1,y1)
        # start position of dragged handle
        self.xyS = None

    def draw(self,color):
        xy0 = self.xy0
        xy1 = self.xy1
        xy0.draw(color,'rect')
        xy1.draw(color,'rect')
        # draw the rectangle
        self.vc.draw_rect(color,xy0.x,xy0.y,xy1.x,xy1.y)

    def on_h_selected(self,h):
        if h is not None:
            self.xyS = (h.x,h.y)

    def on_h_move(self,src,x,y,xS,yS):
        src.x = self.xyS[0] + x - xS
        src.y = self.xyS[1] + y - yS

    def get(self):
        return (
            self.xy0.x, self.xy0.y,
            self.xy1.x, self.xy1.y)

# 4 point polygon control
class PolyCntrl():
    def __init__(self,vc):
        self.vc = vc
        self.h_center = None
        self.h_pts = []
        self.prop = None

    def add_pt(self,x,y):
        h = Handle(self)
        self.h_pts.append(h)
        h.set(x,y)
        if len(self.h_pts) == 2:
            self.h_center = Handle(self)
        self.set_center()

    def set_center(self):
        if self.h_center is None:
            return
        (xsum,ysum) = (0,0)
        for h in self.h_pts:
            xsum += h.x
            ysum += h.y
        self.h_center.set( xsum/len(self.h_pts), ysum/len(self.h_pts) )

    def on_h_selected(self,h):
        if h is not None:
            # save start positions of all handles
            if self.h_center is not None:
                self.h_center.xS = self.h_center.x
                self.h_center.yS = self.h_center.y
            for hx in self.h_pts:
                hx.xS = hx.x
                hx.yS = hx.y

    def on_h_move(self,src,x,y,xS,yS):
        dx = x - xS
        dy = y - yS
        if src == self.h_center:
            # moved position handle: translate all
            h = self.h_center
            h.set(h.xS + dx, h.yS + dy)
            for hx in self.h_pts: 
                hx.set(hx.xS + dx, hx.yS + dy)
        elif src in self.h_pts:
            # move the point, then reset center
            src.set(src.xS + dx, src.yS + dy)
            self.set_center()
            
    def get_pts(self):
        return [(p.x,p.y) for p in self.h_pts]

    def is_inside(self,x,y):
        if len(self.h_pts) <= 2:
            return False
        pts = [(p.x,p.y) for p in self.h_pts]
        return polygon.poly_contains(pts,(x,y))

    def draw(self,color):
        if len(self.h_pts) == 0:
            return
        if self.h_center is not None:
            self.h_center.draw(color,'oval')
        for hx in self.h_pts:
            hx.draw(color,'tri')
        # draw the polygon lines
        pts = []
        for h in self.h_pts:
            pts.append(h.x)
            pts.append(h.y)
        self.vc.draw_poly(color,pts)


# Draw-a-line-segment control
class LineSegCntrl:
    def __init__(self,vc,x,y):
        self.vc = vc
        (self.x0,self.y0) = (x,y)
        (self.x1,self.y1) = (x,y)

    def draw(self,color,width):
        self.vc.draw_line(color,
                (self.x0,self.y0,self.x1,self.y1),
                width=width)

    def set_endpoint(self,x,y):
        (self.x1,self.y1) = (x,y)
 
    def get_endpoints(self):
        return (self.x0,self.y0, self.x1,self.y1)

