# Copyright Al Cramer 2021 ac2.71828@gmail.com
from PIL import ImageTk,Image,ImageDraw,ImageOps

import scripts
from scripts import Pt,Ani
import resources as res
import controls
from imgvc import ImgViewCntrl
import animate
import images

import uiglobals
import xfrmpersp
import uilighting

# canvas: set in __main__
can = None

# width & height of image (non-scaled)
w_img = 0
h_img = 0

# left and top margins for image view controller
pad = 260

# the image view controller
vc = None

# index of shot (and shot) we're currently showing
ixshot = -1
shot = None
# visibility setting
visible = {
    'sprimgs': True,
    'bg': True,
    'resize': True,
    'lighting': True,
    'bbpath': False,
}

# selected animation, pt, or text element 
sel_ani = None
sel_pt = None

# force a re-draw
def draw_edit():
    if vc is not None:
        vc.draw()

# controls for animations
anicntrls = []
# the bounding-box path control
bbpath_cntrl = None
# light position control
light_cntrl = None

def draw_anicntrls(kind,hide_anis):
    for c in anicntrls:
        if (not c.ani in hide_anis) and c.ani.kind == kind:
            c.draw(sel_pt,visible['resize'])

def draw_sel_anicntrl():
    for c in anicntrls:
        if c.ani == sel_ani:
            c.draw(sel_pt,visible['resize'])

def make_cntrls():
    global anicntrls,bbpath_cntrl,light_cntrl
    anicntrls = []
    if shot is None:
        return
    anicntrls.append(controls.AniCntrl(vc,shot.cam))
    for ani in shot.anis:
        anicntrls.append(controls.AniCntrl(vc,ani))
    # build the bounding-box path control.
    bbpath_cntrl = controls.BBPathCntrl(vc)
    # build the light control
    params = images.get_default_lighting()
    if shot is not None:
        _params = shot.fxdct.get('lighting')
        if _params is not None:
            params = _params.copy()
    light_cntrl = controls.LightCntrl(vc,16,params,
            on_drag_end=uilighting.on_light_apply)
    light_cntrl.set_visible(False)


# user wants these ani's to be hidden
hide_anis = []
def hide_ani(ani):
    hide_anis.append(ani)
    if ani == sel_ani:
        deselect_all()
    draw_edit()

def show_all_anis():
    global hide_anis
    hide_anis = []
    draw_edit()

# list of handlers for selection-change events: users of the
# display add methods to this list, and we call them when
# the selection changes.
sel_change_handlers = []
def on_sel_change():
    for f in sel_change_handlers:
        f()

def set_selected(ani,pt):
    global sel_pt, sel_ani
    sel_pt = pt
    sel_ani = ani
    on_sel_change()
    uiglobals.validate_view()

def deselect_all():
    global sel_pt, sel_ani
    sel_ani = None
    sel_pt = None
    uiglobals.validate_view()

def on_h_selected(h):
    global sel_ani, sel_pt
    # a handle was selected. De-selection is also handled here
    # (h of None means de-select).
    if h is None:
        deselect_all()
        on_sel_change()
        return
    # Does clicking the handle set our selected ani & pt? 
    if h.pt is not None:
        set_selected(h.cntrl.ani,h.pt)
        on_sel_change()

# mouse events are handled by our view controller.
def on_mousedown(ev):
    if vc is not None:
        vc.on_mousedown(ev)

def on_mouseup(ev):
    if vc is not None:
        vc.on_mouseup(ev)
        # this will validate timeline if pts were moved
        uiglobals.validate_view()

def on_mousemove(ev):
    if vc is not None:
        vc.on_mousemove(ev)

def on_doubleclick(ev):
    if vc is not None:
        vc.on_doubleclick(ev)

# draw functions
def draw_spr(pt):
    # create sprite image
    fnspr = pt.ani.fnlst[0]
    if pt.fnspr != '':
        fnspr = pt.fnspr
    (w,h) = (pt.w,pt.h)
    if pt.has_pxfrm():
        img = res.get_img_pxfrm(fnspr,
                pt.pxfrm_w, pt.pxfrm_h, pt.pxfrm_fall, h)
    else:
        #img = res.get_img(fnspr).resize((int(w),int(h)),Image.ANTIALIAS)
        img = res.get_img_spr(fnspr,w,h)
    if pt.flip == 1:
        img = ImageOps.mirror(img)
    if pt.rot != 0.0:
        img = img.rotate(pt.rot,expand=True)
    vc.paste_img(img,pt.x,pt.y)

def draw_te(pt):
    te = pt.ani.te
    # get (x0,y0,x1,y1) for bg. 
    (x0,y0,x1,y1) = te.get_bb_bg(pt.x,pt.y)
    bgspec = te.bgspec
    if bgspec != 'null':
        if bgspec.startswith('#'):
            vc.draw_filled_rect((x0,y0,x1,y1),bgspec)
        else:
            img = res.get_res(bgspec)
            w = int(x1-x0+1)
            h = int(y1-y0+1)
            img = img.resize((w,h),Image.ANTIALIAS)
            vc.paste_img(img,x0,y0)
    if te.get_text() == '':
        return
    (x0,y0,x1,y1) = te.get_bb_text(pt.x,pt.y)
    vc.draw_text(te.fontcolor,x0,y0,
        te.get_text(),
        font=res.get_font(te.fontname,te.fontsize))

# called from the image view controller to draw the shot 
def draw_content():
    if scripts.cnt_shots() == 0:
        return False
    s = shot
    # paste bg image.
    # If bg is not visible, paste a white square
    img_bg = animate.get_bg_pil(s)
    if not visible['bg']:
        (w,h) = img_bg.size
        img_bg = Image.new("RGBA",(w,h), (0xff,0xff,0xff))

    (w,h) = img_bg.size
    vc.paste_img(img_bg,int(w/2), int(h/2))

    # Collect all pts in the ani's (excluding those in "hide_anis")
    # and sort by Z
    pts = []
    for ani in s.anis:
        if not ani in hide_anis:
            for pt in ani.path:
                # pt needed ref to ani that owns it
                pt.ani = ani
                pts.append(pt)
    pts.sort(key=lambda e:e.z)

    # draw
    def draw_ani_pt(pt):
        if pt.ani.kind == 'spr':
            if visible['sprimgs']:
                try:
                    draw_spr(pt)
                except FileNotFoundError:
                    pass
        else:
            # text element
            draw_te(pt)
    
    for pt in pts:
        draw_ani_pt(pt)
    
    # draw lighting mask if defined. Do this after bg % sprites are
    # drawn, but before controls
    if s.mask is not None:
        (w,h) = s.mask.size
        vc.paste_img(s.mask,int(w/2),int(h/2))

    # draw the sprite and text controlls
    draw_anicntrls('spr',hide_anis)
    draw_anicntrls('txt',hide_anis)
    # draw cam 
    draw_anicntrls('cam',hide_anis)
    # if ani is selected, redraw (it should be on top in Z order)
    if sel_ani is not None:
        if sel_ani.kind in ['spr','txt']:
            for pt in sel_ani.path:
                draw_ani_pt(pt)
        draw_sel_anicntrl()
    # draw bbpath controller if visible
    if visible['bbpath']:
        bbpath_cntrl.set_visible(True)
        bbpath_cntrl.draw()
    else:
        bbpath_cntrl.set_visible(False)
    # draw light controller if defined
    if light_cntrl is not None:
        light_cntrl.draw('yellow')
    return True

def edit_shot(ix,show_tab_id=None):
    global w_img, h_img, hide_anis, ixshot, shot, vc, visible
    hide_anis = []
    can.delete('all')
    vc = None
    nshots = scripts.cnt_shots()
    if nshots == 0:
        return
    if ix >= nshots:
        ix = nshots - 1
    ixshot = ix
    shot = scripts.get_shot(ix)
    # set image w & h: derived from bg.
    img = animate.get_bg_pil(shot)
    (w_img,h_img) = img.size
    # create the image view controller
    vc = ImgViewCntrl(can,w_img,h_img,draw_content,
            on_h_selected=on_h_selected,
            MAXDIM_VIEW= res.MAXDIM_DISPLAY)
    # build the controllers for cam & sprites
    make_cntrls()
    # if shot has lighting, create the lighting mask.
    params = shot.fxdct.get('lighting')
    if params is not None and visible['lighting']:
        shot.mask = res.get_lighting_mask(w_img,h_img,params)
    else:
        shot.mask = None
    if show_tab_id is not None:
        uiglobals.show_tab(show_tab_id)
    # re-init visible (except for lighting -- that's controlled by user)
    visible['sprimgs'] = True
    visible['bg'] = True
    visible['resize'] = True
    visible['bbpath'] = False
    # this will call validate_view
    deselect_all()

def cam_selected():
    return (sel_ani is not None and
        sel_ani == shot.cam)

def sprite_selected():
    return (sel_ani is not None and
        sel_ani != shot.cam)

