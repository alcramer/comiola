from random import random
from PIL import Image

# Jiggle effect for sprite animation. It uses 3 images:
# the original sprite image ("img_spr"); a transformed version
# of the sprite image ("img_xfrm"); and a second transformed
# version, "img_dual", which is dual to the first transform.

# The effect can run with or without smear. Without smear, we
# show img_xfrm for N frames, then show img_dual for N frames,
# then create new versions and repeat. (N = 6/speed, so at speed of
# 1 a cycle lasts 1/2 second). With smear, it's essentially the
# same, except for first half of the cycle we fadeout img_spr
# and fadein img_xfrm; for second half, we fadout img_spr and
# fadein img_dual.

# We create img_xfrm and img_dual using "sqeeze and tug" transform.
# squeeze/tug maps a pt in the unit square (a,b) to a second
# point in the unit square (_a,_b). It's inspired by isometric
# perspective and works like this.
# 1. As we advance along the horizontal axis, vertical space is 
# squeezed together. Pts are also tugged downward. 
# This gives us _b as a function of a and b.
# 2. As we advance along the vertical axis, horizontal space is 
# squeezed together. Pts are also tugged rightward. 
# This gives us _a as a function of a and b.
#
# The xfrm parameters are:
# 1. rvert: the vertical squeeze parameter. It's the ratio
#    height of right-side / height of left-side.
#    If revert > 1, we invert it and treat it as the 
#    squeeze left-side case.
# 2. tvert parameter. We tug the right side down by:
#    (1-rvert)* tvert
#    tvert ranges 0..1
# 3. rhor,thor: same as rvert,tvert but for horizontal space.

class FXjiggle():
    def __init__(self,speed,amount,smear):
        self.smear = smear
        self.ixjiggle = 0
        self.set_play_params(speed,amount)

    def set_play_params(self,speed,amount):
        self.speed = speed
        self.amount = amount
        self.set_xfrm_params()

    def set_xfrm_params(self):
        # "rvert" param
        R_DIM = .1
        r = 1.0  - self.amount*random()*R_DIM
        if random() > .5:
            r = 1/r
        self.rvert = r
        # "rhor" param
        r = 1.0  - self.amount*random()*R_DIM
        if random() > .5:
            r = 1/r
        self.rhor = r
        # "tvert" and "thor" are in range 0..1
        self.tvert = random()
        self.thor = random()
    
    def to_dual(self):
        self.rvert = 1/self.rvert
        self.rhor = 1/self.rhor
        self.tvert = 1 - self.tvert
        self.thor = 1 - self.thor

    def xfrm(self,img_src):
        # transform image, adding jiggle
        def sqeezetug(a,b,r,t):
            if r <= 1:
                q = (1-r)*t
                scale_a = 1 + a*(r-1)
                q_a = a*q
            else:
                r = 1/r
                q = (1-r)*t
                scale_a = r + a*(1-r)
                q_a = (1-a)*q
            return b*scale_a + q_a

        (w,h) = img_src.size
        DIM = 200
        img_src = img_src.resize((DIM,DIM))
        img_dst = Image.new('RGBA',(DIM,DIM),(255,255,255,0))
        pix_src = img_src.load()
        pix_dst = img_dst.load()
        for x in range(0,DIM):
            for y in range(0,DIM):
                a = x/DIM
                b = y/DIM
                _b = sqeezetug(a,b,self.rvert,self.tvert)
                _a = sqeezetug(b,a,self.rhor,self.thor)
                (_x,_y) = ( int(.5+DIM*_a), int(.5+DIM*_b))
                try:
                    pix_dst[_x,_y] = pix_src[x,y]
                except IndexError:
                    pass
        # scale by 2/(r+1)
        r = self.rvert
        if r > 1:
            r = 1/r
        h_dst = int( h * 2/(r+1) )
        r = self.rhor
        if r > 1:
            r = 1/r
        w_dst = int( w * 2/(r+1) )
        img_dst = img_dst.resize((w_dst,h_dst))
        # crop to get final size of (w,h)
        x0 = int( (w_dst-w)/2 )
        y0 = int( (h_dst-h)/2 )
        img_dst = img_dst.crop((x0,y0,x0+w,y0+h))
        return img_dst

    def add_jiggle(self,img_pil,must_init=False):
        # Add jiggle to an image. 

        # len_period: number of frames before we change
        # the xfrm. Below means: speed of "1" means 6 frames.
        speed = max(self.speed,.01)
        len_period = int(6/speed)
        # minimum period is 3: less slows down the anmiation
        if len_period < 3:
            len_period = 3
        if must_init:
            self.ixjiggle = 0
            self.set_xfrm_params()
        if self.ixjiggle % len_period == 0:
            # create img_xfrm
            # is this an even or odd period?
            ixperiod = int(self.ixjiggle/len_period)
            is_even_period = ixperiod % 2 == 0
            if is_even_period:
                self.set_xfrm_params()
            else:
                # convert to the dual of the previous xfrm
                self.to_dual()
            self.img_xfrm = self.xfrm(img_pil)

        if img_pil.size != self.img_xfrm.size:
            self.img_xfrm = self.img_xfrm.resize(img_pil.size)
        # save ixjiggle and advance for next call
        ix = self.ixjiggle % len_period
        self.ixjiggle += 1
        if self.smear:
            # blend img_pil and img_xfrm
            blend_weight = .5 + .5*(ix/len_period)
            # record weight for test/dev 
            self.jig_weight = blend_weight
            #print('ix: %d weight: %.2f' % (ix,blend_weight))
            img = Image.blend(img_pil, self.img_xfrm, blend_weight)
        else:
            # just use current xfrm
            img = self.img_xfrm
        return img

