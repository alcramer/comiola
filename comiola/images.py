# Copyright Al Cramer 2021 ac2.71828@gmail.com
from PIL import Image, ImageDraw, ImageOps, ImageFont
import os
import shutil
import io
import imageio
import numpy
import math

import colors

'''
Panel creation: we accept an image, split as needed,
and turn it into 1 or more panels (p0.jpg, p1.jpg, etc.)
'''

MIN_GUTTER = 10

class Reg:
    def __init__(self,x0,y0,x1,y1):
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1
        self.ixrow = -1
        self.ixcol = -1
        self.v_bg = None

    def __str__(self):
        return ('x0,y0: %d,%d w:%d h:%d' %
            (self.x0, self.y0, self.get_w(), self.get_h()))

    def equals(self,rx):
        return (self.x0 == rx.x0 and
            self.y0 == rx.y0 and
            self.x1 == rx.x1 and
            self.y1 == rx.y1)

    def assign(self,rx):
        self.x0 = rx.x0
        self.y0 = rx.y0
        self.x1 = rx.x1
        self.y1 = rx.y1

    def contains(self,x,y):
        return (self.x0 <= x and x <= self.x1 and
                self.y0 <= y and y <= self.y1)

    def clone(self):
        return Reg(self.x0,self.y0,self.x1,self.y1)

    @classmethod
    def from_CWH(cls,xc,yc,w,h):
        # set from center/width/height
        return Reg(
            xc - int(w/2),
            yc - int(h/2),
            xc + int(w/2),
            yc + int(h/2))

    def get_w(self):
        return self.x1 - self.x0 + 1

    def get_h(self):
        return self.y1 - self.y0 + 1

    def get_CWH(self):
        w = self.x1 - self.x0 + 1
        h = self.y1 - self.y0 + 1
        return (
            self.x0 + int(w/2),
            self.y0 + int(h/2),
            w,
            h)

    def serialize(self):
        lst = []
        lst.append('%d' % self.x0)
        lst.append('%d' % self.y0)
        lst.append('%d' % self.x1)
        lst.append('%d' % self.y1)
        return ','.join(lst)

    @classmethod
    def unserialize(cls,src):
        lst = src.split(',')
        return Reg(
            int(lst[0]),
            int(lst[1]),
            int(lst[2]),
            int(lst[3]))
        

    def expand(self,rx):
        # expand self to include rx
        self.x0 = min(self.x0,rx.x0)
        self.x1 = max(self.x1,rx.x1)
        self.y0 = min(self.y0,rx.y0)
        self.y1 = max(self.y1,rx.y1)

    def xlate(self,xdelta,ydelta):
        self.x0 += xdelta
        self.y0 += ydelta
        self.x1 += xdelta
        self.y1 += ydelta

    def xlate_scale(self,xdelta,ydelta,scale):
        self.x0 = int( (self.x0 + xdelta)*scale )
        self.y0 = int( (self.y0 + ydelta)*scale )
        self.x1 = int( (self.x1 + xdelta)*scale )
        self.y1 = int( (self.y1 + ydelta)*scale )

    def _print(self):
        print ("(%d %d %d %d) ixrow:%d ixcol:%d" %\
                (self.x0,self.y0,self.x1,self.y1,self.ixrow,self.ixcol))

MIN_PIX_FG = 10
EPSILON = 20

def dump_regs(regs):
    # test/dev: dump list of regions
    for i in range(0,len(regs)):
        print('%d. %s' % (i,regs[i]))

def get_bb_content(w,h,pix,margin=10):
    # get bounding box for content of image. "pix" is the loaded 
    # gray-scale version of the image.
    def is_bg_row(pix,x0,x1,y):
        (lb,ub) = (x0, min(x0+6,x1))
        _sum = 0.0
        for x in range(lb,ub+1):
            _sum += pix[x,y]
        v_bg = int (_sum / (ub-lb+1))
        n_pix_fg = 0
        for x in range(x0,x1+1):
            if abs(pix[x,y] - v_bg) > EPSILON:
                n_pix_fg += 1
                if n_pix_fg > MIN_PIX_FG:
                    break
        return n_pix_fg <= MIN_PIX_FG

    def is_bg_col(pix,y0,y1,x):
        (lb,ub) = (y0, min(y0+6,y1))
        _sum = 0.0
        for y in range(lb,ub+1):
            _sum += pix[x,y]
        v_bg = int (_sum / (ub-lb+1))
        n_pix_fg = 0
        for y in range(y0,y1+1):
            if abs(pix[x,y] - v_bg) > EPSILON:
                n_pix_fg += 1
                if n_pix_fg > MIN_PIX_FG:
                    break
        return n_pix_fg <= MIN_PIX_FG

    (x0,y0,x1,y1) = (0,0,w-1,h-1)
    while is_bg_row(pix,x0,x1,y0) and y0+1 < h:
        y0 += 1
    while is_bg_row(pix,x0,x1,y1) and y1-1 >= 0:
        y1 -= 1
    while is_bg_col(pix,y0,y1,x0) and x0+1 < w:
        x0 += 1
    while is_bg_col(pix,y0,y1,x1) and x1-1 >= 0:
        x1 -= 1
    x0 -= margin
    y0 -= margin
    x1 += margin
    y1 += margin
    x0 = max(0,x0)
    y0 = max(0,y0)
    x1 = min(x1,w-1)
    y1 = min(y1,h-1)
    return (x0,y0,x1,y1)

def make_bg_transp_reg(im_pil,r):
    # make image background transparent in the region "r".
    pix = im_pil.convert('L').load()
    im_pil = im_pil.convert('RGBA')
    (x0,y0,x1,y1) = (r.x0, r.y0, r.x1, r.y1)
    # make left & right sides of row transparent
    N = 6 # sample size
    for y in range(r.y0, r.y1):
        # left side
        _sum = 0.0
        for x in range(x0,x0+N):
            _sum += pix[x,y]
        v_bg = int(float(_sum)/N)
        for x in range(x0, x1):
            if abs(pix[x,y] - v_bg) > EPSILON:
                break
            im_pil.putpixel((x,y),(255,255,255,0))
        # right side
        _sum = 0.0
        for x in range(x1-N,x1):
            _sum += pix[x,y]
        v_bg = int(float(_sum)/N)
        for x in range(x1-1,0,-1):
            if abs(pix[x,y] - v_bg) > EPSILON:
                break
            im_pil.putpixel((x,y),(255,255,255,0))
    # make top and bottom of colums transparent
    for x in range(x0,x1):
        # top
        _sum = 0.0
        for y in range(y0,y0+N):
            _sum += pix[x,y]
        v_bg = int(float(_sum)/N)
        for y in range(y0,y1):
            if abs(pix[x,y] - v_bg) > EPSILON:
                break
            im_pil.putpixel((x,y),(255,255,255,0))
        # bottom
        _sum = 0.0
        for y in range(y1-N,y1):
            _sum += pix[x,y]
        v_bg = int(float(_sum)/N)
        for y in range(y1-1,0,-1):
            if abs(pix[x,y] - v_bg) > EPSILON:
                break
            im_pil.putpixel((x,y),(255,255,255,0))
    return im_pil

def make_bg_transp(img_pil):
    # make image background transparent. This is for cell matricies:
    # we split img_pil into regions, call make_bg_transp_reg
    # on each one, then rebuild img_pil.
    # find the regions
    im = Im(None, trim=True, img_pil=img_pil)
    regs = get_regions(im)
    for r in regs:
        make_bg_transp_reg(im.im_pil,r)
    #im.im_pil.show()
    return im.im_pil

class Im:
    def __init__(self,fn,trim=False,img_pil=None):
        self.fn = fn
        if img_pil is None:
            self.im_pil = Image.open(fn)
        else:
            self.im_pil = img_pil
        self.pix = self.im_pil.convert('L').load()
        if trim:
            # trim margins
            bb = get_bb_content(*self.im_pil.size,self.pix)
            self.im_pil = self.im_pil.crop(bb)
            self.pix = self.im_pil.convert('L').load()
        (self.w,self.h) = self.im_pil.size

def purge_small_regs(regs):
    # purge small regions from a list of regions: test is
    # based on MIN_GUTTER.
    _regs = []
    for r in regs:
        if r.get_w() > MIN_GUTTER and r.get_h() > MIN_GUTTER:
            _regs.append(r)
    return _regs

def split_vert(im,src):
    # split region "rsrc" along vertical gutters
    regs = []
    cur_r = None
    for x in range(src.x0,src.x1+1):
        _sum = 0
        ub = min(src.y0 + 6,src.y1)
        for y in range(src.y0,ub+1):
            _sum += im.pix[x,y]
        v_bg = int (_sum / (ub-src.y0 +1))
        n_pix_fg = 0
        for y in range(src.y0,src.y1+1):
            if abs(im.pix[x,y] - v_bg) > EPSILON:
                n_pix_fg += 1
                if n_pix_fg > MIN_PIX_FG:
                    break
        if n_pix_fg < MIN_PIX_FG:
            # a white line
            cur_r = None
        else:
            if cur_r is None:
                cur_r = Reg(x,src.y0,x,src.y1)
                # save v_bg: it's needed when we re-split
                cur_r.v_bg = v_bg
                regs.append(cur_r)
            else:
                cur_r.x1 = x
    # purge small regions
    regs = purge_small_regs(regs)
    if len(regs) == 0:
        return [src]
    # assign col indices
    for i in range(0,len(regs)):
        regs[i].ixcol = i
    return regs

def purge_small_regs(regs):
    _regs = []
    for r in regs:
        if r.get_w() > MIN_GUTTER and r.get_h() > MIN_GUTTER:
            _regs.append(r)
    return _regs

def split_hor(im,src):
    # split region "src" along horizontal gutters
    regs = []
    cur_r = None
    for y in range(src.y0,src.y1+1):
        if src.v_bg is None:
            _sum = 0
            ub = min(src.x0 + 6,src.x1)
            for x in range(src.x0,ub+1):
                _sum += im.pix[x,y]
            v_bg = int (_sum / (ub-src.x0 +1))
        else:
            v_bg = src.v_bg
        n_pix_fg = 0
        for x in range(src.x0,src.x1+1):
            if abs(im.pix[x,y] - v_bg) > EPSILON: 
                n_pix_fg += 1
                if n_pix_fg > MIN_PIX_FG:
                    break
        if n_pix_fg < MIN_PIX_FG:
            # a white line
            cur_r = None
        else:
            if cur_r is None:
                cur_r = Reg(src.x0,y,src.x1,y)
                regs.append(cur_r)
                # save v_bg: it's needed when we re-split
                cur_r.v_bg = v_bg
            else:
                cur_r.y1 = y
    # purge small regions
    regs = purge_small_regs(regs)
    if len(regs) == 0:
        return [src]
    # assign row indices
    for i in range(0,len(regs)):
        regs[i].ixrow = i
    return regs

def get_regions(im):
    r = Reg(0,0,im.w-1,im.h-1)
    panel_regs = []
    cols = split_vert(im,r)
    if len(cols) > 1:
        for ixcol in range(0,len(cols)):
            regs = split_hor(im,cols[ixcol])
            for ixrow in range(0,len(regs)):
                r = regs[ixrow]
                r.ixcol = ixcol
                r.ixrow = ixrow
                panel_regs.append(r)
    else:
        rows = split_hor(im,r)
        for ixrow in range(0,len(rows)):
            regs = split_vert(im,rows[ixrow])
            for ixcol in range(0,len(regs)):
                r = regs[ixcol]
                r.ixcol = ixcol
                r.ixrow = ixrow
                panel_regs.append(r)
    # sort
    for i in range(0,len(panel_regs)-1):
        for j in range(i+1,len(panel_regs)):
            rI = panel_regs[i]
            rJ = panel_regs[j]
            if ((rJ.ixrow < rI.ixrow) or 
                ((rJ.ixrow == rI.ixrow) and
                    (rJ.ixcol < rI.ixcol))):
                panel_regs[i] = rJ
                panel_regs[j] = rI

    return panel_regs

def reformat_image(max_w,max_h,im,force_to_fit=False):
    # reformat image so it fits inside (0,0,max_w-1,max_h-1).
    # If force_to_fit is False, and image fits as is,
    # just return copy of original. If force_to_for is True,
    # resize image so max dim (width or height) equals dim.
    (src_w,src_h) = im.size
    max_w -= 1
    max_h -= 1
    if (src_w <= max_w) and (src_h <= max_h) and not force_to_fit:
        return im.copy()
    scale_w = max_w/float(src_w) 
    scale_h = max_h/float(src_h) 
    if scale_w < 1.0 or scale_h < 1.0:
        # need to shrink
        scale = min(scale_w,scale_h)
    else:
        # need to expand
        scale = max(scale_w,scale_h)
    return im.resize( (int(scale*src_w), int(scale*src_h)) )

def write_panels(fpsrc,fnbase,outdir,im_recs,
    dim=1200, trim_mar=False, split_pan=False, margin=16):
    # the file "fsrc" is broken into 1 or more regions, each
    # becomes a panel.
    # options are:
    # "dim": max panel dimension (w or h)
    # "trim_mar": trim margins
    # "split_pan": split panels
    # "margin": margin around spit panels
    if split_pan:
        im = Im(fpsrc, True)
        regs = get_regions(im)
    else:
        # use whole image
        im = Im(fpsrc, trim_mar)
        regs = [Reg(0,0,im.w-1,im.h-1)]
    # write out the images
    for i in range(0,len(regs)):
        r = regs[i]
        # get file path
        if len(regs) == 1:
            fn = fnbase
        else:
            fn = '%s.%d' % (fnbase,(i+1))
        path = '%s/%s.png' % (outdir,fn)
        # get img for region
        # TODO: margin
        (x0,y0,x1,y1) = (r.x0,r.y0,r.x1,r.y1)
        r_im = im.im_pil.crop((x0,y0,x1,y1))
        r_im = reformat_image(dim,dim,r_im)
        r_im.save(path)
        # create an image rec
        (im_w, im_h) = r_im.size
        im_recs.append( (fn,im_w,im_h) )

def write_cells(img_pil,fnbase,outdir,max_dim_cell):
    # find the regions
    im = Im(None, trim=False,img_pil=img_pil)
    regs = get_regions(im)
    # write out the cells. We return a list of filenames 
    fnames = []
    for i in range(0,len(regs)):
        r = regs[i]
        # get file path
        if len(regs) == 1:
            fn = fnbase
        else:
            fn = '%s.%d' % (fnbase,(i+1))
        path = '%s/%s.png' % (outdir,fn)
        fnames.append(fn)
        # get image for region and resize as needed
        (x0,y0,x1,y1) = (r.x0,r.y0,r.x1,r.y1)
        # This gets rid of some hard-to-reproduce errors
        (x0,y0) = (x0+3,y0+3)
        (x1,y1) = (x1-3,y1-3)
        img = im.im_pil.crop((x0,y0,x1,y1))
        #img = reformat_image(max_dim_cell,max_dim_cell, img)
        img.save(path)
        img.close()
    return fnames

def get_resample_rect(w_src,h_src,w_dst,h_dst):
    # We have 2 images, "src" and "dst". Given their dimensions, compute
    # the resample rectangle (x0,y0,x1,y1) needed to resample src to give
    # dst.
    ar_src = h_src/float(w_src)
    ar_dst = h_dst/float(w_dst)
    if ar_dst <= ar_src:
        # dst is wide relative to src
        w_samp = w_src
        h_samp = int(w_samp * ar_dst)
        x0 = 0
        y0 = int( (h_src - h_samp)/2 )
        x1 = w_src
        y1 = y0 + h_samp
    else:
        # dst is tall relative to src
        h_samp = h_src
        w_samp = int( h_samp/ar_dst )
        x0 = int( (w_src - w_samp)/2 )
        x1 = x0 + w_samp
        y0 = 0
        y1 = h_samp
    return(x0,y0,x1,y1)


def reformat_mp4(fpin,fpout,w_dst,h_dst,post_status=None):
    writer = imageio.get_writer(fpout, fps = 24, mode='I')
    reader = imageio.get_reader(fpin)
    ixfrm_in = -1
    ixfrm_out = -1
    dat_in = reader.get_meta_data()
    (w_src,h_src) = dat_in['size']
    # set scale for fps
    fps_src = float(dat_in['fps'])
    fps_scale = 24.0/fps_src
    # compute number of frames in dst 
    nfrms_src = dat_in['duration']*fps_src
    nfrms_dst = int(nfrms_src * fps_scale)
    # We resample src images to accord with dst aspect ration,
    # then resize the result
    (x0,y0,x1,y1) = get_resample_rect(w_src,h_src,w_dst,h_dst)
    for image in reader.iter_data():
        ixfrm_in += 1
        ixfrm_scaled = int(ixfrm_in * fps_scale)
        if ixfrm_scaled > ixfrm_out:
            # include this frame
            ixfrm_out = ixfrm_scaled
            # get the image as pil
            img_pil = Image.fromarray(image)
            # resample
            img_pil = img_pil.crop((x0,y0,x1,y1))
            # resize
            img_pil = img_pil.resize((w_dst,h_dst), Image.ANTIALIAS)
            writer.append_data( numpy.asarray(img_pil))
            if post_status is not None and ixfrm_out % 10 == 0:
                post_status(
                    'wrote frame %d of ~ %d' % (ixfrm_out,nfrms_dst))
            # start dev code
            #if ixfrm_out > 200:
                #break
            # end dev code
    reader.close()
    writer.close()
    if post_status is not None:
        post_status('')

def merge_mp4s(fpout,fpinlst,post_status):
    writer = imageio.get_writer(fpout, fps = 24, mode='I')
    for fpvid in fpinlst:
        reader = imageio.get_reader(fpvid)
        (head,tail) = os.path.split(fpvid)
        fnroot = tail[:-4]
        #`dat = reader.get_meta_data()
        nframes_in = 0
        for image in reader.iter_data():
            nframes_in += 1
            writer.append_data(image)
            if post_status is not None:
                if nframes_in % 20 == 0:
                    post_status(
                        'wrote frame %d of "%s"' % (nframes_in,fnroot))
        reader.close()
    writer.close()
    post_status('')

def cut_mp4(fpin,fpout,S,E,post_status=None):
    # cut clip region S..E (inclusive)
    reader = imageio.get_reader(fpin)
    writer = imageio.get_writer(fpout, fps = 24, mode='I')
    ix = 0
    for image in reader.iter_data():
        if (ix >= S and ix <= E):
            if post_status is not None:
                if (ix+1) % 20 == 0:
                    post_status("Cut frame %d" % (ix+1))
        else:
            writer.append_data(image)
            if post_status is not None:
                if (ix+1) % 20 == 0:
                    post_status("Retained frame %d" % (ix+1))
        ix += 1
    reader.close()
    writer.close()
    post_status('')

# Lighting. We implement lighting by pasting a black mask (with varying
# alpha) onto the frame images.

def get_default_lighting():
    return {
            'kind':'ambient',
            'xor':.5,
            'yor':.5,
            'brightness':0.1,
            'floor':0.0,
            'radius':0.5,
            'color':'#000000',
            'lock_to_cam': False
            }

def make_lighting_mask(w_im,h_im, params):
    (kind,xor,yor,brightness,floor,radius,color) = (
        params['kind'],
        params['xor'],
        params['yor'],
        params['brightness'],
        params['floor'],
        params['radius'],
        params['color']
    )
    
    (r,g,b) = colors.hexcode_to_bytes(color)
    mask = Image.new("RGBA", (w_im, h_im), (r,g,b,0))
    pix = mask.load()

    for y in range(0,h_im):
        for x in range(0,w_im):

            # normalize x value by im.w
            _x = x/w_im
            d = abs(_x - xor)

            if kind == 'spotlight':
                _y = y/h_im
                d = math.sqrt((_x-xor)**2 + (_y-yor)**2)

            # "brightness" in constrained to the range 0 .. 1.0
            if brightness > 1:
                brightness = 1.0

            if d < brightness*radius:
                alpha = 0.0
            elif d >= radius:
                alpha = 1.0
            else:
                alpha = (d - brightness*radius)/(radius - brightness*radius)

            if alpha > 1 - floor:
                alpha = 1 - floor

            alpha = int(alpha * 255)
            if alpha < 0:
                alpha = 0
            elif alpha > 255:
                alpha = 255

            pix[x,y] = (r,g,b,alpha)
    return mask

def ut_reg():
    img = Image.new("RGB",(200,200))
    draw = ImageDraw.Draw(img)
    pts = ((50,50), (150,50), (150,150), (50,150))

    def draw_reg(xc,yc):
        dim = 30
        (x0,y0,x1,y1) = (
            xc-int(dim/2),
            yc-int(dim/2),
            xc+int(dim/2),
            yc+int(dim/2))
        draw.rectangle((x0,y0,x1,y1),fill='red')

        dim = 15
        (x0,y0,x1,y1) = (
            xc-int(dim/2),
            yc-int(dim/2),
            xc+int(dim/2),
            yc+int(dim/2))
        draw.ellipse((x0,y0,x1,y1),fill='yellow')

    for p in pts:
        draw_reg(*p)

    #img.show()
    img.save('x.png')
    write_cells(img,'x','.',600)

if __name__ == '__main__':
    ut_reg()





