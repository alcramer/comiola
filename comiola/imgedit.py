import numpy
import math
from PIL import Image,ImageDraw
import colors
import polygon

# Here are functions called by "uiimgedit" to implement image editing.

def get_bb_content(src, SAME_COLOR_PARAM, margin=0):
    # get bounding-box for content in image
    # (if margin is specified, expand the box by margin)
    def is_bg_row(y):
        for x in range(0,w):
            if abs(pix[x,y] - bgcolor) > SAME_COLOR_PARAM:
                return False
        return True
    def is_bg_col(x):
        for y in range(0,h):
            if abs(pix[x,y] - bgcolor) > SAME_COLOR_PARAM:
                return False
        return True
    gray = src.convert('L')
    (w,h) = gray.size
    pix= gray.load()
    bgcolor = pix[0,0]
    (x0,y0,x1,y1) = (0,0,w-1,h-1)

    while is_bg_row(y0) and y0+1 < h:
        y0 += 1
    while is_bg_row(y1) and y1-1 >= 0:
        y1 -= 1
    while is_bg_col(x0) and x0+1 < w:
        x0 += 1
    while is_bg_col(x1) and x1-1 >= 0:
        x1 -= 1
    if margin != 0:
        x0 = max(0,x0-margin)
        y0 = max(0,y0-margin)
        x1 = min(w-1,x1+margin)
        y1 = min(h-1,y1+margin)
    return (x0,y0,x1,y1)

# These are boolean constants, used as values in an 8-bit color image
UNVISITED = 0
VISITED = 255

def visit_image(src,onvisit):
    # Visit each pixel in image, calling "onvisit(x,y)"
    (w,h) = src.size
    pix = src.load()
    for x in range(0,w):
        for y in range(0,h):
            onvisit(x,y)

def color_region(src, startXY, SAME_COLOR_PARAM, replace_with,
        poly_pts=None):
    # A region is a set of connected points, all with the same 
    # color. This function visits all pts in the region containing
    # startXY, replacing each pixel with "replace_with" (a
    # 4 tuple: (r,g,b,alpha), each ranging 0..255).
    
    (w,h) = src.size
    pix = src.load()

    def onvisit(x,y):
        pix[x,y] = replace_with

    visit_region(src, startXY, SAME_COLOR_PARAM, onvisit,
        poly_pts=poly_pts)

def visit_region(src, startXY, SAME_COLOR_PARAM, onvisit,
        poly_pts=None):
    # A region is a set of connected points, all with the same 
    # color. This function visits all pts in the region containing
    # startXY and calls "onvisit(x,y"
    
    (w,h) = src.size
    pix = src.load()

    def visit(x,y):
        visited[x,y] = VISITED
        onvisit(x,y)
        trans.append( (x,y) )

    bgcolor = pix[startXY[0], startXY[1]]
    visited_pil = Image.new('L',(w,h),color=UNVISITED)

    # if region is constrained by a polygon, draw edges
    # as visited.
    if poly_pts is not None:
        # need closing pt
        poly_pts.append(poly_pts[0])
        ImageDraw.Draw(visited_pil).line(poly_pts, fill=VISITED, width=2)

    visited = visited_pil.load()
    # mark all pix that don't match bg color as "visited"
    for x in range(0,w):
        for y in range(0,h):
            v = pix[x,y]
            if (abs(v[0]-bgcolor[0]) > SAME_COLOR_PARAM or
                abs(v[1]-bgcolor[1]) > SAME_COLOR_PARAM or
                abs(v[2]-bgcolor[2]) > SAME_COLOR_PARAM):
                visited[x,y] = VISITED 

    if poly_pts is not None:
        # ensure line segments are joined
        for p in poly_pts:
            visited[p[0],p[1]] = VISITED

    trans = []
    visit(*startXY)

    # visit all pix in "trans", creating a new version
    # consisting of their unvisted neighbors.
    # Interate untill trans is empty
    while len(trans) > 0:
        trans_old = trans
        trans = []
        for pt in trans_old:
            x,y = pt
            # visit all unvisited neighbors of pt
            if  x-1 >= 0 and visited[x-1,y] == UNVISITED:
                visit(x-1,y)
            if  x+1 < w and visited[x+1,y] == UNVISITED:
                visit(x+1,y)
            if  y-1 >= 0 and visited[x,y-1] == UNVISITED:
                visit(x,y-1)
            if  y+1 < h and visited[x,y+1] == UNVISITED:
                visit(x,y+1)

def texture_region(img, startXY, SAME_COLOR_PARAM, 
    SCALE_PARAM, SATURATION_PARAM, t_img, poly_pts=None):

    def onvisit(x,y):
        try:
            (xmax,ymax) = t_img.size
            xmax -= 1
            ymax -= 1
            v = t_pix[(x) % xmax, (y) % ymax]
            # Do we accept this pixel? Convert to grayscale and
            # accept if value in lower half of range. The conversion
            # is taken from a net-posting: supposedly takes sensity of
            # eye to red vs green vs blue.
            v_gray = .299*v[0] + .587*v[1] + .114*v[2]
            if v_gray < 128:
            #if v[3] > 128:
                _v = (graymap[v[0]],
                        graymap[v[1]],
                        graymap[v[2]],
                    v[3])
                img.putpixel((x,y),_v)
        except IndexError:
            pass

    # scale texture img so it covers the dst image
    (w_dst,h_dst) = img.size
    (w_src,h_src) = t_img.size
    wsc = float(w_dst)/w_src
    hsc = float(h_dst)/h_src
    sc = max(wsc,hsc)
    # adjust by user-specified scaling
    sc *= SCALE_PARAM
    t_img = t_img.resize((int(w_src*sc), int(h_src*sc)), Image.ANTIALIAS)
    t_pix = t_img.convert('RGBA').load()
    # create grayscale mapping. SATURATION param of .5 means
    # a black pixel maps to medium gray, .75 means it maps to a
    # darker gray, etc.
    graymap = [0] * 256
    for v in range(0,256):
        _v = int(255 - SATURATION_PARAM*(255-v))
        if _v < 0:
            _v = 0
        elif _v > 255:
            _v = 255
        graymap[v] = _v
    # visit region, transfering non-transparent pixels from
    # t_img to img.
    visit_region(img, startXY, SAME_COLOR_PARAM,onvisit,poly_pts)

def create_cell_matrix( w_dst, h_dst, n_rows, n_cols):
    # get dst image
    img_dst = Image.new("RGBA", (w_dst,h_dst),'#000000')
    # compute w & h for cells
    # "WG" is width of gutter
    WG = 30
    w_cell = (w_dst - (n_cols +1)*WG)/float(n_cols)
    h_cell = (h_dst - (n_rows +1)*WG)/float(n_rows)
    w_cell = int(w_cell)
    h_cell = int(h_cell)
    # get image for cell: a white rectange
    img_cell = Image.new("RGBA", (w_cell,h_cell),'#ffffff')
    # paste img_cell 
    y = WG
    layout = [] 
    for ixrow in range(0,n_rows):
        x = WG
        for ixcol in range(0,n_cols):
            img_dst.paste(img_cell,(int(x),int(y)),mask=img_cell)
            xc = x+int(w_cell/2)
            yc = y+int(h_cell/2)
            layout.append((xc,yc,w_cell,h_cell))
            x += (w_cell + WG)
        y += (h_cell + WG)
    return (img_dst,layout)

