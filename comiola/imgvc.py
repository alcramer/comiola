# Copyright Al Cramer 2021 ac2.71828@gmail.com
from PIL import Image, ImageTk, ImageDraw

# An ImageViewCntrl displays an image, plus a set of Handles.
# The image can be dragged & zoomed in/out. The view-control takes
# care of converting between display-canvas coords and image coordinates.
#
# A Handle is a small colored shape that the user
# can click (to select) and drag. Each handle has a "cntrl" (controller)
# attribute. When a handle "h" is selected, the ImgViewCntrl calls
# "h.cntrl.on_h_selected" . When a handle is dragged, the
# ImgViewCntrl calls "h.cntrl.on_h_move" . These "on_h_xxx"
# functions are responsible for interpreting user gestures and
# modifying the Model (which is the currently opened Script). After
# calling these functions, an ImgViewCntrl calls its "draw" function
# to re-draw the image. This function calls a "draw_content" function
# (passed in when the ImgViewCntrl is created): that function is implemented
# by the ImgViewCntrl's client, and will draw the various controls on
# top of the image.
#
# To override the default mouse event handling, set the
# client_mouse(down|move|up) attributes. To restore the default
# handling, set these attributes to None.
#
# Details: 
# The display (module display.py) creates an ImgViewCntrl
# to display the shot and edit controls. It provides the Tkinter 
# canvas used by its ImgViewCntrl. Tkinter mouse events are bound
# to methods in the display, which passes them on to its ImgViewCntrl
# for handling.
#
# The image editor (module uimgedit.py) also creates an ImgViewCntrl
# to display images and controls. This works just like the display:
# Tkinter mouse events are bound to methods in uiimgedit.py, which
# passes them on to its ImgViewCntrl for handling.

# dimension for contrl handle (actually 1/2 the width in pixels)
HANDLE_DIM = 6

class ImgViewCntrl:
    def __init__(self,can,w_img,h_img,draw_content,
        on_h_selected=None,
        on_drag_end=None,
        bg_color=(0x45,0x45,0x45),
        MAXDIM_VIEW=600):
        self.can = can
        self.w_img = w_img
        self.h_img = h_img
        self.draw_content = draw_content
        self.on_h_selected = on_h_selected
        self.on_drag_end = on_drag_end
        self.bg_color = bg_color
        self.MAXDIM_VIEW = MAXDIM_VIEW
        # scale factor, image coord's to canvas coords. Init
        # so image has specified max dimension
        self.scale_im_to_can = float(MAXDIM_VIEW)/max(w_img, h_img)
        # initial offsets: want top left corner at (20,20)
        self.xoff = 20
        self.yoff = 20

        # PIL image we blit to canvas, and static ref required by tkinter
        self.img_dst = None
        self.img_tk = None
        # PIL draw object for img_dst
        self.draw_pil = None

        # the handles
        self.handles = []
        # selected handle
        self.h_selected = None

        # Drag operations
        # handle we're dragging
        self.drag_h = None
        # mouse (x,y) at drag start (canvas coords)
        self.drag_xyS = None
        # (xoff,yoff) at start of drag
        self.offsetS = None

        # client mouse down/move/up handlers: if non null,
        # they override the standard handlers
        self.client_mousedown = None
        self.client_mousemove = None
        self.client_mouseup = None

    # reinit scale
    def reinit_scale(self,w_img,h_img):
        (self.w_img, self.h_img) = (w_img,h_img)
        self.scale_im_to_can = float(self.MAXDIM_VIEW)/max(w_img, h_img)
        self.xoff = 20
        self.yoff = 20

    # canvas coords -> image coords
    def to_im_coords(self,x_can,y_can):
        s = self.scale_im_to_can
        return (
            int( (x_can - self.xoff)/s ),
            int( (y_can - self.yoff)/s ),
            )

    # image coords-> canvas coord
    def from_im_coords(self,x_im,y_im):
        s = self.scale_im_to_can
        return (
            int( x_im*s + self.xoff ),
            int( y_im*s + self.yoff ),
            )

    # Draw primitives. These accept image coordinates, and convert
    # them into canvas coords.

    def xlate_pts(self,pts):
        _pts = []
        for i in range(0,int(len(pts)/2)):
            _pts.extend(self.from_im_coords(pts[2*i],pts[2*i+1]))
        return _pts

    # draw a line, as defined by a set of 2 or more points. 
    # "pts" is the set (x0,y0,x1,y1,x2,y2,...). We're using PIL
    # nomenclature here: this function might be more accurately named
    # "draw set of one or more connected line segements".
    def draw_line(self,color,pts,width=2):
        self.draw_pil.line(self.xlate_pts(pts), fill=color, width=width)

    # draw a rectangle: x0,y0,x1,y1 are top-left, lower-right coord's
    def draw_rect(self,color,x0,y0,x1,y1,width=2):
        self.draw_line(color,
            (x0,y0, x1,y0, x1,y1, x0,y1, x0,y0),
            width=width)

    # draw a polygon, as defined by 3 or more points.
    # "pts" is the set (x0,y0,x1,y1,x2,y2,...). 
    def draw_poly(self,color,pts,width=2):
        # add closing line-segment
        _pts = []
        _pts.extend(pts)
        _pts.append(pts[0])
        _pts.append(pts[1])
        self.draw_line(color,_pts,width=width)

    # draw a handle. shape is "rect", "tri", or "oval"
    # x,y are in image coords.
    def draw_handle(self,x,y,color,shape):
        dim = HANDLE_DIM
        (x,y) = self.from_im_coords(x,y)
        (x0,y0) = (int(x - dim), int(y - dim))
        (x1,y1) = (int(x + dim), int(y + dim))
        if shape == 'rect':
            self.draw_pil.rectangle((x0,y0,x1,y1),fill=color)
        elif shape == 'tri':
            (x0,y0) = (x - dim, y + dim)
            (x1,y1) = (x, y - dim)
            (x2,y2) = (x + dim, y + dim)
            self.draw_pil.polygon((x0,y0,x1,y1,x2,y2),fill=color)
        else:
            self.draw_pil.ellipse((x0,y0,x1,y1),fill=color)

    def draw_filled_rect(self,bb,color):
        self.draw_pil.rectangle(self.xlate_pts(bb), fill=color)


    def draw_text(self,color,x,y,text,font):
        (x,y) = self.from_im_coords(x,y)
        self.draw_pil.text((x,y),text,fill=color,font=font)

    # paste img: (x,y) are center coord's
    def paste_img(self,img,x,y):
        (x,y) = self.from_im_coords(x,y)
        (w,h) = img.size
        w = int(w*self.scale_im_to_can)
        h = int(h*self.scale_im_to_can)
        img = img.resize((w,h),Image.ANTIALIAS)
        (x0,y0) = (
            int(x - w/2), int(y - h/2)
        )
        self.img_dst.paste(img,(x0,y0),mask=img)

    def draw(self):
        # 'img_dst' is our drawing of the view. It's the size
        # of the canvas.
        c = self.can
        c.update()
        w_can = c.winfo_width()
        h_can = c.winfo_height()
        self.img_dst = img_dst = Image.new("RGB",(w_can,h_can), self.bg_color)
        self.draw_pil = ImageDraw.Draw(img_dst)

        # draw content onto img_dst. False means no content drawn
        if self.draw_content():
            self.can.delete("all")
            self.img_tk = ImageTk.PhotoImage(img_dst,master=self.can)
            self.can.create_image(0,0,
                    anchor='nw',image=self.img_tk)
        else:
            self.can.delete("all")
        img_dst.close()

    def zoomin(self):
        self.scale_im_to_can += .05
        self.xoff = int(.5 + self.xoff*1.05)
        self.yoff = int(.5 + self.yoff*1.05)
        self.draw()

    def zoomout(self):
        self.scale_im_to_can -= .05
        self.xoff = int(.5 + self.xoff/1.05)
        self.yoff = int(.5 + self.yoff/1.05)
        self.draw()

    def add_handle(self,h):
        self.handles.append(h)

    def remove_cntrl(self,c):
        _handles = []
        for h in self.handles:
            if h.cntrl != c:
                _handles.append(h)
        self.handles = _handles

    # hit test for a handle. x & y should be image-coords.
    def is_hit(self,h,x,y):
        hd = HANDLE_DIM
        return (
            h.visible and
            (x >= h.x - hd) and
            (x <= h.x + hd) and
            (y >= h.y - hd) and
            (y <= h.y + hd))

    def on_mousedown(self,ev):
        # set drag-start values
        self.drag_xyS = (ev.x, ev.y)
        self.offsetS = (self.xoff,self.yoff)
        (x,y) = self.to_im_coords(ev.x,ev.y)
        h = self.h_selected
        if h is not None and self.is_hit(h,x,y):
            # duplicate selection event: tell contrl and setup for 
            # drag handling.
            try:
                h.cntrl.on_h_selected(h)
            except AttributeError:
                pass
            self.drag_h = h
            return

        h = None
        for hx in self.handles:
            if self.is_hit(hx,x,y):
                # See note on "hand_z" in module controls.py
                if h is None or hx.z > h.z:
                    h = hx
        if h is not None:
            # selection event
            self.h_selected = h
            # setup for drag-handling
            self.drag_h = h
            if self.on_h_selected is not None:
                self.on_h_selected(h)
            try:
                h.cntrl.on_h_selected(h)
            except AttributeError:
                pass
            return

        # a deselection event?
        if self.h_selected is not None:
            self.h_selected = None
            self.last_h_selected = None
            if self.on_h_selected is not None:
                self.on_h_selected(None)
            try:
                h.cntrl.on_h_selected(None)
            except AttributeError:
                pass
        # pass to client if handler is defined.
        if self.client_mousedown is not None:
            self.client_mousedown(x,y)

    def on_mouseup(self,ev):
        if self.drag_h is not None:
            if self.on_drag_end is not None:
                self.on_drag_end()
            try:
                self.drag_h.cntrl.on_drag_end()
            except AttributeError:
                pass
        self.drag_h = None
        self.drag_xyS = None
        self.offsetS = None
        # pass to client if handler is defined.
        if self.client_mouseup is not None:
            (x,y) = self.to_im_coords(ev.x,ev.y)
            self.client_mouseup(x,y)

    def on_mousemove(self,ev):
        if self.drag_xyS is None:
            # not a drag (see mouseup handling)
            return
        (drag_xS,drag_yS) = self.drag_xyS
        xdelta = ev.x - drag_xS
        ydelta = ev.y - drag_yS
        sc = self.scale_im_to_can
        if abs(xdelta/sc) <= 2 and abs(ydelta/sc) <= 2:
            # this reduces lag
            return
        if self.client_mousemove is not None:
            (x,y) = self.to_im_coords(ev.x,ev.y)
            if self.client_mousemove(x,y):
                return
        drag_h = self.drag_h
        if drag_h is not None:
            # dragging a handle
            (x,y) = self.to_im_coords(ev.x,ev.y)
            (xS,yS) = self.to_im_coords(drag_xS,drag_yS)
            try:
                drag_h.cntrl.on_h_move(drag_h,x,y,xS,yS)
            except AttributeError:
                pass
        else:
            # If client doesn't handle this event, we interpret it
            # a move of the canvas.
            event_handled = False
            if self.client_mousemove is not None:
                (x,y) = self.to_im_coords(ev.x,ev.y)
                if self.client_mousemove(x,y):
                    event_handled = True
            if not event_handled:
                # moving canvas
                self.xoff = self.offsetS[0] + xdelta
                self.yoff = self.offsetS[1] + ydelta
        self.draw()

    def on_doubleclick(self,ev):
        if self.h_selected is None:
            return
        (x,y) = self.to_im_coords(ev.x,ev.y)
        hits = []
        for h in self.handles:
            if self.is_hit(h,x,y):
                hits.append(h)
        if len(hits) > 1:
            for i in range(0,len(hits)):
                if hits[i] == self.h_selected:
                    if i == len(hits) -1:
                        self.h_selected = hits[0]
                    else:
                        self.h_selected = hits[i+1]
                    h = self.h_selected
                    try:
                        h.cntrl.on_h_selected(h)
                    except AttributeError:
                        pass
                    if self.on_h_selected is not None:
                        self.on_h_selected(h)
                    break

