# Copyright Al Cramer 2023 ac2.71828@gmail.com
import tkinter as tk
import os
import sys
from PIL import ImageTk,Image,ImageDraw,ImageOps,ImageFilter
from random import random


from tkalt import *
from fxjiggle import FXjiggle

# This is a test harness for experimenying with jiggle. It runs as
# a standalone app to test the "add_jiggle" function in "xfrmjiggle.py".
# It also defines an alternative jiggle  xfrm for comparison 
# purposes.

# alternative function 
class FXjiggleAlt(FXjiggle):
    def __init__(self,speed,amount,smear):
        super().__init__(speed,amount,smear)

    def set_xfrm_params(self):
        # get rv in range -DIM .. DIM (scaled by "amount")
        def get_rv(DIM,want_int=True):
            v = 2*DIM*self.amount*random()
            v -= DIM/2
            if want_int:
                v = int(v)
            return v
        # dim's for deltas
        DIM_WH = 10
        DIM_XY = 5
        self.dw = get_rv(DIM_WH)
        self.dh = get_rv(DIM_WH)
        self.dx = get_rv(DIM_XY)
        self.dy = get_rv(DIM_XY)
    
    def to_dual(self):
        self.dw = -self.dw
        self.dh = -self.dh
        self.dx = -self.dx
        self.dy = -self.dy
 
    def xfrm(self,img_src):
        (w,h) = img_src.size
        w_adj = w + self.dw
        h_adj = h + self.dh
        img_adj = img_src.resize((w_adj,h_adj))
        #img_adj = img_adj.filter(ImageFilter.BLUR)
        (x0,y0) = ( int((w_adj-w)/2), int((h_adj-h)/2))
        img_adj = img_adj.crop((x0,y0,x0+w,y0+h)) 

        # x/y adjustment
        img_xfrm = Image.new('RGBA',img_src.size,(255,255,255,0))
        img_xfrm.paste(img_adj,(self.dx,self.dy),mask=img_adj)

        return img_xfrm

# UI for test harness

col_cntrl_fg = '#EDEDED'
col_cntrl_bg = '#303030'
col_label_fg = '#959595'

styles = {
    'menu': {
        'bg':'black',
        'fg':'white',
        'fg.label':'white',
        'bg.entry':'white',
        'fg.entry':'black'
    },
    '*': {
        'bg':col_cntrl_bg,
        'fg':col_cntrl_fg,
        'border':False,
        'bg.entry':'white',
        'fg.entry':'black',
        'bg.textbox':'white',
        'fg.textbox':'black',
        'fg.label':col_label_fg,
        'fg.hr':'#bebebe',
        'border.optionmenu':True,
        'bg.canvas':'white',
        # start dev code: enable this when creating demo videos
        #'fontsize':14,
        # end dev code
    },
}

tkalt_init('Jiggle Lab',styles)

cells = []
DIM = 300
MAR = 15

speed = 1.0
amount = 1.0

fxjig = None

class Cell:
    def __init__(self,img_src,ixrow,ixcol,label):
        self.img_src = img_src
        self.img_pil = img_src
        self.ixrow = ixrow
        self.ixcol = ixcol
        self.label = label
        (self.x,self.y) = (MAR+ixcol*(DIM+MAR), MAR+ixrow*(DIM+MAR))
        (self.w,self.h) = img_src.size
        self.ixjiggle = 0

    def draw(self,can):
        self.tk = ImageTk.PhotoImage(self.img_pil,master=can)
        can.create_image(self.x,self.y,anchor='nw',image=self.tk)
        can.create_text(int(self.x+MAR),
                self.y+DIM-15,anchor='nw',text=self.label)


def validate_view():
    can = ui.can
    can.delete('all')
    can['bg'] = 'white'

    if len(cells) == 0:
        ui.speed.set(None)
        ui.amount.set(None)
        return

    ui.speed.set(speed)
    ui.amount.set(amount)
    for c in cells:
        c.draw(can)


def on_file():
    global cells,speed,amount
    fp = askopenfilename( initialdir=None,
            filetypes=
            [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG')])
    if len(fp) == 0:
        return
    img_src = Image.open(fp)
    img_src.thumbnail((DIM,DIM))
    img_src = img_src.convert('RGBA')
    c0 = Cell(img_src,0,0,'Image')
    cells = [c0]
    speed = 1
    amount = 1

    validate_view()

def apply_play_params():
    global speed,amount
    amount = ui.amount.get()
    speed = ui.speed.get()
    if fxjig is not None:
        fxjig.set_play_params(speed,amount)

def on_reinit():
    global cells
    if len(cells) > 0:
        cells = [cells[0]]
    validate_view()

def on_step():
    global cells,fxjig
    if len(cells) == 0:
        return
    jigid = ui.jigid.get()
    if len(cells) == 1:
        c0 = cells[0]
        img_src = c0.img_pil
        c1 = Cell(img_src,0,1,'Xfrm')
        c2 = Cell(img_src,0,2,'Composite')
        cells = [c0,c1,c2]
        if jigid == 'squeezetug':
            fxjig = FXjiggle(speed,amount,True)
        else:
            fxjig = FXjiggleAlt(speed,amount,True)
        must_init = True
    else:
        must_init = False
    [c0,c1,c2] = cells
    c2.img_pil = fxjig.add_jiggle(c0.img_pil,must_init)
    c1.img_pil = fxjig.img_xfrm
    if jigid == 'dims':
        c1.label = 'Xfrm. dx: %d dy: %d dw: %d dh: %d' % (
            fxjig.dx, fxjig.dy, fxjig.dw, fxjig.dh)
    else:
        c1.label = 'Xfrm. rvert: %.2f rhor: %.2f' % (
                fxjig.rvert, fxjig.rhor)
    c2.label = 'Composite. Blend weight: %.2f' % fxjig.jig_weight
    validate_view()

halted = False
def on_tick():
    if halted:
        validate_view()
        return

    t_start = time.time()

    on_step()

    t_end = time.time()
    t_elapsed = int( 1000 * (t_end - t_start))
    msecs_frame = int(1000/24)
    t_wait = max(1, msecs_frame - t_elapsed)
    print('t_wait: %d' % t_wait)
    ui.can.after(t_wait,on_tick)

def on_play():
    global halted
    halted = False
    on_tick()

def on_halt():
    global halted
    halted = True
    validate_view()

def onchange_xfrm():
    on_halt()

Layout(None,None,w=1000,h=600,
        N = Frame(None,None,
            [[
                Button('File',on_file),
                OptionMenu('jigid',('squeezetug','dims'),"Xfrm:",
                    onchange=onchange_xfrm),
                Button('Re-init',on_reinit),
                Button('Step',on_step),
                Button('Play',on_play),
                Button('Halt',on_halt),
                RangeParamControl('speed','Speed:',width=4,
                    _format='%.2f',lb=0,hb=None,incr=0.05,
                    onchange=apply_play_params),
                RangeParamControl('amount','Amount:',width=4,
                    _format='%.2f',lb=0,hb=None,incr=0.05,
                    onchange=apply_play_params),

            ]]),
        C = Canvas('can',None,weight=1)
        ).make()


def main():
    mainloop()

if __name__ == '__main__':
    main()

