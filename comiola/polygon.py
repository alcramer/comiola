# Copyright Al Cramer 2021 ac2.71828@gmail.com
from PIL import Image, ImageDraw

# Functions for polygons (sets of (x,y) values) used by the PolyCntrl
# in controls.py

# These are boolean constants, used as values in an 8-bit color image
# They are used by the pixel-visiting code in "imgedit.py"
INSIDE_POLY = 0
OUTSIDE_POLY = 255

def get_bb(pts):
    # get bounding box for poly
    p = pts[0]
    (x,y) = (p[0],p[1])
    (bb0x,bb0y) = (x,y)
    (bb1x,bb1y) = (x,y)
    for p in pts:
        (x,y) = (p[0],p[1])
        bb0x = min(bb0x,x)
        bb0y = min(bb0y,y)
        bb1x = max(bb1x,x)
        bb1y = max(bb1y,y)
    return (int(bb0x) ,int(bb0y) ,int(bb1x) ,int(bb1y))

def draw_poly(bb0x,bb0y,bb1x,bb1y,pts):
    # create image and draw polygon. 

    # rewrite "pts" to coordinate system with origin at bb0
    _pts = []
    for p in pts:
        _pts.append( (p[0]-bb0x, p[1]-bb0y) )
    pts = _pts

    # draw the polyon: background is OUTSIDE_POLY, points 
    # inside are INSIDE_POLY
    w = int(bb1x - bb0x + 1)
    h = int(bb1y - bb0y + 1)
    img = Image.new('L', (w,h),color=OUTSIDE_POLY)
    draw = ImageDraw.Draw(img)
    draw.polygon(pts,fill=INSIDE_POLY)

    return img

def visit_poly(pts,on_visit):
    if len(pts) <= 2:
        return
    (bb0x,bb0y,bb1x,bb1y) = get_bb(pts)
    # draw filled polygon (white on black)
    img = draw_poly(bb0x,bb0y,bb1x,bb1y,pts) 
    # walk image 
    (w,h) = img.size
    pix = img.load()
    for x in range(1,w-1):
        for y in range(1,h-1):
            if pix[x,y] == 0:
                try:
                    on_visit(int(x+bb0x), int(y+bb0y))
                except IndexError:
                    pass
    img.close()

def poly_contains(pts,xytest):
    if len(pts) <= 2:
        return False
    (x,y) = xytest
    # get bounding box for points
    (bb0x,bb0y,bb1x,bb1y) = get_bb(pts)
    if ( x < bb0x + 1 or x > bb1x -1 or
        y < bb0y +1 or y > bb1y -1):
        return False
    # draw filled polygon (white on black)
    img = draw_poly(bb0x,bb0y,bb1x,bb1y,pts) 
    try:
        return img.getpixel( (x-bb0x, y-bb0y) ) == INSIDE_POLY
    except IndexError:
        return False

def ut_contains():
    pts = [ (-20,-20), (40,-30), (20,20), (-10,10) ]
    test = [
        ((-10,-15),True),
        ((5,5),True),
        ((20,21),False),
        ]
    for e in test:
        pt = e[0]
        result = e[1]
        if poly_contains(pts,pt) == result:
            print('PASS for (%d,%d)' % (pt[0],pt[1])) 
        else:
            print('FAIL for (%d,%d)' % (pt[0],pt[1])) 

def ut_visit():
    #pts = [ (10,10), (90,0), (50,50), (20,20)] 
    pts = [ (25,25), (75,25), (75,75), (25,75)] 

    def on_visit(x,y):
        print( '%d, %d' % (x,y))

    visit_poly(pts, on_visit)

#ut_visit()
#ut_contains()


