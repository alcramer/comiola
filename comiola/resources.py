# Copyright Al Cramer 2021 ac2.71828@gmail.com
import os
import shutil
import sys
import time
from PIL import Image, ImageFont, ImageDraw
from tkalt import showerror
import imageio
import images
import xfrmpersp

# Resource management. We have several different classes of
# resources, and manage them to avoid reloading where possible.
# Here's the strategy.
# comiola system resources:
#   These are images for buttons & fill patterns maintained in the
#   "res" subdirectory. These are simply loaded when first needed,
#   then re-used. The cache is a simple dictionary (mapping 
#   filename->img file) named "res_cache".
# art resources:
#   These are images uploaded by user and stored im the project 
#   directory. The cache is a circular buffer, "art_cache"
# font:
#   These are loaded fonts. The cache is a circular buffer, "font_cache".
# lighting-masks:
#   Costly to build, so we cache them in a circular bufer, "lighting_cache"

# Lengths for circular buffers
LEN_FONT_CACHE = 12
LEN_ART_CACHE = 12
LEN_LIGHTING_CACHE = 8

# smaller dim (w,h) for MP4 frame
MINDIM_MP4 = 608

# max dim (w,h) for frame in display
MAXDIM_DISPLAY = 576

proj_dir = ''
ar_shot = 1.0
w_shot = MAXDIM_DISPLAY
h_shot = MAXDIM_DISPLAY

def set_proj_params(pdir,ar):
    global proj_dir,ar_shot,w_shot,h_shot
    proj_dir = pdir
    ar_shot = ar
    if ar >= 1.0:
        # tall: make h_shot the max dim
        h_shot = MAXDIM_DISPLAY
        w_shot = int(h_shot/ar)
    else:
        # wide: make w_shot the max dim
        w_shot = MAXDIM_DISPLAY
        h_shot = int(w_shot *ar)

def get_fpart(fn):
    return os.path.join(proj_dir,fn)

def parse_fp(fp):
    # utility: path split fn to get "head" and "root" 
    (head,tail) = os.path.split(fp)
    terms = tail.split('.')
    if len(terms) == 1:
        return (head,tail,'')
    else:
        return (head,'.'.join(terms[:-1]),terms[-1])

def install_image(fn,max_dim=2000):
    # install image to project dir if not present. Return root of
    # filename
    (head,root,ext) = parse_fp(fn)
    if head != proj_dir or not fn.endswith('.png'):
        with Image.open(fn).convert('RGBA') as src:
            src = images.reformat_image(max_dim,max_dim,src)
            src.save('%s/%s.png' % (proj_dir,root))
        if head == proj_dir and not fn.endswith('.png'):
            os.remove(fn)
    clear_cache()
    return root

def get_mp4_dims():
    # get (w,h) for mp4 frames: we want smaller
    # dim to be MINDIM_MP4.
    if ar_shot <= 1:
        h = MINDIM_MP4
        w = int( h/ar_shot )
        # ffmpeg wants dim to be multiple of 16
        w = 16 * int(w/16) 
    else:
        w = MINDIM_MP4
        h = int( w*ar_shot)
        # ffmpeg wants dim to be multiple of 16
        h = 16 * int(h/16) 
    return (w,h)

def install_mp4(fpsrc,post_status):
    # install video to project dir if not present. Returns
    # (status,filename_root,w,h)
    (head,root,ext) = parse_fp(fpsrc)
    # get (w,h) for frames in the installed version
    (w_dst,h_dst) = get_mp4_dims() 
    # get dims for src
    reader = imageio.get_reader(fpsrc)
    dat = reader.get_meta_data()
    reader.close()
    (w_src,h_src) = dat['size']
    if ((w_dst==w_src and h_dst==h_src and dat['fps']==24)):
        # format is correct. Copy to project dir if not already there.
        if head != proj_dir:
            try:
                shutil.copy(fpsrc, proj_dir)
            except:
                # Coppy failed
                return (False,root,w_dst,h_dst)
        # sucess
        return (True,root,w_dst,h_dst)
    # Must reformat: write to _tmp.mp4 (in case src file is in projdir),
    # then move to dst
    fptmp = get_fpart('__tmp.mp4')
    try:
        images.reformat_mp4(fpsrc,fptmp,w_dst,h_dst,post_status)
    except RuntimeError:
        try:
            os.remove(fptmp)
        except:
            pass
        return (False,root,w_dst,h_dst)
    fpdst = get_fpart('%s.mp4' % root)
    try:
        os.remove(fpdst)
    except:
        pass
    os.rename(fptmp,fpdst)
    return (True,root,w_dst,h_dst)

def get_secs_clip(fp):
    with imageio.get_reader(fp) as reader:
        return reader.count_frames() / 24.0

# Cicular Buffer for cached resources.
class CBCache():
    def __init__(self,lenbuf):
        self.buf = [None]*lenbuf
        self.cbix = 0

    def get(self,_id):
        for e in self.buf:
            if e is not None and e[0] == _id:
                #print('cbcache: reusing %s' % _id)
                return e[1]
        #print('cbcache: no item found %s' % _id)
        return None

    def put(self,_id,item):
        ixlst = self.cbix % len(self.buf)
        if self.buf[ixlst] is not None:
            try:
                self.buf[ixlst].close()
            except AttributeError:
                pass
        self.buf[self.cbix % len(self.buf)] = [_id,item] 
        #print('installed %s' % _id)
        self.cbix += 1

# Utility: get directory for comiola asset
def get_assets_dir(kind):
    # "kind" can be:
    # 'faq' -- directory for faq
    # 'res' -- image button images, fonts, icons.
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        my_path = os.path.abspath(__file__)
        base_path = os.path.split(my_path)[0]
    return os.path.join(base_path, kind)

# Utility: get aspect ratio of an art image
def get_img_ar(fn):
    (w,h) = get_img(fn).size
    return float(h)/w

# comiola system resources.
res_cache = {}
def get_res(fn):
    img = res_cache.get(fn)
    if img is None:
        fp = os.path.join( get_assets_dir('res'), fn+'.png')
        img = Image.open(fp).convert('RGBA')
        res_cache[fn] = img
    return img

# fonts
font_cache = CBCache(LEN_FONT_CACHE)

def get_font(fontname,fontsize):
    # get font for a text element
    # .ttf file are named: "comics", "serif", and "sans". Accept 
    # various spellings for sans-serif.
    fontname = fontname.lower()
    if fontname in ['sansserif','sans-serif']:
        fontname = 'sans'
    _id = fontname+fontsize
    font =  font_cache.get(_id) 
    if font is not None:
        return font
    fontsize = int( fontsize[:-2] )
    path = os.path.join( get_assets_dir('res'), fontname + '.ttf')
    font = ImageFont.truetype(path,fontsize)
    font_cache.put(_id,font)
    return font

# lighting masks
lighting_cache = CBCache(LEN_LIGHTING_CACHE)

def get_lighting_mask(w_im,h_im, params):
    # lighting masks are generated with max dim of 608
    dim = 608
    (_w,_h) = (w_im, h_im)
    scale = dim/max(w_im,h_im)
    _w = int(w_im * scale)
    _h = int(h_im * scale)
    _id = ' '.join([
            '%d' % _w, 
            '%d' % _h, 
            '%s' % params['kind'],
            '%.2f' % params['xor'], 
            '%.2f' % params['yor'], 
            '%.2f' % params['brightness'], 
            '%.2f' % params['floor'], 
            '%.2f' % params['radius'],
            '%s' % str(params['color'])
        ])
    mask =  lighting_cache.get(_id) 
    if mask is None:
        mask = images.make_lighting_mask(_w,_h,params)
        lighting_cache.put(_id,mask)
    return mask.resize((w_im,h_im))

# user-supplied art resources (sprites & backgrounds)
art_cache = CBCache(LEN_ART_CACHE)

# get user-art image
def get_img(fn):
    img = art_cache.get(fn)
    if img is not None:
        return img
    if fn.endswith('.mp4'):
        # video file: use first frame
        try:
            with imageio.get_reader(get_fpart(fn)) as reader:
                img = Image.fromarray(reader.get_next_data())
                img = img.convert('RGBA')
                draw_pil = ImageDraw.Draw(img)
                draw_pil.rectangle((20,20,300,40),fill='black')
                draw_pil.text((24,24),
                    'Clip: %s' % fn,
                    fill='white')
        except FileNotFoundError:
            pass
    else:    
        try:
            img = Image.open( get_fpart('%s.png' % fn) ).convert('RGBA')
        except FileNotFoundError:
            # try for a jpg
            try:
                img = Image.open( get_fpart('%s.jpg' % fn) ).convert('RGBA')
            except FileNotFoundError:
                pass
    if img is None:
        # create an empty, transparent image
        img = Image.new("RGBA",(w_shot,h_shot),(255,255,255,0))
    art_cache.put(fn,img)
    return img

# get a sized sprite image. This lets us reuse images, if they match
# the requested size
spr_cache = CBCache(LEN_ART_CACHE)
def get_img_spr(fn,w,h):
    (w,h) = (int(w), int(h))
    fnqual = '%s:%d_%d' % (fn,w,h)
    img = spr_cache.get(fnqual)
    if img is not None:
        return img
    img = get_img(fn)
    img = img.resize((w,h),Image.ANTIALIAS)
    spr_cache.put(fnqual,img)
    return img

# get a user-art image, with pespective xfrm. Because the xfrm
# take time, we cache and resue where possible.
def get_img_pxfrm(fn,w,h,fall,h_pt):
    fnqual = '%s:%.2f_%.2f_%.2f' % (fn,w,h,fall)
    img = art_cache.get(fnqual)
    if img is None:
        try:
            img = Image.open( get_fpart('%s.png' % fn) ).convert('RGBA')
        except FileNotFoundError:
            # create a 100X100 white square
            img = Image.new("RGBA",(100,100),(255,255,255,0))
        img = xfrmpersp.xfrm(img, w, h, fall,h_pt)
        art_cache.put(fnqual,img)

    (w_img,h_img) = img.size
    scale = h_pt/float(h_img)
    img = img.resize( (int(w_img*scale),int(h_img*scale)) )
    return img

# called after images have been edited, to pick up the new versions.
def clear_cache():
    global art_cache,spr_cache
    art_cache = CBCache(LEN_ART_CACHE)
    spr_cache = CBCache(LEN_ART_CACHE)
