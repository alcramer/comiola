files = \
"""
uibanner.py
uiimgedit.py
uimenu.py
uipersp.py
uisequences.py
uishots.py
uitext.py
uitimeline.py
"""

class ParseFail(Exception):
    pass

class Tk:
    def __init__(self):
        self.nsp = 0
        self.v = ''
        self.isid = False

    def tostr(self):
        sp = ''
        for i in range(0,self.nsp):
            sp += ' '
        return '%s%s' % (sp,self.v)

def print_tks(tks):
    for t in tks:
        print('"%s"' % t.tostr())

def tks_to_str(tks):
    tmp = [e.tostr() for e in tks]
    return ''.join(tmp)

def tokenize(src):
    def new_tk():
        t = Tk()
        tks.append(t)
        return t

    src = list(src.rstrip())
    tks = []
    cur_tk = new_tk()
    i = 0
    while i < len(src):
        while i<len(src) and src[i] in ['\t',' ']:
            cur_tk.nsp += (4 if src[i] == '\t' else 1)
            i += 1
            continue
        if src[i].isalpha() or src[i] == '_':
            cur_tk.isid = True
            while i<len(src) and (src[i].isalnum() or src[i] == '_'):
                cur_tk.v += src[i]
                i += 1
            cur_tk = new_tk()
            continue
        cur_tk.v = src[i]
        i += 1
        cur_tk = new_tk()
    if tks[-1].v == '':
        tks = tks[:-1]
    return tks

class Decl():
    def __init__(self,classid,args):
        self.classid = classid
        self.args = args

    def printme(self):
        print('Decl. classid: %s' % self.classid)
        for a in self.args:
            print(a)
        print('')

    def rewrite(self,varid):
        if self.classid == 'Entry':
            if len(self.args) == 2:
                return "Entry('%s',%s)" % (varid,self.args[0])
            elif len(self.args) == 3:
                return "Entry('%s',%s,%s)" % (varid,self.args[0],self.args[2])
            else:
                raise ParseFail('rewrite failed for %s' % varid)
        elif self.classid == 'Lab':
            if len(self.args) == 2:
                return "Label(%s,id='%s')" % (self.args[0],varid)
            else:
                raise ParseFail('rewrite failed for %s' % varid)
        elif self.classid == 'Checkbox':
            if len(self.args) == 2:
                return "Checkbox('%s',%s)" % (varid,self.args[0])
            else:
                raise ParseFail('rewrite failed for %s' % varid)

def get_symtbl(body):
    tbl = {}
    lno = 0
    _body = []
    for li in body:
        lno += 1
        test = li.replace(' ','')
        terms = test.split('=')
        if len(terms) != 2:
            _body.append(li)
            continue
        rhs = terms[1]
        if not (
            rhs.startswith('Entry(') or
            rhs.startswith('Lab(') or
            rhs.startswith('Checkbox(')):
            _body.append(li)
            continue
        terms = li.split('=')
        varid = terms[0].strip()
        rhs = terms[1]
        ix0 = rhs.find('(')
        ix1 = rhs.rfind(')')
        if ix1 == -1:
            raise ParseFail('no closing ")" line: %d' % lno)
        classid = rhs[0:ix0].strip()
        args = rhs[ix0+1:ix1]
        args = args.split(',')
        tbl[varid] = Decl(classid,args)
    return (tbl,_body)

def print_symtbl(tbl):
    for key in tbl:
        print(key)
        tbl[key].printme()

class Doc:
    def __init__(self,fn,body,make_ui):
        self.fn = fn
        self.body = body
        self.make_ui = make_ui

    def writeme(self):
        with open('x%s' % self.fn,'w') as f:
            f.write('\n'.join(self.body))
            f.write('\ndef make_ui():\n')
            f.write('\n'.join(self.make_ui))
        print('wrote x%s' % self.fn)

def rewrite_vars(symtbl,src):
    _src = []
    for li in src:
        tks = tokenize(li)
        for t in tks:
            if t.isid and symtbl.get(t.v) is not None:
                t.v = 'ui.' + t.v
        _src.append(tks_to_str(tks))
    return _src


def rewrite_make_ui(symtbl,src,emsg_off):
    # Pass1: update old decls
    _src = []
    lno = emsg_off
    for li in src:
        lno += 1
        test = li.replace(' ','')
        if not (
            test.find('Lab(') != -1 or
            test.find('But(') != -1):
            _src.append(li)
            continue
        ix0 = li.find('(')
        ix1 = li.rfind(')')
        if ix1 == -1:
            raise ParseFail('no closing ")" line: %d' % lno)
        args = li[ix0+1:ix1]
        args = args.split(',')
        left = li[:ix0+1]
        right = li[ix1:]
        tmp = [left]
        if test.find('But(') != -1:
            _args = []
            for i in range(0,len(args)):
                if i != 2:
                    _args.append(args[i])
            tmp.append(','.join(_args))
        else:
            tmp.append(args[0])
        tmp.append(right)
        li = ''.join(tmp)
        li = li.replace('Lab','Label')
        li = li.replace('But','Button')
        _src.append(li)
    src = _src
    
    # Pass2: replace var ref's with decls
    _src = []
    for li in src:
        tks = tokenize(li)
        for t in tks:
            if t.isid and symtbl.get(t.v) is not None:
                decl = symtbl[t.v]
                t.v = decl.rewrite(t.v)
        _src.append(tks_to_str(tks))
    return _src

def rewrite(fn):
    with open(fn,'r') as f:
        lines = f.readlines()
        body = []
        make_ui = []
        dst = body
        for li in lines:
            li = li.rstrip()
            test = li.strip()
            if test.startswith('build_table') or test.startswith('build_row'):
                continue
            if li.startswith('def make_ui'):
                dst = make_ui
                continue
            dst.append(li)
    nlines_body = len(body)
    # start dev code
    #for key in tbl:
        #decl = tbl[key]
        #print( decl.rewrite(key) )
    # print_symtbl(tbl)
    # end dev code
    (tbl,body) = get_symtbl(body)
    body = rewrite_vars(tbl,body)
    make_ui = rewrite_make_ui(tbl,make_ui,nlines_body)
    return Doc(fn,body,make_ui)

files = files.strip().split('\n')
docs = []
for fn in files:
    fn = fn.strip()
    if len(fn) == 0:
        continue
    print(fn)
    docs.append( rewrite(fn) )

for d in docs:
    d.writeme()





