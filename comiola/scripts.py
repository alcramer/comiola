# Copyright Al Cramer 2021 ac2.71828@gmail.com
import os
import shutil
from PIL import ImageTk,Image,ImageDraw,ImageOps
import images
import io
import math
import json
from images import Reg
import resources as res
from tkalt import *

# version number
version = '1.0.2'

# header for comiola project file
proj_file_header = 'comiola %s' % version

# debugging: id for Pt's
_pid = 0

class Pt:
    def __init__(self,x=0.0,y=0.0,
        param=0.0, z=0.0, rot=0.0, w=0.0, h=0.0, fnspr='', 
        flip=0, pxfrm_w=1.0, pxfrm_h=1.0, pxfrm_fall=0.0):
        global _pid 
        self.classname = self.__class__.__name__
        self.x = float(x)
        self.y = float(y)
        self.param = float(param)
        self.z = float(z)
        self.rot = float(rot)
        self.w = float(w)
        self.h = float(h)
        self.fnspr = fnspr
        self.flip = int(flip)
        self.pxfrm_w = float(pxfrm_w)
        self.pxfrm_h = float(pxfrm_h)
        self.pxfrm_fall = float(pxfrm_fall)

        self._id = _pid
        _pid += 1

    def clone(self):
        return Pt(self.x,self.y,self.param,self.z,self.rot,
                self.w,self.h,self.fnspr,self.flip,
                self.pxfrm_w, self.pxfrm_h, self.pxfrm_fall)

    def set_xy(self,x,y):
        self.x = x
        self.y = y

    def set_wh(self,w,h):
        self.w = w
        self.h = h

    def has_pxfrm(self):
        return (self.pxfrm_w != 1.0 or
                self.pxfrm_h != 1.0 or
                self.pxfrm_fall != 0.0)

    def equal_pxfrm(self,ptx):
        return (self.pxfrm_w == ptx.pxfrm_w and
                self.pxfrm_h == ptx.pxfrm_h and
                self.pxfrm_fall == ptx.pxfrm_fall)

class Ani:
    def __init__(self,
            kind='spr',
            tS = 0.0, tE = 1.0, 
            cycles=1.0, frames_per_cell=0.0,reverb=0.0):
        self.classname = self.__class__.__name__
        # kind: "spr","cam","txt"
        self.kind = kind
        self.tS = float(tS)
        self.tE = float(tE)
        self.cycles = float(cycles)
        self.frames_per_cell = float(frames_per_cell)
        self.reverb = reverb
        self.path = []
        self.fnlst = []
        self.te = None
        self.lockto = None
        self.fxdct = {} 
        # used in serialization
        self.id = str(id(self))

    def clone(self):
        cl = Ani(
                self.kind,
                self.tS, self.tE,
                self.cycles,self.frames_per_cell,self.reverb)
        for p in self.path:
            cl.path.append(p.clone())
        if self.kind == 'spr':
            cl.fnlst = self.fnlst[:]
        elif self.kind == 'txt':
            cl.te = self.te.clone()
        # give cl its own copy of the fx dict
        jstr = json.dumps(self.fxdct)
        cl.fxdct = json.loads(jstr)
        # used in serialization
        self.cl = cl
        return cl

    def xlate_path(self,xdelta,ydelta):
        for p in self.path:
            p.x += xdelta
            p.y += ydelta

    def set_path_params(self):
        path = self.path
        N = len(path)
        if N == 0:
            return
        path[0].param = 0.0
        if N > 1:
            step = 1/(float(N-1))
            for i in range(1,N):
                path[i].param = path[i-1].param + step

    def add_pt(self,pt):
        self.path.append(pt)
        pt.ani = self
        self.set_path_params()

    def delete_pt(self,pt):
        self.path.remove(pt)
        self.set_path_params()

    def interpolate_pt(self,targ):
        # given a param value "targ" (0<=targ<=1) get
        # corresponding pt.
        path = self.path
        if len(path) == 1:
            return path[0].clone()
        for i in range(1,len(path)):
            p = path[i]
            prv = path[i-1]
            if targ >= prv.param and targ < p.param:
                dp = targ - prv.param

                # "dv_dp is the derivative dv/dp
                dv_dp = (p.x - prv.x)/(p.param - prv.param)
                x = prv.x + dv_dp * dp  

                dv_dp = (p.y - prv.y)/(p.param - prv.param)
                y = prv.y + dv_dp * dp  

                dv_dp = (p.z - prv.z)/(p.param - prv.param)
                z = prv.z + dv_dp * dp  

                dv_dp = (p.rot - prv.rot)/(p.param - prv.param)
                rot = prv.rot + dv_dp * dp  

                # width and height
                if self.kind == 'txt':
                    # w & h don't change for text
                    w = prv.w
                    h = prv.h
                else:
                    # width and height are special cases. h is interpolated
                    # like x & y, but w is reset so aspect ratio is the same
                    # as aspect ratio. for prv. This is because the sprite
                    # image can change from pt to pt.
                    dv_dp = (p.h - prv.h)/(p.param - prv.param)
                    h = prv.h + dv_dp * dp  
                    ar = prv.h / prv.w
                    w = h/ar

                return Pt(int(.5+x), int(.5 + y),targ,z,rot,
                    int(.5 + w),int(.5 + h), prv.fnspr, prv.flip,
                    prv.pxfrm_w, prv.pxfrm_h, prv.pxfrm_fall)
        return path[-1].clone()

    def get_fn_pt(self,pt):
        if self.kind == 'spr':
            if pt.fnspr != '':
                return pt.fnspr
            return self.fnlst[0]
        return ''

    def cnt_cells(self):
        if self.kind == 'spr':
            return len(self.fnlst)
        return 1

    def cnt_pts(self):
        return len(self.path)

    def static_cells(self):
        # "static_cells" means single pt and multiple cells
        return (self.kind == 'spr' and len(self.path)==1 and
                len(self.fnlst) > 1)

    def set_frames_per_cell(fpc):
        self.frames_per_cell = fpc
        # reset reverb to default value for this fpc
        if fpc <= 2:
            self.reverb = 0
        elif fpc <= 4:
            self.reverb = 1
        else:
            self.reverb = 2

    def dump(self):
        print('ani.kind: %s' % self.kind)
        for i in range(0,len(self.path)):
            p = self.path[i]
            print('pt%d. x:%.2f y:%.2f w:%2f h:%.2f' %
                (i,p.x,p.y,p.w,p.h))

class TextEl:
    def __init__(self,
            fontname='comics',
            fontsize='24pt',
            fontcolor='#000000',
            bgspec='null',
            w_txt = 0.0,
            h_txt = 0.0,
            w_bg = 0.0,
            h_bg = 0.0,
            yoff_bg = 0.0):
        self.classname = self.__class__.__name__
        self.fontname = fontname
        self.fontsize = fontsize
        self.fontcolor = fontcolor
        self.bgspec = bgspec
        self.w_txt = float(w_txt)
        self.h_txt = float(h_txt)
        self.w_bg = float(w_bg)
        self.h_bg = float(h_bg)
        self.yoff_bg = float(yoff_bg)
        # use get_text and set_text for this value.
        self._text = ''

    def get_text(self):
        return self._text

    def set_text(self,text):
        self._text = text
        self.set_layout()

    def clone(self):
        cl = TextEl(
            self.fontname,self.fontsize,self.fontcolor, self.bgspec,
            self.w_txt, self.h_txt, self.w_bg, self.h_bg, self.yoff_bg)
        cl._text = self._text
        return cl

    def set_layout(self):
        if self._text == '':
            # We support empty text elements in case user
            # wants to create a word balloon & populate with
            # their own content.
            (self.w_txt, self.h_txt) = (150,75) 
        else:
            # measure text
            im = Image.new('RGB',(res.w_shot,res.h_shot))
            draw = ImageDraw.Draw(im)
            draw.text((0,0),self._text,fill=(255,255,255),
                font=res.get_font(self.fontname,self.fontsize))
            (x0,y0,x1,y1) = im.getbbox()
            (self.w_txt, self.h_txt) = (x1+1, y1+1) 
        # we provide a margin so the bg is a bit larger 
        # than the text.
        mar = 10
        (self.w_bg, self.h_bg) = (
            self.w_txt + 2*mar,
            self.h_txt + 2*mar) 
        self.yoff_bg = 0

    def get_bb_bg(self,xc,yc):
        # given (x,y) of center, get bounding box for bg
        w = int(self.w_bg/2)
        h = int(self.h_bg/2)
        yoff = int(self.yoff_bg)
        return (xc-w, yc-h+yoff, xc+w, yc+h+yoff)

    def get_bb_text(self,xc,yc):
        # given (x,y) of center, get bounding box for text
        w = int(self.w_txt/2)
        h = int(self.h_txt/2)
        return (xc-w, yc-h, xc+w, yc+h)

class Shot:
    # A shot is:
    # "secs" -- number of secs for this shot.
    # "cam" -- an Ani representing the camera 
    # "bgspec" -- bg spec for the shot. Either a color value ('#ff0000')
    #             or a filename (minus file extension)
    # "fxspec" -- effects-spec (fade,dissolve) 
    # "anis" -- list of Ani objects for the sprites & text
    def __init__(self, secs=3.0, bgspec=None, cam=None,fxspec=None):
        self.classname = self.__class__.__name__
        self.secs = float(secs)
        self.bgspec = bgspec
        self.cam = cam
        self.fxspec = fxspec
        self.fxdct = {} 
        self.anis = []

    def clone(self):
        sh = Shot(self.secs, self.bgspec, self.cam.clone(),self.fxspec)
        for e in self.anis:
            sh.anis.append(e.clone())
        # "lockto" is a relation between objects: correct the clone
        all_anis = [self.cam]
        all_anis.extend(self.anis)
        for e in all_anis:
            if e.lockto is not None:
                targ = e.lockto
                e.cl.lockto = targ.cl
        # give sh its own copy of the fx dict
        jstr = json.dumps(self.fxdct)
        sh.fxdct = json.loads(jstr)
        return sh

    def _continue(self):
        sh = self.clone()
        # undo any lockto's (they're not compatible with continue)
        # For each ani path, trim it down to last point
        all_anis = [sh.cam]
        all_anis.extend(sh.anis)
        for e in all_anis:
            e.lockto = None
            e.path = [ e.path[-1] ]
        return sh

    def has_bg_clip(self):
        return self.bgspec.endswith('.mp4')

    def has_bg_illo(self):
        return not self.bgspec.startswith('#')

    def preload(self):
        try:
            if self.has_bg_illo():
                res.get_img(self.bgspec)
            for ani in self.anis:
                if (ani.kind == 'spr'):
                    for fn in ani.fnlst:
                        res.get_img(fn)
                elif ani.kind == 'txt':
                    res.get_font(ani.te.fontname,ani.te.fontsize)
        except FileNotFoundError:
            pass

    def get_ani_cycles(self,ani):
        # get cycles for an animation (how many times it repeats)
        if (ani.kind == 'spr' and
            ani.cnt_pts() == 1 and ani.cnt_cells() > 1):
            # This is cell animation: one pt in the path, and multiple
            # cells.  cycles is treated as a dependent variable,
            # and (frame_per_cell,tS,tE,self.secs) are
            # treated as parameters. 
            nframes_ani = float( (ani.tE - ani.tS) * self.secs * 24 )
            return nframes_ani/ (ani.cnt_cells() * ani.frames_per_cell)
        else:
            # standard case: cycles is an independent parameter
            return ani.cycles

    def set_secs(self,secs):
        self.secs = secs
        for ani in self.anis:
            ani.cycles = self.get_ani_cycles(ani)

    def sync_secs(self,ani):
        # use for 1pt, multi-cell animation: resets secs
        # to accord with ani's (cycles,frames_per_cell) values
        nframes = ani.cycles * ani.cnt_cells() * ani.frames_per_cell
        secs = (nframes/24.0)/(ani.tE - ani.tS)
        self.set_secs(secs)

class ProjClips:
    def __init__(self):
        self.classname = self.__class__.__name__
        self.fnvideos = []
        self.fnmerged = 'Untitled'

class Script:
    def __init__(self):
        self.classname = self.__class__.__name__
        self.clips = ProjClips()
        self.time = 10.0
        self.shots = []
        # aspect-ratio for shots
        self.ar_shot = 1.0

    def cnt_shots(self):
        return len(self.shots)

    def get_time(self):
        _sum = 0
        for s in self.shots:
            _sum += s.secs
        mins = int(_sum/60)
        secs = _sum - 60.0 * mins
        return (mins,secs)

    def serialize(self):

        # this dictionary maps classnames to the set of object 
        # attributes we will serialize. 
        attrids = {}
        for obj in [Pt(),Ani(),TextEl(),Shot(),Script(),ProjClips()]:
            keys = []
            for key in obj.__dict__:
                keys.append(key)
            attrids[obj.classname] = keys

        def get_dict(obj):
            d = {}
            for key,v in obj.__dict__.items():
                if key in attrids[obj.classname]:
                    if key == 'lockto':
                        if v is not None:
                            # a relation between objects: replace "v" with
                            # a reference.
                            d[key] = v.id
                    else:
                        d[key] = v
            return d

        return (proj_file_header + '\n' +
            json.dumps(script, indent=2, default=lambda o: get_dict(o)))


    @classmethod
    def unserialize(cls,src):
        # line0 is version. Currently ignored: strip from src
        lines = src.split('\n')
        src = '\n'.join(lines[1:])

        # json decoder
        refpool = []
        objdct = {}
        def decoder(obj):
            if 'classname' in obj:
                cls = obj['classname']
                if cls == 'Pt':
                    _obj = Pt()
                elif cls == 'Ani':
                    _obj = Ani()
                elif cls == 'TextEl':
                    _obj = TextEl()
                elif cls == 'Shot':
                    _obj = Shot()
                elif cls == 'Script':
                    _obj = Script()
                elif cls == 'ProjClips':
                    _obj = ProjClips()
                else:
                    # reading an older version
                    return None
                for key,v in obj.items():
                    setattr(_obj,key,v)
                    if key == 'lockto':
                        refpool.append(_obj)
                    if key == 'id':
                        objdct[v] = _obj
                return _obj
            return obj

        script = json.loads(src,object_hook=decoder)
        # "lockto" is an inter-object relation: resolve object refs
        for e in refpool: 
            e.lockto = objdct.get(e.lockto) 

        # TMP code for back-compat
        for s in script.shots:
            if hasattr(s,'fxspec'):
                # this supplanted by more general fxdct
                if s.fxspec is not None:
                    (fxname,secs) = s.fxspec.split(':')
                    s.fxdct['fade'] = {'secs':float(secs)}
            # color attribute was added to lighting 1.0.2
            lighting = s.fxdct.get('lighting')
            if lighting is not None:
                if lighting.get('color') is None:
                    lighting['color'] = '#000000'

        return script



script = Script()
proj_dir = ''
proj_name = ''
proj_filepath = ''
# the script, serialized, at project start
script0_serialized = ''

def script_open():
    return proj_dir != ''

def close_project():
    global script, proj_dir, proj_name, proj_filepath
    script = Script()
    proj_dir = ''
    proj_name = ''
    proj_filepath = ''

def script_changed():
    global script0_serialized
    return  (proj_filepath != '' and 
            script0_serialized != script.serialize())

def cnt_shots():
    return len(script.shots)

def get_shot(ix):
    if ix < 0 or ix >= cnt_shots():
        return None
    else:
        return script.shots[ix]

def save_script(name):
    global proj_name,proj_filepath,script0_serialized
    if name != '':
        if not name.endswith('.cprj'):
            name += '.cprj'
        proj_name = name
        proj_filepath = '%s/%s' % (proj_dir,proj_name)
    with open(proj_filepath,'w') as f:
        script0_serialized = script.serialize()
        f.write(script0_serialized)

def open_project(d,name,create,ar=1.0):
    global proj_dir,proj_name,proj_filepath,script,script0_serialized
    while d.endswith('/'):
        d = d[-1]
    proj_dir = d
    proj_name = name
    if not proj_name.endswith('.cprj'):
        proj_name += '.cprj'
    proj_filepath = '%s/%s' % (d,proj_name)
    if create:
        # create new script and save
        script = Script()
        script.ar_shot = ar
        save_script(proj_name)
        # copy fill texture files to project directory
        src = res.get_assets_dir('res/textures')
        file_names = os.listdir(src)
        for fn in file_names:
            shutil.copy(os.path.join(src, fn), proj_dir)
    else:
        try:
            with open(proj_filepath,'r') as f:
                src = f.read()
                script = Script.unserialize(src)
                script0_serialized = script.serialize()
        except:
            showerror(
                'Could not read "%s/%s"' % (d,name))
            proj_filepath = ''
            return False
    res.set_proj_params(proj_dir,script.ar_shot)
    return True

def close_project():
    global script, proj_dir, proj_name, proj_filepath
    script = Script()
    proj_dir = ''
    proj_name = ''
    proj_filepath = ''

def extend_script(newshots,add_after):
    # rebuild the shot list, including the new shots
    # add_after == -1 means include at head
    shots = script.shots
    _shots = []
    if add_after != -1:
        _shots = shots[0:add_after+1]
    _shots.extend(newshots)
    if add_after + 1 < len(shots):
        _shots.extend(shots[add_after+1 : ])
    script.shots = _shots

def scale_secs(factor):
    for s in script.shots:
        if not s.bgspec.endswith('.mp4'):
            s.secs *= factor

def on_clip_changed(fnclip):
    secs = res.get_secs_clip(res.get_fpart(fnclip))
    for s in script.shots:
        if s.bgspec == fnclip:
            s.secs = secs

def add_blank_shot(ixafter,color):
    cam = Ani('cam',0.0,1.0,1.0,4)
    (w,h) = (res.w_shot, res.h_shot)
    cam.path.append(Pt(int(w/2), int(h/2), 0.0, 0.0,0.0,w,h))
    extend_script( [Shot(3.0,color,cam)], ixafter)

def clone_shot(ix):
    extend_script([get_shot(ix).clone()],ix)

def continue_shot(ix):
    extend_script([get_shot(ix)._continue()],ix)

def delete_shots(ixS,ixE):
    _shots = []
    deleted = []
    for i in range(0,len(script.shots)):
        if i < ixS or i > ixE:
            _shots.append(script.shots[i])
        else:
            deleted.append(script.shots[i])
    script.shots = _shots
    if len(script.shots) > 0:
        # if last shot is marked fade, add a final black shot
        s = script.shots[-1]
        fadespec = s.fxdct.get('fade')
        if fadespec is not None:
            add_blank_shot(len(script.shots)-1,'#000000')
    return deleted

def copy_shots(ixS,ixE):
    _shots = []
    for i in range(ixS,ixE+1):
        _shots.append(script.shots[i].clone())
    return _shots

def add_ani(kind,ixshot,te=None,fnlst=None):
    ani = Ani(kind,0.0,1.0,1.0,4)
    ani.te = te
    ani.fnlst = fnlst
    shot = get_shot(ixshot)
    shot.anis.append(ani)
    return ani

def delete_ani(ixshot,ani):
    s = get_shot(ixshot)
    try:
        s.anis.remove(ani)
        # if anybody is locked to ani, release them
        if s.cam.lockto == ani:
            s.cam.lockto = None
        for e in s.anis:
            if e.lockto == ani:
                e.lockto = None
    except ValueError:
        pass

def reset_pt_img(pt,fn):
    # set the "fnspr" attribute for a point
    pt.fnspr = res.install_image(fn)
    # we keep the old value for pt.h, but reset pt.w to accord
    # with aspect ratio of new img.
    with Image.open(fn) as img:
        (w_img, h_img) = img.size
        pt.w = w_img * float(pt.h)/h_img

def reset_ani_cells(ani,fnlst):
    # reset the image files for a cell animation.

    ani.fnlst = fnlst
    # Visit each pt in the path: we keep the old value for pt.h, but 
    # reset pt.w to accord with aspect ratio of new img.
    img = res.get_img(fnlst[0])
    (w_img, h_img) = img.size
    for pt in ani.path:
        pt.w = w_img * float(pt.h)/h_img
        # This overrides any image previously assigned to a point.
        pt.fnspr = ''

