# Copyright Al Cramer 2021 ac2.71828@gmail.com
import tkinter as tki
import tkinter.filedialog
import tkinter.messagebox 
import tkinter.font

import re
from PIL import Image, ImageTk

import sys
import os
import time

import colors

class TkaltError(Exception):
    pass

__version__ = '0.11'
def get_tkalt_version():
    return __version__

# tkref to main window
_root = None
# can we close the main window?
_can_close_window = None
# appname
_appname = ''
# font is defined if style's fontsize attribute is specified
font = None

def tkalt_init(appname,stylesheets=None,can_close_window=None,fpicon=None):
    # Init the api.
    # appname: name of app (for error msgs & window title)
    # stylesheets: dictionary of context -> style 
    # can_close_window: callback function, invoked when user closes
    #    the window. Return "True" to allows the close, "False" to
    #    cancel it.
    global _appname,_styles,_root,_can_close_window,font
    _appname = appname
    _root = tki.Tk()
    _can_close_window = can_close_window
    _root.title(appname)
    _root.protocol('WM_DELETE_WINDOW',close_window)
    _styles = _Styles(stylesheets)
    if fpicon is not None and sys.platform != 'darwin':
        try:
            if fpicon.endswith('.ico'):
                #if not getattr(sys, 'frozen', False):
                    #fpicon = os.path.join(
                        #os.path.dirname(__file__), fpicon)
                _root.iconbitmap(fpicon)
            else:
                _root.tk.call('wm', 'iconphoto', 
                    _root._w,tki.PhotoImage(file=fpicon))
        except Exception as ex:
            #print(ex)
            pass
    # font size (if specified) is in "*"
    fontsize = _styles.sheets['*'].get('fontsize')
    if fontsize is not None:
        font = tkinter.font.Font(size=fontsize)


def close_window():
    # close the window (shutdown the app). If you supplied a
    # "can_close_window" function when you called "tkalt_init",
    # the shutdown will be aborted if that function returns False.
    if _can_close_window is not None and not _can_close_window():
        return
    _root.destroy()

def focus_set(uielement):
    # award focus to a uilement
    try:
        uielement.peer.focus_set()
    except:
        pass

def mainloop():
    # start the min event loop
    _root.mainloop()

############################################
# Popup dialogs
############################################

def showinfo(msg):
    # Post info popup.
    tkinter.messagebox.showinfo(_appname,msg)

def showerror(msg):
    # Post error message popup.
    tkinter.messagebox.showerror(_appname,msg)

def askyesnocancel(msg):
    # Post yes/no/cancel popup.
    return tkinter.messagebox.askyesnocancel(_appname,msg)

def askopenfilename(initialdir=None,filetypes=()):
    # Post select file popup. See tkinter documentation for 
    # details on "filetypes" filter.
    return tkinter.filedialog.askopenfilename(
            initialdir=initialdir,filetypes=filetypes)

def askopenfilenames(initialdir=None,filetypes=()):
    # Post select file(s) popup. See tkinter documentation for 
    # details on "filetypes" filter.
    # This code fixes a bug in windows version of tkinter
    _tuple = tkinter.filedialog.askopenfilenames(
            initialdir=initialdir,filetypes=filetypes)
    if _tuple == '':
        return ()
    else:
        return _tuple

def askdirectory():
    # Post select directory popup.
    return tkinter.filedialog.askdirectory()

############################################
# Styles
############################################

# hexcodes for the color names we accept

_colorcodes = {"black": "#000000", "gray": "#808080", "grey": "#808080", "darkgray": "#A9A9A9", "darkgrey": "#A9A9A9", "silver": "#C0C0C0", "lightgray": "#D3D3D3", "lightgrey": "#D3D3D3", "darkblue": "#00008B", "mediumblue": "#0000CD", "blue": "#0000FF", "lightblue": "#ADD8E6", "azure": "#F0FFFF", "lightcyan": "#E0FFFF", "aqua": "#00FFFF", "cyan": "#00FFFF", "paleturquoise": "#AFEEEE", "turquoise": "#40E0D0", "mediumturquoise": "#48D1CC", "darkturquoise": "#00CED1", "darkcyan": "#008B8B", "olive": "#808000", "green": "#008000", "darkgreen": "#006400", "limegreen": "#32CD32", "lime": "#00FF00", "chartreuse": "#7FFF00", "greenyellow": "#ADFF2F", "lightgreen": "#90EE90", "palegreen": "#98FB98", "lightyellow": "#FFFFE0", "beige": "#F5F5DC", "yellow": "#FFFF00", "gold": "#FFD700", "orange": "#FFA500", "tan": "#D2B48C", "darkorange": "#FF8C00", "orangered": "#FF4500", "red": "#FF0000", "brown": "#A52A2A", "darkred": "#8B0000", "maroon": "#800000", "pink": "#FFC0CB", "lightpink": "#FFB6C1", "deeppink": "#FF1493", "crimson": "#DC143C", "violet": "#EE82EE", "fuchsia": "#FF00FF", "magenta": "#FF00FF", "indigo": "#4B0082", "darkmagenta": "#8B008B", "purple": "#800080", "darkviolet": "#9400D3", "blueviolet": "#8A2BE2", "mediumpurple": "#9370DB", "white": "#FFFFFF",}

_styles = None
class _Styles():
    def __init__(self,sheets):
        # "sheets" is supplied by user. It has form:
        # sheets = {
        #   'menu': {
        #      'fg': 'white',
        #      'bg': 'black',
        #      'bg.button': '#00ffff'
        #   },
        #   '*' : {
        #      'fg': 'black',
        #      'bg': '#454545'
        #   }
        #  }
        # ensure default style with fg and bg is defined.
        if sheets is None:
            self.sheets = {}
        else:
            self.sheets = sheets
        defaults = self.sheets.get('*')
        if defaults is None:
            self.sheets['*'] = {'fg':'#000000','bg':'#f0f0f0','border.button':True}
        else:
            if defaults.get('fg') is None:
                defaults['fg'] = '#000000'
            if defaults.get('bg') is None:
                defaults['bg'] = '#f0f0f0'

        # these are the attributes user can change. 
        self.attrids = ['fg','bg','border','hmargin','vmargin']

    def get_attr_value(self,cntxt,attrid,qual,local_style=None):
        # get style attribute value for context ("None" is legal: it means
        # "any context").
        qid = '%s.%s' % (attrid,qual)
        dcts = [local_style]
        if self.sheets is not None:
            dcts.append(self.sheets.get(cntxt))
            dcts.append(self.sheets.get('*'))
        for dct in dcts:
            if dct is None:
                continue
            if qual is not None:
                try:
                    return dct[qid]
                except KeyError:
                    pass
            # un-qualified
            try:
                return dct[attrid]
            except KeyError:
                pass
        return None

    def get_qualified_value(self,cntxt,attrid,qual,local_style=None):
        # Get qualified attr value: return None is none specified
        qid = '%s.%s' % (attrid,qual)
        dcts = [local_style]
        if self.sheets is not None:
            dcts.append(self.sheets.get(cntxt))
            dcts.append(self.sheets.get('*'))
        for dct in dcts:
            if dct is not None:
                try:
                    return dct[qid]
                except KeyError:
                    pass
        return None

    def set_attr(self,tkobj,attrid,v):
        def set_v(attrid,v):
            try:
                tkobj[attrid] = v
            except:
                # attribute not defined on tkobj
                pass
            
        if v is not None:
            if attrid in ['fg','bg']:
                set_v(attrid,v)
            elif attrid == 'border':
                if v:
                    set_v('relief',tki.GROOVE)
                    set_v('borderwidth',2)
                else:
                    set_v('relief',tki.FLAT)
                    set_v('borderwidth',0)
            elif attrid in ['hmargin','vmargin']:
                attrid = 'padx' if attrid == 'hmargin' else 'pady'
                set_v(attrid,v)

    def apply_style(self,tkobj,cntxt,qual,local_style=None):
        for attrid in self.attrids:
            v = self.get_attr_value(cntxt,attrid,qual,local_style)
            self.set_attr(tkobj,attrid,v)

    def apply_local_style(self,tkobj,dct):
        for attrid,v in dct.items():
            self.set_attr(tkobj,attrid,v)

def _get_contrast_color(src):
    #print(src)
    if not src.startswith('#'):
        code = _colorcodes.get(src)
        if code is None:
            raise TkaltError(
                    '"%s" is not a recognized color name' % src)
        src = code
    rgb = bytes.fromhex(src[1:])
    _rgb = [0]*3
    # for component value "v", "space" is amount by whiich we can
    # increase the value before saturating. So it's "255-v". 
    # "space_min" is a parameter used to determine whether src
    # is a pastel color: we call it pastel if space for every component
    # is less than space_min. 
    space_min = 32
    # is src a pastel color? 
    src_is_pastel = True
    for v in rgb:
        space = 255 - v
        if space > space_min:
            # this component is not pastel, so we say src is not pastel
            src_is_pastel = False
            break
    for i in range(0,3):
        v = rgb[i]
        space = 255 - v
        if src_is_pastel:
            # make every component darker
            _rgb[i] = v - int(space_min/2)
        else:
            # src is not pastel: make dark components lighter
            if space > space_min:
                _rgb[i] = int(v + space/2)
            else:
                # light components still unchanged
                _rgb[i] = v
    dst = '#%s' % (''.join('%02x'%i for i in _rgb))
    return dst

############################################
# UI Construction
############################################

# list of all nodes created.
_all_nds = []
# list of all leaves created.
_leaves = []
def vet_nds():
    # dev/test code: vett the tree structure
    for v in _all_nds:
        if v.parent is None:
            if v.id is None:
                print('orphan nd: no id')
                print(v)
                print('python id: %d' % id(v))
            else:
                print('orphan nd: %s' % nd.id)

class _Nd():
    # base class for node in the visual graph
    def __init__(self,id=None,cntxt=None,w=None,h=None,weight=0):
        if id is not None:
            if not id.isidentifier(): 
                raise TkaltError('"%s" is not a valid identifier' % id)
            if hasattr(ui,id):
                raise TkaltError('"%s" already in use' % id)
            setattr(ui,id,self)
        self.id = id
        self.cntxt = cntxt
        self.style = None
        self.w = w
        self.h = h
        self.weight = weight
        self.sum_weight = weight
        self.ixrow = 0
        self.ixcol = 0
        self.colspan = 1
        self.sticky = tki.NW + tki.SE
        self.nds = []
        self.made = False
        _all_nds.append(self)
        self.parent = None

        # set by child classes
        self.peer = None
        # enable/disabled status
        self.enabled = True
        self.always_enabled = False


    def dump(self, ident):
        # dev/test: dump the tree
        tmp = []
        tmp.append(
            '%sId:%s type:%s ixrow:%d ixcol%d sticky:%s' % 
(ident,self.id, str(type(self)), self.ixrow, self.ixcol,self.sticky))
        tmp.append('%speer:%s' % (ident,str(self.peer)))
        print('%s' % '\n'.join(tmp))
        ident += '    '
        for e in self.nds:
            e.dump(ident)

    def add_nds(self,nd):
        # build the tree: "nd" can be a single node or a list of nodes
        if isinstance(nd,list) or isinstance(nd,tuple):
            for e in nd:
                #self.nds.append(e)
                #e.parent = self
                self.add_nds(e)
        else:
            self.nds.append(nd)
            nd.parent = self

    def walk_tree(self):
        # walk tree, propogating context spec downward;
        # also constructs the "_leaves" list and gets weight of subtree.
        if self.parent is not None:
            if self.cntxt is None:
                self.cntxt = self.parent.cntxt
        if len(self.nds) == 0:
            _leaves.append(self)
        for e in self.nds:
            e.walk_tree()
            self.sum_weight += e.sum_weight

    def get_tkpeers(self):
        # get all tk objects implementing this node.
        return self.peer

    def set_enabled(self,vBool):
        for e in self.nds:
            e.set_enabled(vBool)
        try:
            if not self.always_enabled:
                for p in self.get_tkpeers():
                    p['state'] = tki.NORMAL if vBool else tki.DISABLED
                self.enabled = vBool
        except Exception as ex:
            pass

    def is_enabled(self):
        return self.enabled

class Frame(_Nd):
    def __init__(self,id,cntxt,content):
        # Frame is a container for widgets, laid out in one or
        # more rows.
        # id: ui id for this widget
        # cntxt: style context for this frame + subnodes.
        # content: widgets for this frame. 
        super().__init__(id,cntxt,None,None,0)
        if callable(content):
            content = content()
        # canonicalize rows and unpackk into self.nds
        def isrow(v):
            return isinstance(v,list) or isinstance(v,tuple)

        self.rows = []
        if not isrow(content):
            # a single item: coerce to list
            content = [content]
        for e in content:
            if isrow(e):
                self.rows.append(e)
            else:
                self.rows.append([e])
            self.add_nds(e)

    def make(self,parent):
        if self.made:
            return
        self.made = True
        myframe = tki.Frame(master=parent, highlightthickness=0)
        _styles.apply_style(myframe,self.cntxt,'frame')

        if self.ixrow != -1:
            myframe.grid(row=self.ixrow,column=self.ixcol,
                columnspan=self.colspan,sticky=self.sticky)
        self.peer = myframe

        # populate 'myframe' with rows of widgets.
        self.rowframes = []
        for i in range(0,len(self.rows)):
            if len(self.rows[i]) > 0 and isinstance(self.rows[i][0],Hr):
                # special case: just install the Hr's peer (a frame)
                e = self.rows[i][0]
                e.make(myframe)
                e.peer.grid(row=i,column=0,sticky=tki.W+tki.E,
                        padx=[4,4],pady=[3,1])
                continue
            rowframe = tki.Frame(master=myframe, highlightthickness=0)
            self.rowframes.append(rowframe)
            rowframe['bg'] = myframe.cget('bg')
            rowframe.grid(row=i,column=0,sticky=tki.W+tki.E,pady=[1,2])
            ixcol = 0
            for e in self.rows[i]:
                # TODO: comment
                e.alignment_frame = self
                #if isinstance(e,DropdownMenu) or isinstance(e,_DDWidget):
                    #print('debug1')
                # create the peers for e (there may be more than 1)
                # and adjust xpad as needed. 
                e.make(rowframe)
                peers = e.get_tkpeers()
                for peer in peers:
                    padx = [4,4]
                    if hasattr(peer,'lopadx'):
                        padx = getattr(peer,'lopadx')
                    # This tweak improves layout when Checkbox/Radiobutton
                    # is in column 0
                    if ixcol == 0 and isinstance(e,_CheckboxBase):
                        padx[0] += 8
                    if font is not None:
                        try:
                            peer['font'] = font
                        except:
                            pass
                    sticky = tki.W
                    peer.grid(
                        row=0,column=ixcol,sticky=sticky,padx=padx)
                    ixcol += 1

        myframe.bind("<Button-1>", close_all_dropdowns)
        return myframe

    def get_tkpeers(self):
        return [self.peer]


class Canvas(_Nd):
    # Canvas wraps the tkinter class "Canvas": you can draw on it,
    # paste text and images, track mouse movements, etc. 
    def __init__(self,id,cntxt,w=None,h=None,weight=1,
            on_mousedown=None,
            on_mouseup=None,
            on_mousemove=None,
            on_doubleclick=None):
        # id: ui id for this widget
        # cntxt: style context for this canvas
        # w,h,weight: width, height, and weight for this canvas.
        # on_mousedown, etc: event handles for mouse events.
        if id is None:
            raise TkaltError('Canvas requires id')
        super().__init__(id,cntxt,w,h,weight)
        self.on_mousedown = on_mousedown
        self.on_mouseup = on_mouseup
        self.on_mousemove = on_mousemove
        self.on_doubleclick = on_doubleclick

    def make(self,parent):
        if self.made:
            return
        self.made = True
        w = h = 1
        if self.w is not None:
            w = self.w
        elif self.h is not None:
            h = self.h
        c = tki.Canvas(master=parent, width=w, height=h, 
            highlightthickness=0) 
        _styles.apply_style(c,self.cntxt,'canvas')
        c.grid(row=self.ixrow, column=self.ixcol,
            columnspan=self.colspan, sticky=self.sticky)
        c.bind("<Button-1>", close_all_dropdowns)
        if self.on_mousedown is not None:
            c.bind("<Button-1>", self.on_mousedown, add='+')
        if self.on_mouseup is not None:
            c.bind("<ButtonRelease-1>", self.on_mouseup)
        if self.on_mousemove is not None:
            c.bind("<Motion>", self.on_mousemove)
        if self.on_doubleclick is not None:
            c.bind("<Double-Button-1>", self.on_doubleclick)
        self.peer = c
        # for Canvas, the id is bound to the peer
        setattr(ui,self.id,c)
        return c

class ViewSet(_Nd):
    # A set of 1 or more views (Frames or Canvases), only one of which 
    # is visible at any given time. To show a view programatically,
    # call the "show" method, passing in the id of the view to be shown.
    # To create a tabbed view, pass in a list of tab labels to the
    # constructor (one for view).
    def __init__(self,id,cntxt,
            tabids,
            *views):
        if tabids is not None and len(tabids) != len(views):
            msg = ('Too many tab ids' if len(tabids) > len(views) else 
                'Too few tab ids')
            raise TkaltError(msg)
        if id is None:
            raise TkaltError('ViewSet requires id')
        super().__init__(id,cntxt,0,0,0)
        self.can_show= None
        self.on_show= None
        self.tabids = tabids
        for v in views:
            if v.id is None:
                raise TkaltError('ViewSet member requires id')
            self.add_nds(v)

    def make(self,parent):
        if self.made:
            return
        self.made = True
        myframe = tki.Frame(master=parent, highlightthickness=0)
        _styles.apply_style(myframe,self.cntxt,'frame')
        self.peer = myframe
        # positioning info was computed by our parent
        myframe.grid(row=self.ixrow, column=self.ixcol, 
            sticky=self.sticky)
        if self.tabids is not None:
            self.make_tabview(myframe)
            return
        for e in self.nds:
            (e.ixrow, e.ixcol, e.colspan,e.sticky) = (
                0,0,1,tki.NW+tki.SE)
            pane = e.make(myframe)
            _styles.apply_style(pane,self.cntxt,'frame')
            pane.grid_remove()
            e.tkframe = pane
        myframe.rowconfigure(0,weight=self.sum_weight)
        myframe.columnconfigure(0,weight=self.sum_weight)
        # set to first view
        self.show(self.nds[0].id)
        return myframe

    def make_tabview(self,myframe):
        # make tab bar and selector buttons
        tabbar = tki.Frame(master=myframe)
        _styles.apply_style(tabbar,self.cntxt,'tabbar')
        tabbar.grid(row=0,column=0,sticky=tki.W+tki.E,pady=[2,6])
        for i in range(0,len(self.nds)):
            e = self.nds[i]
            e.tabid = self.tabids[i]
            b = tki.Label(tabbar, text=e.tabid)
            b.idpane = e.id
            b.bind("<Button-1>", self.on_tab)
            # For buttton, set 'fg' and 'bg' as per frame.
            # 'bg' will be reset to shot selection status:
            # unselected tab button gets bg for tab bar, selected
            # button gets bg for frame
            _styles.apply_style(b,self.cntxt,'button')
            b['borderwidth'] = 0
            if font is not None:
                b['font'] = font
            b.grid(row=0,column=i,sticky=tki.W,padx=[4,4])
            e.tkselect = b
        # make panes
        for e in self.nds:
            (e.ixrow, e.ixcol, e.colspan,e.sticky) = (
                1,0,1,tki.NW+tki.SE)
            pane = e.make(myframe)
            _styles.apply_style(pane,self.cntxt,'panel')
            pane.grid_remove()
            e.tkframe = pane
        # configure: weight goes to pane row
        myframe.rowconfigure(1,weight=self.sum_weight)
        myframe.columnconfigure(0,weight=self.sum_weight)
        # bg for selected button is bg for the cntxt;
        # bg for unselected button is given by 'tabbar'.
        # Save these values.
        self.bgselected = myframe.cget('bg')
        self.bgunselected = tabbar.cget('bg')
        # set to first tab
        self.show(self.nds[0].id)
        return myframe


    def show(self,name):
        # show a view
        close_all_dropdowns()
        istabview = self.tabids is not None
        found_frame = True
        for e in self.nds:
            if (e.id == name):
                e.tkframe.grid()
                if sys.platform == 'darwin':
                    # Needed for MAC; causes shaky startup in Windows
                    e.tkframe.update()
                if istabview:
                    e.tkselect['bg'] = self.bgselected
                found_frame = True
            else:
                e.tkframe.grid_remove()
                if istabview:
                    e.tkselect['bg'] = self.bgunselected
        if not found_frame:
            raise TkaltError( 
    '%s is not defined in %s' % (name,self.id))
        if self.on_show is not None:
            self.on_show(name)

    def on_tab(self,ev):
        # used selected a tab
        idpane = ev.widget.idpane
        if self.can_show is not None:
            if not self.can_show(idpane):
                return 
        self.show(idpane)

    def set_enabled(self,vBool):
        super().set_enabled(vBool)
        # enable/disable buttons
        if self.tabids is not None:
            for e in self.nds:
                try:
                    e.tkselect['state'] = tki.NORMAL if vBool else tki.DISABLED
                except Exception as ex:
                    pass

class Layout(_Nd):
    def __init__(self,id,cntxt,w=None,h=None,
            N=None,S=None,E=None,W=None,C=None):
        if _root is None:
            raise TkaltError(
                'Initialization function "tkalt_init" was not called')
        super().__init__(id,cntxt,w,h,0)
        self.N = N
        self.S = S
        self.E = E
        self.W = W
        self.C = C
        if N is not None:
            N.pos = 'N'
            self.add_nds(N)
        if S is not None:
            S.pos = 'S'
            self.add_nds(S)
        if E is not None:
            E.pos = 'E'
            self.add_nds(E)
        if W is not None:
            W.pos = 'W'
            self.add_nds(W)
        if C is not None:
            C.pos = 'C'
            self.add_nds(C)

    def make(self,parent=None):
        if self.made:
            return
        self.made = True
        if parent is None:
            # we're the top of the hierarchy: 
            # set "sum_weight" for all nodes in the tree.
            # Also propogate cntxt info down the tree
            parent = _root
            self.walk_tree()
            # enable this for test/dev
            #vet_nds()
            peer = _root
            if self.w is not None:
                _root.geometry('%dx%d' % (self.w,self.h))
        else:
            # create the frame for self
            # positioning info was computed by our parent
            peer = tki.Frame(master=parent, highlightthickness=0)
            peer.grid(row=self.ixrow, column=self.ixcol, 
                sticky=self.sticky)
            
        # get the rows for the layout
        rows = []
        if self.N is not None:
            e = self.N
            rows.append([e])
        if (self.W is not None or 
            self.C is not None or
            self.E is not None):
            row = []
            e = self.W
            if e is not None:
                row.append(e)
            e = self.C
            if e is not None:
                row.append(e)
            e = self.E
            if e is not None:
                row.append(e)
            rows.append(row)
        if self.S is not None:
            e = self.S
            rows.append([e])
        # set ixrow, ixcol for each node; get max columns
        max_cols = 1
        for i in range(0,len(rows)):
            row = rows[i]
            for j in range(0,len(row)):
                e = row[j]
                e.ixrow = i
                e.ixcol = j
            max_cols = max(max_cols,len(row))
        # set column spans and sticky, then build subnodes
        for e in self.nds:
            e.colspan = 1
            if e.pos in ['N','S']:
                e.colspan = max_cols
            if isinstance(e,Canvas):
                if e.w is not None and e.h is None:
                    e.sticky = tki.N + tki.S
                elif e.h is not None and e.w is None:
                    e.sticky = tki.W + tki.E
            elif (e.ixrow == 0 and e.sum_weight == 0 and
                len(rows) == 0):
                # this element is non-expandable and should
                # stick to the top
                e.sticky = tki.NW + tki.NE
            e.make(peer)
        # configure our rows and columns. "heavy_nds" are nds with
        # sum_weight > 0.
        heavy_nds = []
        for i in range(0,len(rows)):
            row = rows[i]
            w = 0
            for e in row:
                w += e.sum_weight
                if e.sum_weight > 0:
                    heavy_nds.append(e)
            peer.rowconfigure(i,weight=w)
        for nd in heavy_nds:
            peer.columnconfigure(nd.ixcol,weight=nd.sum_weight)
        # start dev code
        #if parent == _root:
            #self.dump('')
        # end dev code
        bg = _styles.get_attr_value(self.cntxt,'bg',None)
        if bg is not None:
            peer['bg'] = bg
        self.peer = peer
        return peer

# objects with id's are accessed as "ui.id"
class _Ui():
    def __init__(self):
        # "tk" is bound to tkinter.
        self.tk = tki

ui = _Ui()

def enable_widget(*widgets):
    for e in _leaves:
        if isinstance(e,Button):
            if e.cmd in widgets:
                e.set_enabled(True)
    for w in widgets:
        if not callable(w):
            w.set_enabled(True)

def disable_widget(*widgets):
    for e in _leaves:
        if isinstance(e,Button):
            if e.cmd in widgets:
                e.set_enabled(False)
    for w in widgets:
        if not callable(w):
            w.set_enabled(False)

def disable_all():
    for e in _leaves:
        e.set_enabled(False)

############################################
# Widgets
############################################

# helper dropdown widgets: get (x,y,w,h) for a peer
def _get_lopeer(peer):
    peer.update()
    x = peer.winfo_x()
    y = peer.winfo_y()
    h = peer.winfo_height()
    w = peer.winfo_width()
    m = peer.master
    # This loop halts when we reach the top-level window:
    # we don't want to add in its offsets.
    while m.master is not None:
        m.update()
        x += m.winfo_x()
        y += m.winfo_y()
        m = m.master
    return (x,y,w,h)
    

class _Widget(_Nd):
    def __init__(self,id=None,label=None,always_enabled=False):
        super().__init__(id)
        self.label = None
        self.always_enabled = always_enabled
        if label is not None:
            self.label = Label(label)
            self.label.always_enabled = always_enabled
            self.add_nds(self.label)
        # for mouseover behavior
        self.bgrestore = None

    def get_tkpeers(self):
        if self.label is not None:
            return [self.label.peer,self.peer]
        else:
            return [self.peer]

    def set_mouseover(self):
        self.peer.bind('<Enter>',self.on_enter)
        self.peer.bind('<Leave>',self.on_leave)

    def on_enter(self,ev):
        if self.peer['state'] == tki.NORMAL:
            self.bgrestore = self.peer.cget('bg')
            self.peer['bg'] = _get_contrast_color(self.bgrestore)

    def on_leave(self,ev=None):
        if self.peer['state'] == tki.NORMAL:
            if self.bgrestore is not None:
                self.peer['bg'] = self.bgrestore
        self.bgrestore = None


class Label(_Widget):
    def __init__(self,text,always_enabled=False,id=None,local_style=None):
        super().__init__(id,always_enabled=always_enabled)
        self.text = text
        self.local_style = local_style

    def set_text(self,text):
        self.peer['text'] = self.text = text
        #self.peer.update()

    def get_text(self):
        return self.text

    def set_style(self,dct):
        _styles.apply_local_style(self.peer,dct)

    def make(self,parent):
        if self.made:
            return
        self.made = True
        self.peer = tki.Label(parent,text=self.text)
        _styles.apply_style(self.peer,self.cntxt,'label',self.local_style)
        # borderwidth is always 0
        self.peer['borderwidth'] = 0
        self.peer.lopadx = [4,0]

class Button(Label):
    def __init__(self,text,cmd,
            always_enabled=False, id=None, local_style=None,retain_dd=False):
        super().__init__(text,always_enabled=always_enabled,
                id=id,local_style=local_style)
        self.cmd = cmd
        self.retain_dd = retain_dd
        # is this a button/menuitem that controls a drop-down menu?
        self.is_dd_cntrl = False

    def make(self,parent):
        if self.made:
            return
        self.made = True
        self.peer = tki.Label(parent, text=self.text,padx=8,pady=2)
        _styles.apply_style(self.peer,self.cntxt,'button',self.local_style)
        self.set_mouseover()
        self.peer.bind("<Button-1>",self.do_cmd)

    def do_cmd(self,ev):
        if not self.enabled:
            return
        self.on_leave()
        if self.is_dd_cntrl:
            time.sleep(.05)
            self.cmd()
            return
        if not self.retain_dd:
            close_all_dropdowns()
        bgsave = self.peer['bg']
        self.peer['bg'] = _get_contrast_color(bgsave)
        self.peer.update()
        time.sleep(.2)
        self.peer['bg'] = bgsave
        self.cmd()

class ImageButton(_Widget):
    def __init__(self,img_pil,cmd,size=48,always_enabled=False,id=None):
        super().__init__(id,always_enabled=always_enabled)
        self.im_pil = img_pil.convert('RGBA')
        self.im_pil.thumbnail((size,size),Image.ANTIALIAS)
        self.cmd = cmd

    def make(self,parent):
        if self.made:
            return
        self.made = True
        im_tk = ImageTk.PhotoImage(self.im_pil,master=parent)
        self.peer = tki.Button(parent, image=im_tk,command=self.cmd,
            relief=tki.GROOVE)
        self.peer['bg'] = self.parent.peer.cget('bg')
        # needed for mac version
        self.peer['highlightbackground'] = self.peer['bg']
        self.set_mouseover()
        self.peer.im_tk = im_tk
        # needed for set_bg
        self.parent = parent

    def set_bg(self,color):
        im_pil = self.im_pil
        if color is None:
            im_new = im_pil
        else:
            im_new = Image.new("RGBA",self.im_pil.size,color)
            im_new.paste(im_pil,(0,0),mask=im_pil)
        im_tk = ImageTk.PhotoImage(im_new,master=self.parent)
        self.peer['image'] = im_tk
        self.peer.update()
        self.peer.im_tk = im_tk


class Entry(_Widget):
    def __init__(self,id,w,label=None,always_enabled=False):
        super().__init__(id,always_enabled=always_enabled,label=label)
        self.w = w
        self.tvar = tki.StringVar()

    def get(self):
        return self.tvar.get()

    def set(self,v):
        self.tvar.set(v)

    def set_default(self,v):
        if self.tvar.get() == '':
            self.tvar.set(v)

    def make(self,parent):
        if self.made:
            return
        self.made = True
        if self.label is not None:
            self.label.make(parent)
        self.peer = tki.Entry(
            parent, textvariable=self.tvar,width=self.w)
        _styles.apply_style(self.peer,self.cntxt,'entry')
        # needed for mac
        self.peer['highlightthickness'] = 0

class NumEntry(Entry):
    def __init__(self,id, w, _format='%d', label=None, always_enabled=False):
        super().__init__(id,w,label=label,always_enabled=always_enabled)
        self._format = _format

    def get(self, widget_name, required=True, vmin=None, vmax=None,
            emsg='%s must be a number'):
        def restore_dd():
            try:
                pp = self.parent.parent
                if isinstance(pp,DropdownMenu):
                    pp.set_visible(True)
            except:
                pass
        v = super().get()
        if v == '':
            if required:
                restore_dd()
                showerror( '%s is required' % widget_name)
            return None
        try:
            if self._format == '%d':
                v = int(v)
            else:
                v = float(v)
        except:
            restore_dd()
            showerror(emsg % widget_name)
            return None
        if vmin is not None:
            v = max(vmin,v)
        if vmax is not None:
            v = min(vmax,v)
        return v

    def set(self,v):
        if v is None:
            super().set('')
        else:
            super().set(self._format % v)

    def set_default(self,v):
        if super().get() == '':
            self.set(v)

    def is_empty(self):
        return super().get() == ''

    def make(self,parent):
        if self.made:
            return
        self.made = True
        if self.label is not None:
            self.label.make(parent)
        self.peer = tki.Entry(
            parent, textvariable=self.tvar,width=self.w)
        _styles.apply_style(self.peer,self.cntxt,'entry')
        # needed for mac
        self.peer['highlightthickness'] = 0

class Range(_Widget):
    def __init__(self,id,w,_format='%d',label=None,always_enabled=False):
        super().__init__(id,always_enabled=always_enabled,label=label)
        self.lo = NumEntry(None,w,_format=_format,
                    always_enabled=always_enabled)
        self.dotdot = Label('..',always_enabled=always_enabled)
        self.hi = NumEntry(None,w,_format=_format,
                    always_enabled=always_enabled)
        self.add_nds((self.lo,self.dotdot,self.hi))

    def set(self,v):
        if v is None:
            self.lo.set(None)
            self.hi.set(None)
        else:
            self.lo.set(v[0])
            self.hi.set(v[1])

    def get(self, widget_name, required=True, vmin=None, vmax=None,
        emsg='%s must be numbers'):
        lo = self.lo.get(widget_name, required,
                vmin=vmin,vmax=vmax, emsg=emsg)
        if lo is None:
            return None
        hi = self.hi.get(widget_name, required,
                vmin=vmin,vmax=vmax, emsg=emsg)
        if hi is None:
            return None
        if lo > hi:
            showerror(
        '%s range should be low .. hi' % widget_name)
            return None
        return (lo,hi)

    def make(self,parent):
        if self.made:
            return
        self.made = True
        #self.lo.make(parent)
        #self.dotdot.make(parent)
        #self.dotdot.peer.lopadx = [4,4]
        #self.hi.make(parent)
        for nd in self.nds:
            nd.make(parent)
        self.dotdot.peer.lopadx = [4,4]

    def get_tkpeers(self):
        #return [self.lo.peer,self.dotdot.peer,self.hi.peer]
        return [nd.peer for nd in self.nds]

class _Icon(_Widget):
    def __init__(self,im_w,im_h,onclick=None):
        self.im_w = im_w
        self.im_h = im_h
        self.onclick = onclick
        super().__init__()

    def _onclick(self,ev):
        if self.onclick is not None:
            self.onclick(ev.x,ev.y)

    def make(self,container):
        self.peer = tki.Canvas(container,
                highlightthickness=0,
                height=self.im_h,width=self.im_w)
        if self.onclick is not None:
            self.peer.bind("<Button-1>",self._onclick)

class _CheckboxBase(_Widget):
    # base class for Checkbox and Radiobutton
    # "qual" is a style qualifier: 'checkbox' or 'radiobutton'
    def __init__(self,id,text,qual,w_icon=13,onchange=None):
        super().__init__(id)
        self.qual = qual
        self.label = Button(text,self.onclick)
        self.icon = _Icon(w_icon,w_icon,onclick=self.onclick)
        self.onchange = onchange
        self.checked = False

    def onclick(self,*args):
        # implemented by child classes
        pass

    def draw_icon(self):
        # implemented by child classes
        pass

    def get_icon_colors(self):
        bg = _styles.get_qualified_value(self.cntxt,'bg',self.qual)
        if bg is None:
            bg = _styles.get_attr_value(self.cntxt,'fg','label')
        fg = _styles.get_qualified_value(self.cntxt,'fg',self.qual)
        if fg is None:
            fg = _styles.get_attr_value(self.cntxt,'bg','label')
        return (fg,bg)

    def is_enabled(self):
        #return self.label.peer['state'] == tki.NORMAL
        return self.enabled

    def set_enabled(self,vBool):
        super().set_enabled(vBool)
        self.draw_icon()

    def make(self,parent):
        if self.made:
            return
        self.made = True
        self.icon.make(parent)
        self.icon.peer.lopadx = [4,0]
        self.label.make(parent)
        self.label.peer.lopadx = [0,4]
        _styles.apply_style(self.label.peer,self.cntxt,self.qual)
        self.label.peer['bg'] = parent.cget('bg')
        self.label.peer['bd'] = 0
        self.label.peer['pady'] = 0
        self.peer = self.label.peer
        self.set_mouseover()
        self.draw_icon()

    def get_tkpeers(self):
        return [self.icon.peer,self.label.peer]

class Checkbox(_CheckboxBase):
    def __init__(self,id,text,onchange=None):
        if id is None:
            raise TkaltError('Checkbox requires id')
        if not id.isidentifier(): 
            raise TkaltError('"%s" is not a valid identifier' % id)

        super().__init__(id,text,'checkbox',13,onchange)

    def set(self,vbool):
        self.checked = vbool
        self.draw_icon()

    def get(self):
        return self.checked

    def onclick(self,*args):
        if not self.is_enabled():
            return
        self.set(not self.checked)
        if self.onchange is not None:
            self.onchange()

    def draw_icon(self):
        can = self.icon.peer
        can.delete("all")
        (fg,bg) = self.get_icon_colors()
        if not self.is_enabled():
            can['bg'] = self.label.peer.cget('disabledforeground')
            return
        dim = self.icon.im_w
        can['bg'] = bg
        if self.checked:
            can.create_line(
                1,int(.66*dim)-1,
                1+int(.33*dim),dim-1,
                dim-1,int(.33*dim-1),
                fill=fg,width=3)

class Radiobutton(_CheckboxBase):
    def __init__(self,id,text,value,onchange=None):
        if id is None:
            raise TkaltError('Radiobutton requires id')
        if not id.isidentifier(): 
            raise TkaltError('"%s" is not a valid identifier' % id)
        # id binding is done after the initialization
        super().__init__(None,text,'radiobutton',15,onchange)
        self.value = value
        # id is bound to the first radiobutton with this id;
        # that button gets the currently selected value and
        # collection of buttons ("grp")
        if hasattr(ui,id):
            first_button = getattr(ui,id)
            if not isinstance(first_button,Radiobutton):
                raise TkaltError('"%s" already in use' % id)
            self.first_button = first_button
            first_button.grp.append(self)
        else:
            self.first_button = self
            setattr(ui,id,self)
            self.sel_value = value
            self.checked = True
            self.grp = [self]
 
    def set(self,v):
        first = self.first_button
        v_cur = first.sel_value
        first.sel_value = None
        found_but = False
        for e in first.grp:
            if e.value == v:
                e.checked = True
                first.sel_value = v
                found_but = True
            else:
                e.checked = False
            e.draw_icon()
        if not found_but:
            # At this point all buttons were set to off, and
            # sel_value set to None.
            if v is not None:
                raise TkaltError(
            'Radiobutton set error. Unrecognized value: %s',v)

    def _set_enabled(self,v):
        super().set_enabled(v)

    def set_enabled(self,v):
        first = self.first_button
        for e in first.grp:
            e._set_enabled(v)

    def onclick(self,*argv):
        if not self.is_enabled():
            return
        first = self.first_button
        v_cur = first.sel_value
        self.set(self.value)
        if v_cur != self.value and first.onchange is not None:
            first.onchange()

    def get(self):
        return self.first_button.sel_value

    def draw_icon(self):
        can = self.icon.peer
        can.delete("all")
        (fg,bg) = self.get_icon_colors()
        bgframe = _styles.get_attr_value(self.parent.cntxt,'bg','frame')
        dim = self.icon.im_w
        can['bg'] = bgframe
        if not self.is_enabled():
            bg = self.label.peer.cget('disabledforeground')
            can.create_oval(
                0,0,
                dim-1,dim-1,
                fill=bg)
            return
        can.create_oval(
            0,0,
            dim-1,dim-1,
            fill=bg)
        if self.checked:
            can.create_oval(
                #2,2,
                #9,9,
                3,3,
                dim-4,dim-4,
                fill=fg)

class Textbox(_Widget):
    def __init__(self,id,w,h):
        # id: ui id for this widget
        # w: width of textbox, as number of characters
        # h: height of textbox, as number of lines 
        super().__init__(id)
        self.w = w
        self.h = h

    def set(self,text):
        self.peer.delete('1.0', tki.END)
        self.peer.insert('1.0',text)

    def get(self):
        return self.peer.get('1.0','end')

    def make(self,parent):
        if self.made:
            return
        self.made = True
        self.peer = tki.Text(
            parent, width=self.w, height=self.h,
            relief=tki.GROOVE)

class Hr(_Widget):
    # horizontal rule for dividing a pane into regions
    def __init__(self):
        super().__init__(None)

    def make(self,parent):
        if self.made:
            return
        self.made = True
        color = _styles.get_attr_value(self.cntxt,'fg','panel')
        if color is None:
            color = 'black'
        self.peer = tki.Frame(master=parent, highlightthickness=0,
                bg=color,height=1,relief=tki.GROOVE)

# List of all ddmenu objects: used to force closure
# (only 1 ddmenu can be open at a time).
_all_dds = []

def close_all_dropdowns(ev=None):
    # close all drop down menus
    for dd in _all_dds:
        try:
            if dd.visible:
                dd.set_visible(False)
        except AttributeError:
            # this happens if we're called during the make
            # for the Layout.
            pass

class _DDWidget(_Widget):
    def __init__(self,cntxt,anchor,ddframe, id=None,label=None,
            always_enabled=False):
        super().__init__(id,label,always_enabled)
        self.cntxt = cntxt
        self.anchor = anchor
        self.ddframe = ddframe
        self.add_nds(anchor)
        self.add_nds(ddframe)
        self.visible = False
        _all_dds.append(self)

    def set_visible(self,visible):
        self.visible = visible
        ddf_peer = self.ddframe.peer
        if visible:
            # close any other open dropdowns
            # If this dropdown is an element of another dropdown,
            # that other dd is called "dd_base". Keep that open,
            # and get a ref to it.
            dd_base = None
            # close any other open dropdowns
            for dd in _all_dds:
                if dd != self and dd.visible:
                    # The if test means: if we're an element in an open
                    # dropdown
                    if self.parent in dd.nds:
                        dd_base = dd
                    else:
                        dd.set_visible(False)
            # get lo for our anchor
            (x,y,w,h) = _get_lopeer(self.anchor.peer)
            if dd_base is None:
                # place directly below the anchor
                ddf_peer.place(x=x,y=(y+h))
                # get width of ddframe. Note the call to winfo_width
                # has to happen AFTER the ddframe has been placed
                # (otherwise it just returns 1)
                ddf_peer.update()
                w_dd = ddf_peer.winfo_width()

                # get lo of frame which will contain the dd; get x coord
                # of its right side
                (x_fr,y_fr,w_fr,h_fr) = _get_lopeer(self.alignment_frame.peer)
                xright_fr = x_fr+w_fr

                # get excess: amount by which dd exceeds right edge of 
                # container
                xright_dd = x + w_dd
                excess = xright_dd - xright_fr
                if excess > 0:
                    # move dd to the left
                    x -= excess
                    ddf_peer.place(x=x,y=(y+h))
                
            else:
                # place our frame to the right of the base's frame
                (xb,yb,wb,hb) = _get_lopeer(dd_base.ddframe.peer)
                ddf_peer.place(
                    x=xb+wb,
                    y=y)
            # This works on windows, fails mac. The "after" version 
            # works on both.
            #ddf_peer.lift()
            ddf_peer.after(1,ddf_peer.lift)
        else:
            # This works on windows, fails mac
            #ddf_peer.place_forget()
            ddf_peer.lower()
            # close any dropdown children: their parent will be
            # our ddframe.
            for dd in _all_dds:
                if dd.parent == self.ddframe:
                    dd.set_visible(False)

    def toggle_visible(self):
        self.set_visible(not self.visible)

    def make(self,container):
        if self.label is not None:
            self.label.make(container)
        # The anchor is styled by our parent. Its peer becomes our peer
        self.anchor.cntxt = self.parent.cntxt
        self.anchor.make(container)
        self.peer = self.anchor.peer 
        # drop down is child of _root
        self.ddframe.ixrow = -1
        self.ddframe.make(_root)
        self.set_visible(False)

    def get_tkpeers(self):
        if self.label is not None:
            return [self.label.peer,self.peer]
        else:
            return [self.peer]

class DropdownMenu(_DDWidget):
    # A button which, when clicked, expands to a drop down menu.
    # The constructor accepts a cntxt arg: that applies
    # to the expanded menu.
    def __init__(self,_id,title,cntxt,on_visible_change,*content):
        self.on_visible_change = on_visible_change
        if isinstance(title,str):
            # create a button to be the dd_cntrl
            self.but = Button(title,self.on_but,always_enabled=True)
            self.but.is_dd_cntrl = True
        elif isinstance(title,_Icon):
            # use icon onclick as the dd_cntrl
            self.but = title
            title.onclick = self.on_but 
        self.ddframe = Frame(None,cntxt,content)
        super().__init__(cntxt,self.but,self.ddframe,id=_id)
        self.always_enabled = True

    def on_but(self,*args):
        self.toggle_visible()
        if self.on_visible_change is not None:
            self.on_visible_change(self.visible)

    def get_tkpeers(self):
        return [self.but.peer]

class OptionMenu(_DDWidget):
    def __init__(self,id,choices,label=None,onchange=None):
        # id: ui id for this widget 
        # choices: the options (list of Strings)
        # label: label for this widget 
        # onchange: callback function: user changed selection 

        #if id is None:
            #raise TkaltError('OptionMenu requires id')
        self.onchange = onchange
        if len(choices) == 0:
            choices.append('')
        self.best = Button(choices[0],self.on_best)
        self.alts = []
        for i in range(0,len(choices)):
            self.alts.append( Button(choices[i],
            lambda ix=i: self.on_alt(ix)))
        self.best.is_dd_cntrl = True
        ddframe = Frame(None,None,self.alts)
        super().__init__(None,self.best,ddframe,id=id,label=label)
        self.add_nds(self.best)
        self.add_nds(self.alts)
        self.icon = _Icon(11,11,onclick=self.on_best)

    def get(self):
        return self.best.get_text()


    def set(self,v):
        self.best.set_text(v)

    def on_best(self,*args):
        # toggle visiblity
        self.toggle_visible()
        self.draw_icon()

    def set_visible(self,visible):
        super().set_visible(visible)
        try:
            self.draw_icon()
        except AttributeError:
            # startup
            pass

    def on_alt(self,ix):
        newchoice = self.alts[ix].get_text()
        self.best.set_text(newchoice)
        # close drop down. First restore buttons to non-mouseover
        # view.
        self.best.on_leave()
        for e in self.alts:
            e.on_leave()
        self.set_visible(False)
        self.draw_icon()
        if self.onchange is not None:
            self.onchange()

    def draw_icon(self):
        bg = self.ddframe.peer.cget('bg')
        fg = _styles.get_attr_value(self.cntxt,'control','optionmenu')
        if fg is None:
            fg = self.best.peer['fg']
        can = self.icon.peer
        dim = self.icon.im_w
        can.delete('all')
        can['bg'] = bg
        if self.visible:
            can.create_polygon(
                int(dim/2),1,
                0,dim-1,
                dim-1,dim-1,
                fill=fg)
        else:
            can.create_polygon(
                0,2,
                dim-1,2,
                int(dim/2),dim-1,
                fill=fg)

    def make(self,container):
        super().make(container)
        fg = _styles.get_attr_value(self.cntxt,'fg','optionmenu')
        bg = _styles.get_attr_value(self.cntxt,'bg','optionmenu')
        buts = [self.best]
        buts.extend(self.alts)
        for b in buts:
            b.peer['bg'] = bg
            b.peer['fg'] = fg
            b.peer['borderwidth'] = 0
            b.peer['padx'] = 4
            b.peer['pady'] = 0
            b.set_mouseover()
        bd = _styles.get_attr_value(
            self.parent.cntxt,'border','optionmenu')
        if bd is not None:
            bd = max(2,bd)
            self.ddframe.peer['bd'] = 2
            self.ddframe.peer['relief'] = 'groove'
        self.icon.make(container)
        self.draw_icon()

    def get_tkpeers(self):
        if self.label is not None:
            return [self.label.peer,self.peer,self.icon.peer]
        else:
            return [self.peer,self.icon.peer]

class _ListEntry():
    def __init__(self,ix,text,parent,onclick):
        self.ix = ix
        peer = self.peer = tki.Label(parent,text=text)
        peer['borderwidth'] = 0
        peer['relief'] = 'groove'
        peer.grid(row=ix,column=0,padx=[4,4],pady=[2,2])
        self.onclick = onclick
        if onclick is not None:
            peer.bind("<Button-1>", self.on_mousedown)

    def on_mousedown(self,ev):
        self.onclick()

class ListView(Frame):
    # A list view
    def __init__(self, id, emptylistmsg,onselchange=None):
        super().__init__(id,None,[])
        self.emptylistmsg = emptylistmsg
        self.ixsel = -1
        self.entries = []
        self.onselchange = onselchange

    def validate(self,model,ixsel=-1):
        self.entries = []
        frame = self.peer
        for widget in frame.winfo_children():
            widget.destroy()
        items = model[:]
        if len(items) == 0:
            items.append(self.emptylistmsg)
        for i in range(0,len(items)):
            onclick = None
            if len(model) > 0:
                onclick = lambda ix=i: self.set_ixsel(ix)
            entry = _ListEntry(i,items[i],frame,onclick)
            self.entries.append(entry)
            peer = entry.peer
            _styles.apply_style(peer,self.cntxt,'entry')
            entry.bgsave = peer['bg']
        self.set_ixsel(ixsel)

    def set_ixsel(self,ix):
        selchanged = ix != self.ixsel
        self.ixsel = ix
        for i in range(0,len(self.entries)):
            entry = self.entries[i]
            bg = entry.bgsave
            if i == ix:
                bg = _get_contrast_color(entry.peer['fg'])
            entry.peer['bg'] = bg
        if self.onselchange is not None and selchanged:
            self.onselchange()


    def get_ixsel(self):
        return self.ixsel

    def make(self,parent):
        if self.made:
            return
        super().make(parent)
        _styles.apply_style(self.peer,self.cntxt,'entry')
        self.peer['bd'] = 2
        self.peer['relief'] = 'groove'

class ParamControl(NumEntry):
    def __init__(self,id, reset_v,
            w, _format='%d', label='', always_enabled=False):

        super().__init__(id, w,
            _format=_format, label=label, always_enabled=always_enabled)

        self.reset_v = reset_v
        self.dec_but = Button('<', 
                lambda: self.reset_v(self,'-'),
                retain_dd=True)
        self.inc_but = Button('>', 
                lambda: self.reset_v(self,'+'),
                retain_dd=True)
        self.add_nds([self.dec_but,self.inc_but])

    def get(self,title=None):
        # "title" needed for error msg if value is malformed
        if title is None:
            if self.label is not None:
                title = self.label.text
            else:
                title = 'Value'
        return super().get(title)

    def make(self,parent):
        super().make(parent)
        self.dec_but.make(parent)
        self.inc_but.make(parent)
        self.dec_but.peer.lopadx = [0,0]
        self.inc_but.peer.lopadx = [0,4]

    def get_tkpeers(self):
        peers = super().get_tkpeers()
        peers.append(self.dec_but.peer)
        peers.append(self.inc_but.peer)
        return peers

class RangeParamControl(ParamControl):
    # ParamControl for range lowbound .. hibound
    def __init__(self,id,label, 
            width=5, _format='%.2f',
            lb=0.0, hb= 1.0, incr=0.05,
            onchange=None,always_enabled=False):

        super().__init__(id, None,
            width, _format=_format, label=label, always_enabled=always_enabled)
        self.lb = lb
        self.hb = hb
        self.incr = incr
        self.onchange = onchange
        self.reset_v = self.reset_by_incr

    def reset_by_incr(self,src,sign):
        v = self.get()
        if v is None:
            # error was reported
            return
        if sign == '+':
            v += self.incr
        else:
            v -= self.incr
        if self.lb is not None:
            v = max(v,self.lb)
        if self.hb is not None:
            v = min(v,self.hb)
        self.set(v)
        if self.onchange is not None:
            self.onchange()

    def get(self,title=None):
        v = super().get()
        if v is None:
            # error reported
            return None
        if self.lb is not None and v < self.lb:
            v = self.lb
            self.set(v)
        if self.hb is not None and v > self.hb:
            v = self.hb
            self.set(v)
        return v

class ColorControl(_Widget):
    # An Entry box for an html color code,
    # plus a little swatch showing the selected color.
    # Clicking the swatch toggles a
    # dropdown menu with color chart and
    # saturation/luminosity controls.
    def __init__(self, id):
        # id: ui id for this widget 
        super().__init__(id)
        self.colorcode = Entry(None,8)
        self.add_nds(self.colorcode)
        self.icon = _Icon(16,16)
        self.colors = [
    ['#000000', '#646464', '#ff0000', '#ff6a00',
        '#ffd800', '#baff00', '#4cff00', '#00ff90'],
    ['#ffffff', '#a0a0a0', '#fee3d4', '#ff00de',
        '#b200ff', '#0026ff', '#0094ff', '#00ffff']
    ]
        self.chart = _Icon(161,41,onclick=self.onclick_chart)

        self.hue = ParamControl(None,self.reset_param,
                    4,'%d','Hue')
        self.lum = ParamControl(None,self.reset_param,
                    4,'%.2f','Luminosity')
        self.sat = ParamControl(None,self.reset_param,
                    4,'%.2f','Saturation')

        self.dd_chart = DropdownMenu(None,self.icon,'colorcontrol',None,
                [self.chart],
                [self.hue],
                [self.lum],
                [self.sat],
                #[Button('Apply',self.apply_hls,always_enabled=True,
                    #retain_dd=True)
                #]
            )
        self.add_nds(self.dd_chart)

    def get(self,widget_name,six_digits=True):
        code = self.colorcode.get()
        if not self.isHexColor(code):
            showerror('%s must be an HTML-style hex code' % widget_name)
            return None
        if len(code) == 4:
            digits = list(code[1:])
            _digits = []
            for d in digits:
                _digits.append(d)
                _digits.append(d)
            code = '#' + ''.join(_digits)
        return code

    def set(self,v):
        self.colorcode.set(v)
        self.draw_icon()
        self.validate_hls()

    def set_default(self,v):
        if self.colorcode.get()=='':
            self.set(v)

    def validate_hls(self):
        if self.dd_chart is not None:
            code = self.colorcode.get()
            if not self.isHexColor(code):
                self.hue.set(None)
                self.lum.set(None)
                self.sat.set(None)
            else:
                (h,l,s) = colors.hex_to_hls(code)
                self.hue.set(int(h*360))
                self.lum.set(l)
                self.sat.set(s)

    def onclick_chart(self,x,y):
        rowix = int(y/20)
        colix = int(x/20)
        #print('rowix, colix: %d %d' % (rowix,colix))
        self.set(self.colors[rowix][colix])

    def isHexColor(self,color):
        return re.search(r'^#(?:[0-9a-fA-F]{3}){1,2}$', color) is not None

    def apply_hls(self):
        (h,l,s) = (self.hue.get(),self.lum.get(),self.sat.get())
        if h is None or l is None or s is None:
            # Error was reported
            return
        h = float(h)/360.0
        colorcode = colors.hls_to_hex(h,l,s)
        self.colorcode.set(colorcode)
        self.draw_icon()

    def reset_param(self,param,sign):
        v = param.get()
        if v is None:
            # error was reported
            return

        if sign == '+':
            if param == self.hue: 
                param.set( int(360 + v + 5) % 360 )
            else:
                # sat or lum
                v += .05
                v = min(1.0,v)
                param.set(v)
        else:
            if param == self.hue: 
                param.set( int(360 +v - 5) % 360 )
            else:
                # sat or lum
                v -= .05
                v = max(v,0)
                param.set(v)
        self.apply_hls()

    def draw_icon(self):
        can = self.icon.peer
        can.delete("all")
        code = self.colorcode.get()
        if not self.isHexColor(code):
            can['bg'] = '#ffffff'
            can.create_line(3,3,13,13,fill='#000000',width=2)
            can.create_line(13,3,3,13,fill='#000000',width=2)
        else:
            can['bg'] = code
        color = _styles.get_attr_value(self.cntxt,'fg','label')
        can.create_polygon(
                0,0,15,0,15,15,0,15,
                fill='',
                outline=color)

    def on_code_changed(self,*args):
        self.draw_icon()
        self.validate_hls()

    def make(self,parent):
        if self.made:
            return
        self.made = True
        self.colorcode.make(parent)
        _styles.apply_style(self.colorcode.peer,self.cntxt,'entry')
        self.icon.make(parent)
        if self.dd_chart is not None:
            self.dd_chart.make(parent)
            self.dd_chart.alignment_frame = self.alignment_frame
            # draw the chart
            can = self.chart.peer
            can.delete("all")
            can['bg'] = 'white'
            DIM_CELL = 20
            for ixrow in range(0,2):
                for ixcol in range(0,8):
                    c = self.colors[ixrow][ixcol]
                    x = ixcol*DIM_CELL
                    y = ixrow*DIM_CELL
                    can.create_rectangle(x,y,x+DIM_CELL,y+DIM_CELL,
                        fill=c, outline='#000000')

        # set up callback on colorcode changed
        self.colorcode.tvar.trace_add("write",self.on_code_changed)

    def get_tkpeers(self):
        if self.dd_chart is None:
            peers = []
        else:
            peers = self.dd_chart.get_tkpeers()
        peers.extend([self.icon.peer, self.colorcode.peer])
        return peers

