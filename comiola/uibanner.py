# Copyright Al Cramer 2021 ac2.71828@gmail.com
import os
import webbrowser

import scripts
import controls
from tkalt import *
import resources as res
import uiimgedit
import uiclips
import uiglobals

import display
import images

def validate_view():
    ui.proj_dir.set(scripts.proj_filepath)
    ui.ixgoto.set(None)
    ui.goto_label.set_text('of 0')
    ui.new_ar_w.set(1)
    ui.new_ar_h.set(1)
    disable_widget(ui.banner)
    if not scripts.script_open():
        return
    if scripts.cnt_shots() == 0:
        enable_widget(save_project,saveas_project,close_project,
                edit_image,edit_clips)
        return
    enable_widget(ui.banner)
    ui.ixgoto.set(1+display.ixshot)
    ui.goto_label.set_text('of %d' % scripts.cnt_shots())
    if display.ixshot == 0:
        disable_widget(goto_prv)
    if display.ixshot >= scripts.cnt_shots() -1:
        disable_widget(goto_nxt)
    ui.proj_dir.set_enabled(False)

def goto():
    ix = ui.ixgoto.get('Shot index', vmin=1, vmax=scripts.cnt_shots())
    if ix is not None:
        display.edit_shot(ix-1)

def goto_prv():
    ix = display.ixshot-1
    if ix >= 0:
        display.edit_shot(ix)

def goto_nxt():
    ix = display.ixshot +1
    if ix <= scripts.cnt_shots()-1:
        display.edit_shot(ix)

def new_project():
    name = ui.new_project_name.get()
    if name == '':
        file_dropdown.set_visible(True)
        showerror('You must enter a name for the project')
        ui.dd_filenew.set_visible(True)
        return
    ar_w = ui.new_ar_w.get().strip()
    ar_h = ui.new_ar_h.get().strip()
    if ar_w == '' or ar_h == '':
        file_dropdown.set_visible(True)
        ui.dd_filenew.set_visible(True)
        showerror('Aspect ratio is required')
        return
    try:
        ar_w = int(ar_w)
        ar_h = int(ar_h)
    except:
        file_dropdown.set_visible(True)
        ui.dd_filenew.set_visible(True)
        showerror('Aspect ratio entries must be numbers')
        return
    d = askdirectory()
    if d is not None and d != '':
        if scripts.open_project(d,name,True,ar=ar_h/float(ar_w)):
            ui.proj_dir.set(scripts.proj_filepath)
        uiglobals.validate_view()


def open_project():
    fp = askopenfilename( filetypes=[('Comiola File','.cprj')])
    if fp == '':
        return
    # this needed if a previous project was opened
    res.clear_cache()
    (head,tail) = os.path.split(fp)
    if scripts.open_project(head,tail,False):
        display.edit_shot(0)
        uiglobals.validate_view()

def save_project():
    scripts.save_script(scripts.proj_name)

def saveas_project():
    name = ui.saveas_name.get().strip()
    if name == '':
        showerror(
            'You must enter a name for "saveas"')
        return
    scripts.save_script(name)
    ui.proj_dir.set(scripts.proj_filepath)

def close_project():
    if not scripts.script_changed():
        scripts.close_project()
        #uiglobals.validate_view()
        uiglobals.show_view('main')
        return

    action  = askyesnocancel('Save project?')
    if action is None:
        # user choose "Cancel"
        return
    if action:
        # user choose "Yes" (save project)
        scripts.save_script(scripts.proj_name)
    scripts.close_project()
    uiglobals.validate_view()

def on_faq():
    webbrowser.open_new_tab('file://' +
        os.path.join(res.get_assets_dir('faq'),'faq.htm')
    )

def zoomin():
    if uiglobals.get_cur_view_id() == 'imgedit':
        uiimgedit.vc.zoomin()
    else:
        display.vc.zoomin()

def zoomout():
    if uiglobals.get_cur_view_id() == 'imgedit':
        uiimgedit.vc.zoomout()
    else:
        display.vc.zoomout()

def edit_image():
    uiimgedit.validate_view()
    uiglobals.show_view('imgedit')

def edit_clips():
    uiclips.validate_view()
    uiglobals.show_view('clipsedit')

# create the ui
file_dropdown = None
def get_ui():
    global file_dropdown
    file_dropdown = DropdownMenu(None,'File',None,None,
            DropdownMenu('dd_filenew','New',None,None,
                Entry('new_project_name',14,'Name:',always_enabled=True),
                [
                    Entry('new_ar_w',2,label='Aspect Ratio',always_enabled=True),
                    Entry('new_ar_h',2,label=':',always_enabled=True),
                ],
                Button('Select Folder',new_project,always_enabled=True),
            ),
            Button('Open',open_project,always_enabled=True),
            Button('Save',save_project),
            [
                Button('Save as',saveas_project),
                Entry('saveas_name',14,'Name:'),
            ],
            Button('Close Project',close_project),
            Hr(),
            Button('Images',edit_image),
            Button('Clips',edit_clips),
            )

    return [[
        file_dropdown,
        Entry('proj_dir',40),
        Button('+',zoomin,always_enabled=True),
        Button('-',zoomout,always_enabled=True),
        Button('GOTO',goto),
        NumEntry('ixgoto',4,_format='%d'),
        Label('of 0',id='goto_label'),
        Button('<',goto_prv),
        Button('>',goto_nxt),
        Button('Help',on_faq,always_enabled=True)
        ]]

