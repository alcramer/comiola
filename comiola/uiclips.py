# Copyright Al Cramer 2021 ac2.71828@gmail.com
from PIL import ImageTk,Image
import shutil
import imageio
import scripts
from tkalt import *
import uiglobals

import resources as res
import images

class Mp4Viewer:
    def __init__(self,can,ixgoto_entry):
        self.can = can
        self.ixgoto_entry = ixgoto_entry
        self.fps = 24
        self.reader = None
        self.nframes = 0
        # pil image for the frame we're currently showing
        self.frame_pil = None

    def set_mp4(self,fp):
        try:
            self.reader.close()
        except:
            pass
        self.reader = None
        self.nframes = 0
        self.can.delete('all')
        if fp is None:
            return
        self.halted = False
        self.forward = True
        self.reader = imageio.get_reader(fp)
        self.nframes = self.reader.count_frames()
        # read and draw first frame
        self.ixframe = 0
        self.show_frame()

    def show_frame(self):

        def get_thumbnail_dims(MAXDIM,ar):
            if ar <= 1.0:
                w = MAXDIM
                h = int(w* ar)
            else:
                h = MAXDIM
                w = int(h/ar)
            return (w,h)

        reader = self.reader
        can = self.can
        reader.set_image_index(self.ixframe)
        img_pil = Image.fromarray(reader.get_next_data())
        # save the raw image: needed for "Save frame"
        self.frame_pil = img_pil
        # resize image so max dim is 300
        MAXDIM = 300
        (w_pil,h_pil) = img_pil.size
        ar_pil = float(h_pil)/w_pil
        (w_tmp,h_tmp) = get_thumbnail_dims(MAXDIM,ar_pil)
        img_tmp = img_pil.resize((w_tmp,h_tmp),Image.ANTIALIAS)
        # get dimensions for dst image: max dim is MAXDIM
        ar_dst = scripts.script.ar_shot
        (w_dst,h_dst) = get_thumbnail_dims(MAXDIM,ar_dst)
        # get clip rectange for tmp->dst, then crop tmp
        clip_rect = images.get_resample_rect(w_tmp,h_tmp,w_dst,h_dst)
        img_dst = img_tmp.crop(clip_rect)
        img_tk = ImageTk.PhotoImage(img_dst,master=can)
        can.delete('all')
        can.create_image(20, 20, anchor='nw',image=img_tk)
        can.img_tk = img_tk
        #img_pil.close()
        self.ixgoto_entry.set(self.ixframe+1)

    def show_next_frame(self):
        if self.halted:
            return
        if self.forward:
            ix = self.ixframe + 1
            if ix >= self.nframes:
                # done
                return
        else:
            ix = self.ixframe - 1
            if ix < 0:
                # done
                return
        # compute wait period is millisecs
        wait = 1.0 / float(self.fps)
        wait = int( wait * 1000 )
        # show frame at ix; track time consumed
        t_start = time.time()
        self.ixframe = ix
        self.show_frame()
        t_end = time.time()
        t_elapsed = int( 1000 * (t_end - t_start))
        wait = max(1, wait - t_elapsed)
        # schedule next frame
        self.can.after( wait, self.show_next_frame) 

    def play(self,forward=True):
        self.halted = False
        self.forward = forward
        self.show_next_frame()

    def halt(self):
        self.halted = True

# filename root for mp4 we're editing
fnroot = ''
# was it modified, and changes not saved?
modified = False
# fp for source file: it will be installed in proj_dir as needed
fpsrc = ''
# fp for working copy: open_clip sets this to proj_dir/fntmp.mp4
fpwrk = ''
# the viewer
viewer = None

def post_status(msg):
    ui.clips_status_msg.peer['text'] = msg
    ui.clips_status_msg.peer.update()

def cleanup():
    try:
        viewer.reader.close()
    except:
        pass
    try:
        os.remove(fpwrk)
    except:
        pass

def on_exit():
    if not do_commit():
        return
    cleanup()
    viewer.set_mp4(None)
    uiglobals.show_view('main')

def do_commit():
    global modified
    scripts.script.clips.fnmerged = ui.mergeto_mp4.get()
    if modified:
        action  = askyesnocancel('Save changes to "%s"?' % fnroot)
        if action is None:
            # user choose "Cancel"
            return False
        if action:
            on_save_clip()
    return True

def load_clip():
    global modified,fpwrk
    cleanup()
    fpwrk =  res.get_fpart('__tmpwrk.mp4')
    shutil.copy( fpsrc, fpwrk)
    viewer.set_mp4(fpwrk)
    modified = False
    validate_view( ui.clips_listview.get_ixsel())


def on_open_clip():
    global fnroot,fpsrc
    fp = askopenfilename( initialdir=scripts.proj_dir,
            filetypes=
            [('MP4 Files','.mp4 .MP4')])
    if fp is None:
        return 
    (installed,fnroot,w,h) = res.install_mp4(fp,post_status)
    if not installed:
        showerror('Could not read "%s"' % fnroot) 
        return
    fpsrc =  res.get_fpart('%s.mp4' % fnroot)
    load_clip()


def on_save_clip():
    global modified
    if modified:
        try:
            shutil.copy(fpwrk,fpsrc)
        except:
            pass
        (head,root,ext) = res.parse_fp(fpsrc)
        scripts.on_clip_changed('%s.mp4' % root)
        modified = False

def on_saveas_clip():
    global modified
    fn = ui.saveas_name_clip.get()
    if len(fn) == 0:
        showerror(
            'You must enter a name for "saveas"')
        return
    if not fn.endswith('.mp4'):
        fn += '.mp4'
    try:
        shutil.copy(fpwrk,res.get_fpart(fn))
    except:
        pass
    modified = False
 

def on_play(which):
    if viewer is None:
        return
    if which == 'playleft':
        viewer.play(forward=False)
    elif which == 'playright':
        viewer.play(forward=True)
    else:
        viewer.halt()

# This code is not currently used
def set_fps_clip():
    if viewer is None:
        return
    try:
        fps = float(ui.fps_clip.get())
    except:
        showerror( "FPS (frames-per-second) must be a number")
        return
    viewer.fps = fps

def goto_clip():
    if viewer is None:
        return
    ix = ui.ixgoto_clip.get('Frame index')
    if ix is None:
        return
    viewer.ixframe = ix - 1
    viewer.show_frame()

def goto_prv_clip():
    if viewer is None:
        return
    ix = viewer.ixframe-1
    if ix >= 0:
        viewer.ixframe = ix
        viewer.show_frame()

def goto_nxt_clip():
    if viewer is None:
        return
    ix = viewer.ixframe+1
    if ix < viewer.nframes:
        viewer.ixframe = ix
        viewer.show_frame()

def validate_view(ixsel=-1):
    global viewer
    # create viewer as needed
    if viewer is None:
        viewer = Mp4Viewer(ui.can_clipsedit,ui.ixgoto_clip)
    ui.frame_clipsmerge.set_enabled(False)
    enable_widget(ui.move_clip_dir,ui.mergeto_mp4)
    ui.mergeto_mp4.set('')
    ui.saveas_name_clip.set('')
    if scripts.script_open():
        ui.mergeto_mp4.set( scripts.script.clips.fnmerged)
        ui.clips_listview.validate(scripts.script.clips.fnvideos,ixsel)
        model = scripts.script.clips.fnvideos
        ixsel = ui.clips_listview.get_ixsel()
        enable_widget(on_add_clip,on_open_clip)
        if len(model) > 0:
            enable_widget(on_mergeto_mp4)
            if ixsel != -1:
                enable_widget(on_remove_clip)
        if len(model) > 1 and ixsel != -1:
            enable_widget(on_move_clip)
    # right side: cut clip
    ui.frame_clipsedit.set_enabled(
        viewer.reader is not None)
    ui.goto_label_clip.peer['text'] = ('of %d' % viewer.nframes)
    ui.clips_range.set(None)

def on_add_clip():
    names = askopenfilenames( initialdir=scripts.proj_dir,
            filetypes=
            [('MP4 Files','.mp4 .MP4')])

    if len(names) == 0:
        return 
    # write mp4 to project dir as needed; add fn roots
    # to clips
    model = scripts.script.clips.fnvideos
    for fn in names:
        (installed,fnroot,w,h) = res.install_mp4(fn,post_status)
        if installed:
            model.append(fnroot)
        else:
            showerror('Could not read "%s"' % fnroot) 
    validate_view(len(model)-1)

def on_remove_clip():
    ixsel = ui.clips_listview.get_ixsel()
    model = scripts.script.clips.fnvideos
    del model[ixsel]
    validate_view(-1)

def on_move_clip():
    ixsel = ui.clips_listview.get_ixsel()
    model = scripts.script.clips.fnvideos
    if ui.move_clip_dir.get() == 'up':
        ixdst = ixsel -1
        if ixdst < 0:
            return
    else:
        ixdst = ixsel +1
        if ixdst >= len(model):
            return
    tmp = model[ixdst]
    model[ixdst] = model[ixsel]
    model[ixsel] = tmp
    validate_view(ixdst)

def onselchange():
    validate_view(ui.clips_listview.get_ixsel())

def on_mergeto_mp4():
    fnout = ui.mergeto_mp4.get()
    if fnout == '':
        showerror('You must provide a filename for "Merge to"')
        return
    clips = scripts.script.clips
    clips.fnmerged = fnout
    fpout = res.get_fpart('%s.mp4' % fnout)
    fpinlst = [res.get_fpart('%s.mp4' % fn) for fn in clips.fnvideos]
    images.merge_mp4s(fpout,fpinlst,post_status)

# This version uses moviepy.editor. It supports audio, but moviepy
# is buggy in the frozen windows version, so we use the version
# above. This version requires
# import moviepy.editor as mpe
def _on_mergeto_mp4():
    fnout = ui.mergeto_mp4.get()
    if fnout == '':
        showerror('You must provide a filename for "Merge to"')
        return
    clips = scripts.script.clips
    clips.fnmerged = fnout
    fpout = res.get_fpart('%s.mp4' % fnout)
    fpinlst = [res.get_fpart('%s.mp4' % fn) for fn in clips.fnvideos]
    post_status('Merging clips (this may take a while)')
    clips_loaded = [mpe.VideoFileClip(c) for c in fpinlst]
    merged = mpe.concatenate_videoclips(clips_loaded)
    merged.write_videofile(fpout, logger=None,fps=24)
    post_status('')
    for c in clips_loaded:
        c.close()
    merged.close()

def on_cut_clip():
    global modified
    _range = ui.clips_range.get('Frames range',required=True,
            vmin=1, vmax = viewer.nframes)
    if _range is None:
        return
    # write trimmed version to tmp, then copy back into fpwrk 
    fptmp = res.get_fpart('__tmp.mp4')
    images.cut_mp4(fpwrk, fptmp, _range[0]-1, _range[1]-1, post_status)
    shutil.copy(fptmp,fpwrk)
    viewer.set_mp4(fpwrk)
    try:
        os.remove(fptmp)
    except:
        pass
    validate_view()
    modified = True

def on_frame_clip():
    fn = ui.frame_name_clip.get().strip()
    if fn == '':
        showerror('Missing filename')
        return
    test = fn.lower()
    if test.endswith('.png') or test.endswith('.jpg'):
        fn = fn[:-4]
    fn = '%s.png' % fn
    viewer.frame_pil.save(res.get_fpart(fn))

def get_ui_menu():
    return [[
        DropdownMenu(None,'File','aux',None,
            Button('Open',on_open_clip,always_enabled=True),
            Button('Save',on_save_clip),
            [
                Button('Save as',on_saveas_clip),
                Entry('saveas_name_clip',14,'Name:'),
            ],
            ),
        ImageButton(res.get_res('playleft'),lambda: on_play('playleft'),16),
        ImageButton(res.get_res('playstop'),lambda: on_play('playstop'),16),
        ImageButton(res.get_res('playright'),lambda: on_play('playright'),16),
        Button('GOTO',goto_clip),
        NumEntry('ixgoto_clip',6),
        Label('of 0',id='goto_label_clip'),
        Button('<',goto_prv_clip),
        Button('>',goto_nxt_clip),

        DropdownMenu('dd_ops_clips','Operations','aux',None,
        [Button('Cut',on_cut_clip),
            Range('clips_range',6,_format='%d'),
        ],
        [Button('Export frame',on_frame_clip),
            Entry('frame_name_clip',14,'Name:'),
        ],
        Button('Revert',load_clip),
        ),

    ]]

def get_ui():
    return [
        [ Button('Exit Clips Editor',on_exit,always_enabled=True) ],
        [Hr()],
        [Label("Merge Clips:")],
        [Button('Add',on_add_clip),
            Button('Remove',on_remove_clip),
            Button('Move',on_move_clip),
            Radiobutton('move_clip_dir','up','up'),
            Radiobutton('move_clip_dir','down','down'),
        ],
        [ListView('clips_listview','No Files Added',
            onselchange=onselchange)],
        [
            Button('Merge to:',on_mergeto_mp4),
            Entry('mergeto_mp4',12)
        ],

        [Hr()],
        Label('',id='clips_status_msg'),
        ]

