# Copyright Al Cramer 2021 ac2.71828@gmail.com

# The UI is a collection of view-controllers, implemented by the
# modules uiXXX.py (there's one for the banner, one for the top control,
# one for each of the tab panes, etc.). This module gives functions,
# defined in __main__, needed by the uiXXX modules.

do_commit = None
validate_view = None
show_tab = None
show_view = None
get_cur_view_id = None
get_cur_tab_id = None

