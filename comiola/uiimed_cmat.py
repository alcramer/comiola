# Copyright Al Cramer 2021 ac2.71828@gmail.com
from PIL import ImageTk,Image,ImageDraw,ImageOps
from tkalt import *
import scripts
import images
import resources as res
import uiimgedit as imed

# This module implements the "Create Cell Matrix" functions for the
# Image Editor.  A cell matrix is just a set of bars drawn on top of an 
# empty image: useful if you need to draw a set of aligned cells.
class CellMatrix():
    def __init__(self,nrows,ncols,wmat,hmat,pad,fnspr=''):
        # (minimum) width of gutter
        WG = 30
        # thickness's for horizontal and vertical gutters
        hgut = WG
        vgut = WG
        # "box" is region occupied by cell, plus its left and top
        # gutter. If top-left corner of box is at (xbox,ybox),
        # top-left corner of cell is (xbox + vgut, ybox + hgut)
        
        # Compute w and h of the boxes
        if fnspr == '':
            # no image -> no need to worry about aspect ratio of
            # cells. Relations are:
            # ncols * wbox + vgut = wmat
            # nrows * hbox + hgut = hmat
            wbox = int( (wmat - vgut)/ncols )
            hbox = int( (hmat - hgut)/nrows )
        else:
            # want aspect ratio of cells to match aspect ratio of
            # image. Linear relations are:
            # dim_mat = N*dim_box + dim_gut
            # dim_box = dim_gut + dim_cell
            # dim_cell = (1 + pad)*dim_spr*scale.
            # These apply to both vertical and horizonal dimensions.
            #
            # First get scale factor for image. Compute for both 
            # vertical and horizontal cases, and choose the smaller.

            (wspr,hspr) = res.get_img(fnspr).size

            def get_scale(mat,gut,N,pad,spr):
                cell = (mat-gut)/N - gut
                return (
                    cell/((1+pad)*spr)
                    )
            hor_scale = get_scale(wmat,vgut,ncols,pad,wspr)
            vert_scale = get_scale(hmat,hgut,nrows,pad,hspr)
            scale = min(hor_scale,vert_scale)

            # Now that we've got the scale, compute hor and vert gutters
            def get_gut(mat,N,pad,spr,scale):
                cell = (1+pad)*spr*scale
                return (
                    (mat - N*cell)/(N+1)
                    )

            vgut = get_gut(wmat,ncols,pad,wspr,scale)
            hgut = get_gut(hmat,nrows,pad,hspr,scale)

            # get box dim's
            wbox = int( (wmat - vgut)/ncols )
            hbox = int( (hmat - hgut)/nrows )

            # get dim's for sprite image
            scale = scale/(1+pad)
            (self.wspr,self.hspr) = ( int(wspr*scale), int(hspr*scale) )

        # create "cells": list of rect's to be drawn in white
        self.cells = []
        for ixrow in range(0,nrows):
            for ixcol in range(0,ncols):
                ybox = ixrow * hbox
                xbox = ixcol * wbox
                # get coords for cell
                (x0,y0) = ( int(xbox+vgut), int(ybox+hgut) )
                (x1,y1) = ( int(xbox+wbox), int(ybox+hbox) )
                self.cells.append((
                    int(xbox+vgut), int(ybox+hgut),
                    int(xbox+wbox), int(ybox+hbox) ))

    def draw(self,draw_pil):
        for c in self.cells:
            draw_pil.rectangle(c,fill='#ffffff')

def validate_view():
    ui.cmat_w.set_default(2000)
    ui.cmat_h.set_default(1200)
    ui.cmat_rows.set_default(2)
    ui.cmat_cols.set_default(3)
    ui.cmat_pad_spr.set_default(.1)
    if imed.img_pil is None:
        disable_widget( on_makecells,on_cmat_cancel)
    else:
        enable_widget( on_makecells,on_cmat_cancel)


def restore_cmat_dd():
    ui.dd_file_imgedit.set_visible(True)
    ui.dd_cell_matrix.set_visible(True)

def on_cmat_spr():
    fp = askopenfilename(filetypes=
            [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG')])
    if fp != '':
        (head,root,ext) = res.parse_fp(fp)
        ui.cmat_fnspr.set(root)
        # install as needed
        res.install_image(fp)
    restore_cmat_dd()

def on_cmat_create():
    global untitled_enum, img_pil_start,cmat, sel_prop
    w = ui.cmat_w.get('Width')
    if w is None:
        restore_cmat_dd()
        return
    h = ui.cmat_h.get('Height')
    if h is None:
        restore_cmat_dd()
        return
    n_rows = ui.cmat_rows.get('Rows')
    if n_rows is None:
        restore_cmat_dd()
        return
    n_cols = ui.cmat_cols.get('Cols')
    if n_cols is None:
        restore_cmat_dd()
        return

    if imed.untitled_enum == 0:
        fn = "Untitled"
    else:
        # Note: imed.untitled_enum bumps up when save file
        fn = "Untitled%d" % imed.untitled_enum

    # sprite may or may not be specified
    pad = 0
    fnspr= ui.cmat_fnspr.get().strip()
    if fnspr != '':
        pad = ui.cmat_pad_spr.get()
        if pad is None:
            restore_cmat_dd()
            return

    img = Image.new("RGBA", (w,h),'#000000')
    cmat = CellMatrix(n_rows,n_cols,w,h,pad,fnspr)
    draw_pil = ImageDraw.Draw(img)
    cmat.draw(draw_pil)

    imed.start_session(fn,img)

    # add sprites as needed
    if fnspr == '':
        return
    # get w and h for the sprite: scale was computed when cmat was
    # constructed.
    (w,h) = (cmat.wspr,cmat.hspr)
    img_spr = res.get_img(fnspr)
    for c in cmat.cells:
        (x0,y0,x1,y1) = c
        (x,y) = ( (x0+x1)/2, (y0+y1)/2 )
        imed.session.add_prop(img_spr,(x,y,w,h))
    imed.session.set_sel_prop(None)

    imed.validate_view()

def on_makecells():
    fnroot = ui.fn_makecells.get().strip()
    if fnroot == '':
        ui.dd_file_imgedit.set_visible(True)
        ui.dd_makecells.set_visible(True)
        showerror( "Missing file name")
        return
    imed.session.merge_all_props(imed.img_pil)
    fnames = images.write_cells(imed.img_pil, fnroot, scripts.proj_dir, 600)
    if len(fnames) == 0:
        showinfo('No cells found')
    elif len(fnames) == 1:
        showinfo('Created 1 file: %s' % fnames[0])
    else:
        showinfo('Created %d files: %s' % (len(fnames),
            ','.join(fnames)))
    imed.session.modified = False
    imed.validate_view()

def on_cmat_cancel():
    ui.dd_cell_matrix.set_visible(False)
    ui.dd_makecells.set_visible(False)

def get_ui():

    return (
            Label('Cells:'),
            DropdownMenu('dd_cell_matrix','Create Matrix','aux',None,
                [
                    NumEntry('cmat_rows',3,'%d','Rows:'),
                    NumEntry('cmat_cols',3,'%d','Cols:'),
                ],
                [
                    NumEntry('cmat_w',6,'%d','Width:'),
                    NumEntry('cmat_h',6,'%d','Height:'),
                ],
                [
                    Button('Add Sprite', on_cmat_spr,retain_dd=True),
                    Entry('cmat_fnspr',12),
                    RangeParamControl('cmat_pad_spr','Padding:',hb=2.0)
                ],
                Hr(),
                [
                    Button('Create', on_cmat_create),
                    Button('Cancel', on_cmat_cancel,
                        always_enabled=True,retain_dd=True)
                ]
                ),
            DropdownMenu('dd_makecells','Extract Cells',None,None,
                    Entry('fn_makecells',18,'File Name:'),
                    [
                        Button('Extract', on_makecells),
                        Button('Cancel', on_cmat_cancel,
                            always_enabled=True,retain_dd=True),
                    ]
                )
            )
