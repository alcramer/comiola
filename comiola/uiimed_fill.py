# Copyright Al Cramer 2022 ac2.71828@gmail.com
import scripts
from tkalt import *

import uiglobals
import uiimgedit as imed
import resources as res
import polygon

from imgedit import color_region, texture_region, visit_image

def validate_view():
    global fp_fill
    if fp_fill is None:
        fp_fill = res.get_fpart('fill.night.png')
    (head,root,ext) = res.parse_fp(fp_fill)
    ui.texture.set_default(root)
    ui.opacity.set_default(1.0)
    ui.tolerance.set_default(.1)

# fill texture file path
fp_fill = None

def on_texture_reset():
    global fp_fill
    fn = askopenfilename(
            [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG')])
    if fn == '':
        return
    fp_fill = fn
    (head,root,ext) = res.parse_fp(fp_fill)
    ui.texture.set(root)
    
def erase(replace_with,poly_pts):
    # Visit all pts in image (or portion contained on poly, if defined),
    # replacing every pixel with replace_with.
    img_pil = imed.img_pil
    if img_pil is None:
        return
    pix = img_pil.load()

    def onvisit(x,y):
        pix[x,y] = replace_with

    if poly_pts is not None:
        polygon.visit_poly(poly_pts,onvisit)
    else:
        visit_image(img_pil,onvisit)

def replace_color(targXY, SAME_COLOR_PARAM, replace_with,poly_pts):
    # Visit all pts in image (or portion contained in poly, if defined),
    # replacing all pixels that
    # match the one at targXY with "replace_with"
    img_pil = imed.img_pil
    if img_pil is None:
        return
    pix = img_pil.load()
    targ = pix[targXY[0], targXY[1]]

    def onvisit(x,y):
        v = pix[x,y]
        # Note: don't recolor transparent pixels
        if (abs(v[0]-targ[0]) < SAME_COLOR_PARAM and
            abs(v[1]-targ[1]) < SAME_COLOR_PARAM and
            abs(v[2]-targ[2]) < SAME_COLOR_PARAM and
            abs(v[3]) > 127):
            pix[x,y] = replace_with


    if poly_pts is not None:
        polygon.visit_poly(poly_pts,onvisit)
    else:
        visit_image(img_pil,onvisit)

def do_fill(x,y):

    # fill may be constrained by a polygon: get the vertices.
    poly_pts = None
    pcntrl = imed.polyCntrl
    if pcntrl is not None and pcntrl.is_inside(x,y):
        poly_pts = pcntrl.get_pts()

    # procede as per fill-mode
    mode =  ui.fillmode.get() 
    if mode in ['color','reset color','erase']:
        if not imed.set_cur_color():
            return
    tolerance = get_param(ui.tolerance)
    if tolerance is None:
        return
    # tolerance is displayed in range 0 .. 1.0; we want
    # 1-byte value 0 .. 255
    tolerance = int(tolerance*255)
    color_bytes = imed.color_bytes
    color = (color_bytes[0],color_bytes[1],color_bytes[2],255)
    img_pil = imed.img_pil

    if mode == 'color':
        color_region(img_pil,(x,y), tolerance, color, poly_pts)
    elif mode == 'transparency':
        color = (255,255,255,0)
        color_region(img_pil,(x,y), tolerance, color, poly_pts)
    elif mode == 'texture':
        im = Image.open(fp_fill)
        sc = ui.imgedsc.get('Scale')
        if sc is None:
            return
        opacity = get_param(ui.opacity)
        if opacity is None:
            return
        rot = float(ui.imgedrot.get())
        if rot != 0:
            im = im.rotate(rot,expand=True)
        texture_region(img_pil, (x,y),
                tolerance, sc, opacity, im, poly_pts=poly_pts)
    elif mode == 'replace color':
        replace_color((x,y), tolerance, color, poly_pts)
    elif mode == 'erase':
        erase(color,poly_pts)
    validate_view()
    modified = True

def reset_params(param,sign):
    v = param.get()
    if v is None:
        # error was reported
        return
    if sign == '+':
        v += .05
    else:
        v -= .05
        v = max(v,0)
    v = min(v,1.0)
    param.set(v)

def get_param(uiel):
    v = uiel.get()
    if v is None:
        # error reported
        return None
    if v < 0:
        v = 0
        uiel.set(v)
    elif v > 1.0:
        v = 1.0
        uiel.set(v)
    return v

def get_ui():
    return [
        OptionMenu('fillmode',
            ['color','texture','transparency','replace color','erase'],
            'Fill:'),
        ParamControl('tolerance',reset_params,
            4,'%.2f','Tolerance:'),
        ParamControl('opacity',reset_params,
            4,'%.2f','Opacity'),
        [
            Label('Texture:'),
            Button('Reset',on_texture_reset),
            Entry('texture',16)
        ],
        [
            NumEntry('imgedsc',6,_format='%.2f',label='Scale:'),
            Label('Rot.:'),
            OptionMenu('imgedrot',
                ['0','90','180','270'])
        ]
    ]

