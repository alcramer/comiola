# Copyright Al Cramer 2022 ac2.71828@gmail.com
import scripts
from tkalt import *

import images
import uiglobals
import uiimgedit as imed
import resources as res

# src image
img_src = None
def set_src(img):
    global img_src
    img_src = img

# lighting parameters
params = images.get_default_lighting()

def validate_view():
    if imed.tool != 'lighting':
        disable_widget(on_light_apply,on_light_clear,
            ui.imed_light_kind, ui.imed_light_brightness,
            ui.imed_light_floor, ui.imed_light_radius,
            ui.imed_light_color
            )
    else:
        enable_widget(on_light_apply,on_light_clear,
            ui.imed_light_kind, ui.imed_light_brightness,
            ui.imed_light_floor, ui.imed_light_radius,
            ui.imed_light_color
            )
        ui.imed_light_kind.set(params['kind'])
        ui.imed_light_brightness.set(params['brightness'])
        ui.imed_light_floor.set(params['floor'])
        ui.imed_light_radius.set(params['radius'])
        ui.imed_light_color.set(params['color'])

def on_light_apply():
    # update params
    kind,bright,floor,radius,color = (
            ui.imed_light_kind.get(),
            ui.imed_light_brightness.get(),
            ui.imed_light_floor.get(),
            ui.imed_light_radius.get(),
            ui.imed_light_color.get('Color')
            )
    if bright is None or floor is None or radius is None:
        return
    params['kind'] = kind
    params['brightness'] = bright
    params['floor'] = floor
    params['radius'] = radius
    params['color'] = color

    # paste mask on to image src
    (w_src,h_src) = img_src.size
    img = img_src.copy()
    mask = res.get_lighting_mask(w_src,h_src,params)
    img.paste(mask,(0,0,w_src,h_src),mask=mask)
    #img = img.convert('RGB').convert('RGBA')
    img.putalpha(255)
    imed.img_pil = img
    imed.session.modified = True
    imed.vc.draw()

def on_light_clear():
    imed.img_pil = img_src.copy()
    imed.session.modified = False
    imed.vc.draw()

def get_ui():
    return [
        Label('Lighting'),
        [
        Radiobutton('imed_light_kind','ambient','ambient', onchange=on_light_apply),
        Radiobutton('imed_light_kind','spotlight','spotlight')
        ],
        [RangeParamControl('imed_light_brightness','Brightness', 
            onchange=on_light_apply) ],
        [RangeParamControl('imed_light_radius','Radius', 
            hb=None, onchange=on_light_apply) ],
        [RangeParamControl('imed_light_floor','Floor', 
            onchange=on_light_apply) ],
        ColorControl('imed_light_color'),
        [Button('Apply',on_light_apply),Button('Clear',on_light_clear)]
    ]

