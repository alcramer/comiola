# Copyright Al Cramer 2022 ac2.71828@gmail.com
from imgvc import ImgViewCntrl
from PIL import Image
import controls

# Session abstraction for image editing.  A session starts when we 
# open a file for  editing, or create an empty new image.

# Props: a kind of sprite, that can be moved/resizes/transformed,
# then pasted down onto the image. Initially the controller is a
# SprPtCntrl; use 'self.set_cntrl(which)' to reset to PerspCntrl
# (and back).
class Prop:
    def __init__(self,vc,x,y,w,h,img_src):
        self.vc = vc
        self.img_src = img_src
        # this point gives position/size
        self.pt = controls.Pt(x,y, 0.0, 0.0, 0.0, w,h)
        self.cntrl = controls.SprPtCntrl(vc,None,self.pt)
        self.cntrl.prop = self

    def set_cntrl(self,which):
        if which == 'spr':
            self.cntrl.set_mode('spr')
        else:
            self.cntrl.set_mode('persp')

    def persp_is_visible(self):
        return self.cntrl.mode == 'persp'

    def draw(self,is_selected):
        (x,y,w,h) = (self.pt.x, self.pt.y, self.pt.w, self.pt.h)
        img = self.img_src.resize((int(w),int(h)),Image.ANTIALIAS)
        self.vc.paste_img(img,x,y)
        img.close()
        # draw control
        self.cntrl.draw('red',is_selected)

    def merge(self,img_pil):
        (x,y,w,h) = (self.pt.x, self.pt.y, self.pt.w, self.pt.h)
        img = self.img_src.resize((int(w),int(h)),Image.ANTIALIAS)
        (x0,y0) = ( int(x-w/2), int(y-h/2) )
        img_pil.paste(img,(x0,y0),mask=img)
        img.close()


LEN_UNDO_BUF = 6

class Session:

    def __init__(self,fn,vc,img):
        self.fn = fn
        self.vc = vc
        self.img_revert = img.copy()
        self.modified = False
        # props collection
        self.props = []
        self.sel_prop = None
        # circular buffer for undo
        self.undo_buf = [None] * LEN_UNDO_BUF
        self.ix_undo = 0

    def set_sel_prop(self,p):
        self.sel_prop = p

    def add_prop(self,img,xywh=None):
        if xywh is not None:
            (x,y,w,h) = xywh
        else:
            # create initial point.  position is upper left corner
            # Must compute w,h attributes
            dim = 300
            (w,h) = img.size
            ar = float(h)/w
            if ar <= 1.0:
                w = dim
                h = int(.5 + ar*w)
            else:
                h = dim
                w = int(.5 + h/ar)
            x = 20+w/2
            y = 20+h/2
        if self.sel_prop is not None:
            # this is about to be de-selected: set mode to 'spr'
            self.sel_prop.set_cntrl('spr')
        self.sel_prop = Prop(self.vc, x, y, w, h, img)
        self.props.append(self.sel_prop)

    def clone_sel_prop(self):
        # Note: the clone becomes selected
        if self.sel_prop is not None:
            img = self.sel_prop.img_src.copy()
            self.add_prop(img)
            self.sel_prop.pt.x += 50
            self.vc.draw()

    def delete_prop(self,p):
        if p == self.sel_prop:
            self.sel_prop = None
        self.vc.remove_cntrl(p.cntrl)
        self.props.remove(p)

    def delete_all_props(self):
        for p in self.props:
            self.vc.remove_cntrl(p.cntrl)
        self.sel_prop = None
        self.props = []

    def merge_prop(self,img_pil,p):
        p.merge(img_pil)
        self.delete_prop(p)
        self.modified = True

    def merge_all_props(self,img_pil):
        lst = self.props[:]
        for p in lst:
            p.merge(img_pil)
            self.delete_prop(p)
        self.sel_prop = None

    def draw_props(self):
        for p in self.props:
            p.draw(p==self.sel_prop)

    # Undo rec's. There are 2 kinds: "undo_img" and "undo_prop".

    def push_undo(self,rec):
        ix = self.ix_undo % LEN_UNDO_BUF
        self.ix_undo += 1
        self.undo_buf[ix] = rec

    def pop_undo(self):
        if self.ix_undo == 0:
            return None
        self.ix_undo -= 1
        ix = self.ix_undo % LEN_UNDO_BUF
        return self.undo_buf[ix]

