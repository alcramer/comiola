# Copyright Al Cramer 2021 ac2.71828@gmail.com
from PIL import ImageTk,Image,ImageDraw,ImageOps
import os

import scripts
import resources as res
from resources import get_res
from tkalt import *
from colors import *
import controls
from imgvc import ImgViewCntrl

from imgedit import get_bb_content, create_cell_matrix 

import uiglobals
import display
import xfrmpersp
import images
import polygon

import uiimed_session
import uiimed_cmat
import uiimed_light
import uiimed_fill

# UI for the image editor. Some functionality is implemented here, and
# some is done by subordinate modules: these all start with "uiimed_"

# Our drawing canvas
can = None

# selection tool (a moveable rectangle)
selCntrl = None

# polygon tool
polyCntrl = None

# line segment cntrl
linesegCntrl = None

# lighting View-Controller
lightCntrl = None

# currently selected tool
tool = ''

# image buttons for toolbar
toolbuttons = None

# the current session. A session starts when we 
# open a file for  editing, or create an empty new image.
# "img_pil" is the image we're editing; "draw_pil" it's draw object
# "vc" is our view-controller.
# "session" encapsulates session data & functions (do/undo records,
# and sprite management).
vc = None
img_pil = None
draw_pil = None
session = None
def start_session(fn,img):
    global vc, img_pil, draw_pil, session
    global selCtrl, polyCntrl, linesegCntrl, lightCntrl, tool

    img_pil = img
    draw_pil = ImageDraw.Draw(img_pil)
    (w,h) = img.size
    vc = ImgViewCntrl(can,w,h,draw_content,
            bg_color=(0x75,0x75,0x75),
            on_h_selected=on_h_selected,
            MAXDIM_VIEW= res.MAXDIM_DISPLAY)
    session = uiimed_session.Session(fn,vc,img)

    (selCtrl, polyCntrl, linesegCntrl, lightCntrl) = (None,None,None,None)
    tool = ''
    validate_view()

# Undo mechanism.
# Undo rec for main image
class  UndoRec():
    def __init__(self):
        self.img = img_pil.copy()
        self.scale = vc.scale_im_to_can
        self.xoff = vc.xoff
        self.yoff = vc.yoff

# Undo for props
class PropUndoRec:
    def __init__(self,prop):
        self.prop = prop
        self.img = prop.img_src.copy()
        self.wh = (prop.pt.w,prop.pt.h)

def push_undo():
    session.push_undo(UndoRec())

def push_undo_prop(prop):
    session.push_undo(PropUndoRec(prop))

def do_undo():
    global img_pil
    if session is None:
        return
    rec = session.pop_undo()
    if rec is None:
        return
    if isinstance(rec,PropUndoRec):
        prop = rec.prop
        (prop.pt.w,prop.pt.h) = rec.wh
        prop.img_src = rec.img
        prop.set_cntrl('spr')
    else:
        img_pil = rec.img
        (w,h) = img_pil.size
        vc.reinit_scale(w,h)
    vc.draw()

# current fill color: [r,g,b]; and current hexcode ("#ff0000") 
color_bytes = None
color_hexcode = None
def set_cur_color():
    global color_bytes, color_hexcode
    color_hexcode = ui.sel_color.get('Color')
    if color_hexcode is None:
        return False
    #color_bytes = bytes.fromhex(color_hexcode[1:])
    tmp = color_hexcode.lstrip('#')
    lv = len(tmp)
    color_bytes = tuple(
            int(tmp[i:i + lv // 3], 16) for i in range(0, lv, lv // 3))
    return True

# enumerator for "Untitled", "Untitled1", "Untitled2", etc. 
untitled_enum = 0

# Initialization: called when we bring up the image editor
def init():
    global tool, selCntrl, polyCntrl, linesegCntrl

    # reset globals to null values
    tool = ''
    selCntrl = None
    polyCntrl = None
    linesegCntrl = None
    can.delete('all')
    validate_view()

def draw_content():
    if session is not None:
        # paste img_pil onto a gray-grid background, so transperency
        # is visible.
        img = Image.new("RGBA",img_pil.size,'#ffffff')
        grid_pil = res.get_res('graygrid')
        (w_grid,h_grid) = grid_pil.size
        (w_img,h_img) = img.size
        ncols = 1 + int(w_img/w_grid)
        nrows = 1 + int(h_img/w_grid)
        y = 0
        for ixrow in range(0,nrows):
            x = 0
            for ixcol in range(0,ncols):
                img.paste(grid_pil,(x,y),mask=grid_pil)
                x += w_grid
            y += h_grid
        img.paste(img_pil,(0,0),mask=img_pil)
        # paste to vc image
        vc.paste_img(img,int(w_img/2), int(h_img/2))
    # draw selection rect if defined
    if selCntrl is not None:
        selCntrl.draw('yellow')
    if polyCntrl is not None:
        polyCntrl.draw('yellow')
    if linesegCntrl is not None:
        linesegCntrl.draw( 
                color_hexcode, int(ui.brushsize.get()))
    session.draw_props()
    if lightCntrl is not None:
        lightCntrl.draw('yellow')
    return True

def validate_view():
    if not scripts.script_open():
        return
    ui.sel_color.set_default('#000000')
    ui.fnimg.set('')
    if session is not None:
        (w,h) = img_pil.size
        ui.fnimg.set("%s   %dx%d" % (session.fn,w,h))
    ui.imgedsc.set_default(1.0)
    ui.w_new_img.set_default(800)
    ui.h_new_img.set_default(600)
    ui.ptool_show.set('hide')
    ui.fnimg.set_enabled(False)
    enable_widget(ui.dd_file_imgedit)
    enable_widget(ui.dd_operations)
    disable_widget(on_save,on_saveas,ui.rb_save_what,
                on_add_prop,on_delete_prop,on_merge_prop,
                on_flip_prop,on_clone_prop,
                on_trim,on_flip,
                ui.rb_merge_what,
                on_ptool_apply,ui.ptool_show)
    if session is not None:
        enable_widget(on_save, on_saveas, ui.rb_save_what, 
                on_trim,on_flip,
                on_add_prop)
        ui.rb_save_what.set('image')
        if len(session.props) > 0:
            enable_widget(ui.rb_merge_what,on_merge_prop)
        sel_prop = session.sel_prop
        if sel_prop is not None:
            enable_widget(on_delete_prop,
                on_flip_prop,on_clone_prop,
                on_ptool_apply,ui.ptool_show)
            if sel_prop.persp_is_visible():
                ui.ptool_show.set('show')

    # validate auxillary modules
    uiimed_cmat.validate_view()
    uiimed_light.validate_view()
    uiimed_fill.validate_view()

    # validate tool bar buttons
    for b in toolbuttons:
        color = None
        if b.name == 'scissors':
            # never shown selected
            pass
        elif b.name == 'select':
            if selCntrl is not None:
                color = '#ffffff'
        elif b.name == 'poly':
            if polyCntrl is not None:
                color = '#ffffff'
        else:
            if b.name == tool:
                color = '#ffffff'
        b.set_bg(color)
    if session is not None:
        vc.draw()

def ask_and_save_changes():
    if session is None:
        return True
    if session.modified:
        action  = askyesnocancel('Save changes to "%s"?' % session.fn)
        if action is None:
            # user choose "Cancel"
            return False
        if action:
            session.merge_all_props(img_pil)
            save_file(session.fn)
    return True

def on_open():
    fp = askopenfilename(filetypes=
            [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG')])
    if fp != '':
        if not ask_and_save_changes():
            return
        fn = res.install_image(fp)
        img = Image.open(
            '%s/%s.png' % (scripts.proj_dir,fn)).convert('RGBA')
        start_session(fn,img)
        validate_view()

def on_new_img():
    w = ui.w_new_img.get('Width')
    if w is None:
        return
    h = ui.h_new_img.get('Height')
    if h is None:
        return
    if not ask_and_save_changes():
        return
    if untitled_enum == 0:
        fn = "Untitled"
    else:
        # Note: untitled_enum bumps up when save file
        fn = "Untitled%d" % untitled_enum
    start_session(fn,
        Image.new("RGBA",(w,h),'#ffffff'))
    validate_view()

def validate_pts(ar):
    # helper for "on_save": reset "w" attribute for pts when
    # aspect ratio of image changes (height remains the same).
    for shot in scripts.script.shots:
        for ani in shot.anis:
            if ani.kind != 'spr':
                continue
            for pt in ani.path:
                if ani.get_fn_pt(pt) == fn:
                    pt.w = pt.h/ar

def save_file(fnsave):
    global untitled_enum
    # save imp_pil (or selection) in project dir, using file name "fnsave"
    # Caller must ensure that "fnsave" is defined.
    save_selection = False
    if ui.rb_save_what.get() == 'selection':
        save_selection = True
        if selCntrl is None:
            showerror( "No region selected")
            return
    session.merge_all_props(img_pil)
    if save_selection:
        img_pil.crop(selCntrl.get()).save(
            '%s/%s.png' % (scripts.proj_dir,fnsave))
    else:
        img_pil.save('%s/%s.png' % (scripts.proj_dir,fnsave))
        # this behavior follows that of paint.net
        session.fn = fnsave
    # if aspect ratio changed, we correct all pts that use this
    # image: height remains the same, but width is altered.
    ar_start = res.get_img_ar(fnsave)
    (w,h) = img_pil.size
    ar = float(h)/w
    if ar != ar_start:
        validate_pts(ar)
    res.clear_cache()
    session.modified = False
    # bump up untitled_enum as needed
    if fnsave == 'Untitled' or fnsave == 'Untitled%d' % untitled_enum:
        untitled_enum += 1
    validate_view()

def on_save():
    save_file(session.fn)

def on_saveas():
    fnsave = ui.saveas_img.get()
    if fnsave == '':
        showerror( "Missing file name")
        return
    save_file(fnsave)

def on_exit():
    global session
    if session is not None and session.modified:
        action  = askyesnocancel('Save changes to "%s"?' % session.fn)
        if action is None:
            # user choose "Cancel"
            return 
        if action:
            save_file(session.fn)
    # when we reenter the image editor, we start a new session
    session = None
    res.clear_cache()
    uiglobals.show_view('main')

def on_tool(which):
    # Called when user presses a tool button. which == '' means:
    # deselect any currently selected tool.
    #
    # The implementation may seem adhoc, but how we respond 
    # to users here is crucial to providing a good work flow. So
    # this function is trying to solve a difficult UX problem.
    global img_pil, tool, selCntrl, polyCntrl, lightCntrl
    if session is None:
        return
    if which == 'scissors':
        if selCntrl is not None:
            push_undo()
            img_pil = img_pil.crop(selCntrl.get())
            (w,h) = img_pil.size
            vc.reinit_scale(w,h)
            selCntrl = None
            vc.draw()
        tool = ''
    elif which == 'select':
        # toggle select control
        if selCntrl is None:
            rect = get_bb_content(img_pil,
                    int(ui.tolerance.get()), margin=0)
            selCntrl = controls.RectCntrl(vc,*rect)
            rect = selCntrl.get()
        else:
            # toggle off
            selCntrl = None
    elif which == 'poly':
        # toggle poly control
        if tool == 'poly':
            tool = ''
            polyCntrl = None
        else:
            tool = which
            polyCntrl = controls.PolyCntrl(vc)
    elif which == '':
        # this means: de-select any selected tool
        tool = ''
    else:
        # succesive clicks toggle the tool
        if tool == which:
            tool = ''
        else:
            tool = which
    # Some tools require that the color selection be valid
    if tool in ['brush','bucket','lineseg']:
        if not set_cur_color():
            # error was reported
            tool = '' 
    # polyCntrl is off, unless tool is bucket or poly
    if not tool in ['bucket','poly']:
        polyCntrl = None
    # register/unregister custom handlers
    if tool in ['brush','bucket','dropper','lineseg','poly']:
        register_custom_handlers()
    else:
        unregister_custom_handlers()
    # lineseg Cntrl is made None: it's instantiated on first
    # mouse click. 
    linesegCntrl = None
    # final adjust of controls
    if tool != '':
        if tool != 'scissors':
            selCntrl = None
    if tool == 'lighting':
        lightCntrl = controls.LightCntrl(vc,16,uiimed_light.params,
                on_drag_end=uiimed_light.on_light_apply)
        uiimed_light.set_src(img_pil)
    else:
        if lightCntrl is not None:
            vc.remove_cntrl(lightCntrl)
        lightCntrl = None
    validate_view()

def on_h_selected(h):
    if h is not None and h.pt is not None:
        session.set_sel_prop(h.cntrl.prop)
    validate_view()

def on_add_prop():
    global sel_prop 
    fp = askopenfilename( initialdir=scripts.proj_dir,
            filetypes=
            [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG')])

    if len(fp) == 0:
        return
    # write image to project dir as needed and add the prop
    fn = res.install_image(fp)
    session.add_prop(res.get_img(fn))
    # unselecting tool gives better work flow: function will call
    # validate_view
    on_tool('')

def on_delete_prop():
    if session is not None:
        if ui.rb_merge_what.get() == 'all':
            session.delete_all_props()
        elif session.sel_prop is not None:
            session.delete_prop(session.sel_prop)
        validate_view()

def on_merge_prop():
    if session is None or len(session.props) == 0:
        return
    if ui.rb_merge_what.get() == 'all':
        push_undo()
        session.merge_all_props(img_pil)
    elif session.sel_prop is not None:
        push_undo()
        session.merge_prop(img_pil,session.sel_prop)
    validate_view()

def on_flip_prop():
    if session is not None:
        p = session.sel_prop
        if p is not None:
            p.img_src = ImageOps.mirror(p.img_src)
            vc.draw()

def on_clone_prop():
    if session is not None:
        session.clone_sel_prop()

def on_ptool_show_change():
    if session is not None:
        p = session.sel_prop
        if p is not None:
            # ptool applies to current prop
            if ui.ptool_show.get() == 'show':
                p.set_cntrl('persp')
            else:
                p.set_cntrl('spr')
            vc.draw()

def on_ptool_apply():
    if session is None:
        return
    p = session.sel_prop
    if p is None or not p.persp_is_visible():
        return
    push_undo_prop(p)
    (wparam,hparam,fall,dim) = p.cntrl.get_persp_params()
    p.img_src = xfrmpersp.xfrm(
            p.img_src,wparam,hparam,fall,dim)
    # "dim" is the height for the point. The transform changes 
    # aspect ratio: adjust p.w accordingly.
    pt = p.pt
    pt.h = dim
    (w,h) = p.img_src.size
    ar = float(h)/w
    pt.w = pt.h/ar
    p.set_cntrl('spr')
    validate_view()

def on_trim():
    global img_pil
    push_undo()
    gray = img_pil.convert('L')
    bb = images.get_bb_content(*gray.size,gray.load())
    img_pil = img_pil.crop(bb) 
    (w,h) = img_pil.size
    vc.reinit_scale(w,h)
    session.modified = True
    vc.draw()

def on_flip():
    global img_pil
    push_undo()
    img_pil = ImageOps.mirror(img_pil)
    session.modified = True
    vc.draw()

# Mouse events. In general we pass them on to the view control for
# handling; but for some tools (brush, dropper, and bucket) we 
# tell the vc to call a custom handler provided by us.
# The custom handlers are passed mouse coords (x,y), where x
# and y are in image coordinates.

# These are the "primary" handlers: they're bound to the canvas by tkinter
def on_mousedown(ev):
    if session is not None:
        vc.on_mousedown(ev)

def on_mouseup(ev):
    if session is not None:
        vc.on_mouseup(ev)

def on_mousemove(ev):
    if session is not None:
        vc.on_mousemove(ev)

def on_doubleclick(ev):
    if session is not None:
        vc.on_doubleclick(ev)

# The functions register/unregister our custom handlers
def register_custom_handlers():
    if session is not None:
        vc.client_mousedown = custom_mousedown
        vc.client_mousemove = custom_mousemove
        vc.client_mouseup = custom_mouseup

def unregister_custom_handlers():
    if session is not None:
        vc.client_mousedown = None
        vc.client_mousemove = None
        vc.client_mouseup = None

xprv = None
yprv = None

def draw_stroke(x,y):
    global xprv, yprv
    # print('stroke. x,y: %d %d xprv,yprv: %d %d width:%d' % (x,y,xprv,yprv,w))
    w_brush = int(ui.brushsize.get())
    if xprv is not None:
        delta = max(abs(x-xprv),abs(y-yprv))
        if delta < 3:
            return
        # draw to canvas
        w = max(int(w_brush*vc.scale_im_to_can),1)
        (_x,_y) = vc.from_im_coords(x,y)
        (_xprv,_yprv) = vc.from_im_coords(xprv,yprv)
        can.create_line(_xprv,_yprv,_x,_y,fill=color_hexcode,width=w)
        can.update()
        # draw to img
        draw_pil.line((xprv,yprv,x,y),
                fill=color_bytes,width=w_brush)
        session.modified = True
    xprv = x
    yprv = y

def custom_mousedown(x,y):
    global  xprv, yprv, linesegCntrl

    # is x,y within the bounds of the image?
    w_img, h_img = (vc.w_img, vc.h_img)
    def in_img(x,y,mar):
        return ( x > -mar  and x < w_img + mar
            and y > -mar and y < h_img +mar)

    if tool in ['brush','bucket','lineseg']:
        # make sure color is current
        if not set_cur_color():
            # error was reported
            return
    if tool == 'brush':
        if not in_img(x,y,16):
            return
        push_undo()
        # setup for "draw_stroke"
        xprv = x
        yprv = y
    elif tool == 'dropper':
        if not in_img(x,y,0):
            return
        try:
            col = img_pil.getpixel((x,y))
            hexcode = '#%02x%02x%02x' % (col[0], col[1], col[2])
            ui.sel_color.set(hexcode)
        except IndexError:
            pass
    elif tool == 'bucket':
        if not in_img(x,y,0):
            return
        push_undo()
        uiimed_fill.do_fill(x,y)
        vc.draw()
        session.modified = True
    elif tool == 'lineseg':
        push_undo()
        linesegCntrl = controls.LineSegCntrl(vc,x,y)
        session.modified = True
    elif tool == 'poly':
        polyCntrl.add_pt(x,y)
        vc.draw()

def custom_mousemove(x,y):
    if tool == 'brush':
        draw_stroke(x,y)
        return True
    if tool == 'lineseg':
        linesegCntrl.set_endpoint(x,y)
        vc.draw()
        return True
    return False

def custom_mouseup(x,y):
    if tool == 'lineseg':
        (x0,y0,x1,y1) = linesegCntrl.get_endpoints()
        draw_pil.line((x0,y0,x1,y1),
                fill=color_bytes,
                width=int(ui.brushsize.get())
                )
        vc.draw()

# '#%02x%02x%02x' % (0, 128, 64)
# red, green, blue = bytes.fromhex("aabbcc")

def on_revert():
    start_session(session.fn,session.img_revert)
    vc.draw()

def get_ui_menu():
    global toolbuttons

    items = [
         DropdownMenu('dd_file_imgedit','File','aux',None,
             Button('Open',on_open,always_enabled=True),
            [
                Button('New',on_new_img),
                NumEntry('w_new_img',5,'%d','Width:'),
                NumEntry('h_new_img',5,'%d','Height:'),
            ],
             Button('Save',on_save),
             Hr(),
            [
                Radiobutton('rb_save_what','image','image'),
                Radiobutton('rb_save_what','selection','selection'),
            ],
            [
                Button('Save as',on_saveas),
                Entry('saveas_img',18),
            ],
            Hr(),
            *uiimed_cmat.get_ui(),
            ),
        Entry('fnimg',22),
        Label('',id='sizeimg')
        ]
       
    toolbuttons = []
    names = ['hand','brush','lineseg','poly','bucket',
            'dropper','select','scissors','lighting']
    for n in names:
        b = ImageButton(get_res(n),lambda n=n: on_tool(n),20)
        b.name = n
        toolbuttons.append(b)
    items.extend(toolbuttons)
    items.extend([
            ColorControl('sel_color'),
            DropdownMenu('dd_props','Sprites',None,None,
                [Button('Add',on_add_prop)],
                [Button('Flip',on_flip_prop)],
                [Button('Clone',on_clone_prop)],
                Hr(),
                [
                    Radiobutton('rb_merge_what','all','all'),
                    Radiobutton('rb_merge_what','selected','selected'),
                ],
                [
                    Button('Merge',on_merge_prop),
                    Button('Delete',on_delete_prop),
                ]
            ),
            DropdownMenu('dd_operations','Operations',None,None,
                [Button('Trim',on_trim)],
                [Button('Flip',on_flip)]
                )

    ])
    return [items]

def get_ui():

    return [
        [
            Button('Exit Image Editor',on_exit,True)
        ],
        Hr(),
        OptionMenu('brushsize',
                ['1','2','4','8','12','16','20', '30','40','60'],
                'Brush Size:'),
        *uiimed_fill.get_ui(),
        [Hr()],
        [Label('Perspective Tool:')],
        [Radiobutton('ptool_show','show','show',
                    onchange=on_ptool_show_change),
                    Radiobutton('ptool_show','hide','hide')],
        [Button('Apply',on_ptool_apply)],
        [Hr()],
        *uiimed_light.get_ui(),
        [Hr()],
        [
            Button('Undo',do_undo,True),
            Button('Revert',on_revert,True)
        ],
    ]
