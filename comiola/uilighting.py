# Copyright Al Cramer 2021 ac2.71828@gmail.com
import scripts
from tkalt import *

import images
import display
import uiglobals
import controls

def validate_view():

    disable_widget(on_light_apply,on_light_clear,
            ui.light_kind, ui.light_brightness,
            ui.light_floor, ui.light_radius,
            ui.light_color,
            ui.light_lock
            )
    if (not scripts.script_open()) or scripts.cnt_shots()== 0:
        return

    if uiglobals.get_cur_tab_id() != 'lighting':
        display.light_cntrl.set_visible(False)
        return

    ix = display.ixshot+1

    display.light_cntrl.set_visible(True)
    enable_widget(on_light_apply,on_light_clear,
            ui.light_kind, ui.light_brightness,
            ui.light_floor, ui.light_radius,
            ui.light_color,
            ui.light_lock
            )
    if display.shot.fxdct.get('lighting') is None:
        ui.light_status.set_text('No lighting has been set')
    else:
        ui.light_status.set_text('Current lighting:')
    params = display.light_cntrl.params
    ui.light_lock.set(params['lock_to_cam'])
    ui.light_kind.set(params['kind'])
    ui.light_brightness.set(params['brightness'])
    ui.light_floor.set(params['floor'])
    ui.light_radius.set(params['radius'])
    ui.light_color.set(params['color'])

def on_light_apply():
    # update model
    params = display.light_cntrl.params
    kind,bright,floor,radius,color = (
            ui.light_kind.get(),
            ui.light_brightness.get(),
            ui.light_floor.get(),
            ui.light_radius.get(),
            ui.light_color.get('Color')
            )
    if (bright is None or floor is None or 
        radius is None or color is None):
        return

    params['kind'] = kind
    params['brightness'] = bright
    params['floor'] = floor
    params['radius'] = radius
    params['color'] = color
    params['lock_to_cam'] = ui.light_lock.get()
    display.shot.fxdct['lighting'] = params.copy()
    display.edit_shot(display.ixshot)

def on_light_clear():
    try:
        del display.shot.fxdct['lighting']
    except:
        pass
    display.edit_shot(display.ixshot)

def get_ui():
    return [
        Label('No lighting has been set',id='light_status'),
        [
        Radiobutton('light_kind','ambient','ambient', onchange=on_light_apply),
        Radiobutton('light_kind','spotlight','spotlight')
        ],
        [RangeParamControl('light_brightness','Brightness', 
            onchange=on_light_apply) ],
        [RangeParamControl('light_radius','Radius', 
            hb=None, onchange=on_light_apply) ],
        [RangeParamControl('light_floor','Floor', 
            onchange=on_light_apply) ],
        ColorControl('light_color'), 
        [Checkbox('light_lock',"Lock to Camera",
                onchange=on_light_apply)],
        Hr(),
        [Button('Apply',on_light_apply),Button('Clear',on_light_clear)]
    ]

