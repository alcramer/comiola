# Copyright Al Cramer 2021 ac2.71828@gmail.com
import scripts
from scripts import Pt
import resources as res
from uishots import delete_sprite
from tkalt import *

import uiglobals
import display

# "tracker": used for tracking shots. This is an ani that is locked
# to another ani (the target). "on_sel_change" is callback function
# called by the display when the selected pt/ani changes.
tracker = None
def on_lock():
    global tracker
    tracker = None
    if display.sel_ani is not None and display.sel_pt is not None:
        tracker = (display.sel_ani,display.sel_pt)

def on_sel_change():
    global tracker
    if tracker is None or display.sel_ani is None:
        tracker = None
        return
    (tracker_ani, tracker_pt) = tracker
    (targ_ani,targ_pt) = (display.sel_ani,display.sel_pt)
    # impose restrictions on target
    if targ_ani.kind != 'cam':
        if (targ_ani.lockto is not None and 
            targ_ani.kind != 'cam'):
            showerror(
'"Lock to" failed: cannot lock to a locked sprite ')
            return
    tracker_ani.lockto = targ_ani
    # trim tracker ani path down to single pt
    tracker_ani.path = [tracker_pt]
    tracker = None
    display.make_cntrls()
    uiglobals.validate_view()

def validate_view():
    ui.cycles.set(None)
    ui.frames_per_cell.set(None)
    ui.reverb.set(None)
    ui.ptZ.set(None)
    ui.rot.set(None)
    ui.cb_jig.set(False)
    ui.jig_amount.set(None)
    ui.jig_speed.set(None)
    ui.cb_smear.set(False)
    but_sprimgs_visible.validate_view()
    but_bg_visible.validate_view()
    but_resize_visible.validate_view()
    but_lighting_visible.validate_view()
    #display.visible['bbpath'] = False
    but_bbpath_visible.validate_view()
    but_bbpath_visible.set_enabled(False)
    disable_widget(ui.menu)
    if scripts.cnt_shots() > 0:
        if display.sel_ani is not None:
            enable_widget( on_hide_path, add_pt, delete_pt, on_lock)
            but_bbpath_visible.set_enabled(True)
            if display.sprite_selected():
                ani = display.sel_ani
                enable_widget(
                    on_dd_params_apply,
                    on_flip,on_rot,clone_sprite,on_setz,on_reset_spr_img,
                    ui.ptZ,ui.rot,ui.cb_jig,ui.cb_smear,
                    ui.jig_amount,ui.jig_speed) 
                ui.ptZ.set(display.sel_pt.z)
                ui.rot.set(display.sel_pt.rot)
                if ani.cnt_cells() > 1 or ani.cnt_pts() > 1:
                    ui.cycles.set(display.shot.get_ani_cycles(ani) )
                    enable_widget(ui.cycles)
                if ani.cnt_cells() > 1:
                    ui.frames_per_cell.set(ani.frames_per_cell)
                    enable_widget(ui.frames_per_cell)
                    ui.reverb.set(ani.reverb)
                    enable_widget(ui.reverb)
                if ani.fxdct.get('jiggle') is not None:
                    ui.cb_jig.set(True)
                    params = ani.fxdct['jiggle']
                    ui.jig_amount.set(params['amount'])
                    ui.jig_speed.set(params['speed'])
                    ui.cb_smear.set(params['smear'])


            but_bbpath_visible.set_enabled(True)
            # Enable "on_unlock" if selected ani is locked
            if display.sel_ani.lockto is not None:
                enable_widget(on_unlock)

def clone_sprite():
    # Clone an animation
    ani = display.sel_ani
    if ani is None or ani.kind in ['cam','txt']:
        return
    # compute initial position: center of image
    cl = ani.clone()
    cl.xlate_path(25,0)
    display.shot.anis.append(cl)
    display.make_cntrls()
    display.set_selected(cl,cl.path[0])
    uiglobals.validate_view()

def on_hide_path():
    if display.sel_ani is not None:
        display.hide_ani(display.sel_ani)

def on_show_all_paths():
    display.show_all_anis()

def add_pt():
    # check if this is static, multi-cell animation before adding the pt
    ani = display.sel_ani
    p = ani.path[-1].clone()
    displace = 50
    if len(ani.path) > 1:
        pprev = ani.path[-2]
        if p.x < pprev.x:
            displace = -50
    p.x += displace
    ani.add_pt(p)
    display.make_cntrls()
    display.set_selected(ani,p)
    uiglobals.validate_view()

def delete_pt():
    ani = display.sel_ani
    pt = display.sel_pt
    if len(ani.path) == 1:
        delete_sprite()
        return
    ani.delete_pt(pt)
    display.set_selected(ani,ani.path[0])
    display.make_cntrls()
    uiglobals.validate_view()

def ask_img_filename():
    return askopenfilename(filetypes=
            [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG')])

def on_reset_spr_img():
    if display.sel_pt is None:
        return
    names = askopenfilenames( initialdir=scripts.proj_dir,
        filetypes=
        [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG')])

    if len(names) == 0:
        return
    if len(names) == 1:
        # apply to selected pt
        scripts.reset_pt_img(display.sel_pt,names[0])
    else:
        # multiple names: cell animation
        roots = []
        for fn in names:
            res.install_image(fn)
            (head,root,ext) = res.parse_fp(fn)
            roots.append(root)
        scripts.reset_ani_cells(display.sel_ani,roots)
    uiglobals.validate_view()

def on_flip():
    display.sel_pt.flip = (display.sel_pt.flip + 1) % 2
    uiglobals.validate_view()

def on_rot():
    v = ui.rot.get('Rotation')
    if v is not None:
        display.sel_pt.rot = v
    uiglobals.validate_view()

def on_setz():
    v = ui.ptZ.get('Z-order')
    if v is not None:
        display.sel_pt.z = v
    uiglobals.validate_view()

def on_unlock():
    display.sel_ani.lockto = None
    uiglobals.validate_view()

def on_dd_params_apply():
    # cycles apply to ani's with more than 1 pt; or more than
    # 1 cell.
    ani = display.sel_ani
    if ani is None:
        return
    if ani.cnt_cells() > 1 or ani.cnt_pts() > 1: 
        vcycles = ui.cycles.get('Cycles')
        if vcycles is None:
            return
        ani.cycles = vcycles
    # frames per cell only applies to multiple-cell ani's
    if ani.cnt_cells() > 1:
        reverb = ui.reverb.get('Fade frames')
        if reverb is None:
            return
        ani.reverb = reverb
        vfpc = ui.frames_per_cell.get('Frames per cell')
        if vfpc is None:
            return
        ani.frames_per_cell = vfpc
        if ani.cnt_pts() == 1: 
            # reset shot length to accord with cycles and fps
            display.shot.sync_secs(ani)
            # ui.shot_secs is defined in uishots.py
            ui.shot_secs.set(display.shot.secs)
    # jiggle applies to all 
    if ui.cb_jig.get():
        vsize = ui.jig_amount.get()
        if vsize is None:
            return
        vspeed = ui.jig_speed.get()
        if vspeed is None:
            return
        vsmear = ui.cb_smear.get()
        ani.fxdct['jiggle'] = {'amount':vsize,
                'speed':vspeed, 'smear':vsmear}
    else:
        # delete effect if present
        if ani.fxdct.get('jiggle') is not None:
            del ani.fxdct['jiggle']

def on_cb_jig():
    if ui.cb_jig.get():
        # turned on: supply defaults
        ui.jig_amount.set(1.0)
        ui.jig_speed.set(1.0)
        ui.cb_smear.set(False)
    else:
        # turned off: clear
        ui.jig_amount.set(None)
        ui.jig_speed.set(None)
        ui.cb_smear.set(False)

# A "Button" object (see widgets.py) that toggles options in the
# display
class DisplayToggle(Button):
    def __init__(self,opt, show_label, hide_label,always_enabled=True):
        self.opt = opt
        self.show_label = show_label
        self.hide_label = hide_label
        super().__init__(hide_label,self.on_click, 
                always_enabled=always_enabled)

    def validate_view(self):
        self.set_text(self.hide_label if display.visible[self.opt]
                else self.show_label)

    def on_click(self):
        display.visible[self.opt] = not display.visible[self.opt]
        #uiglobals.validate_view()
        #display.draw_edit()
        display.edit_shot(display.ixshot)

but_sprimgs_visible = DisplayToggle('sprimgs','Show Sprites','Hide Sprites')
but_bg_visible = DisplayToggle('bg','Show Background','Hide Background')
but_resize_visible = DisplayToggle('resize','Show Size Controls','Hide Size Controls')
but_lighting_visible = DisplayToggle('lighting','Show Lighting','Hide Lighting')
but_bbpath_visible = DisplayToggle('bbpath',
        'Show Path Control','Hide Path Control',
        always_enabled=False)

def get_ui():

    return [[
        DropdownMenu(None,"View",None,None,
            but_sprimgs_visible,
            but_bg_visible,
            but_resize_visible,
            but_lighting_visible,
            Hr(),
            Button('Hide Path',on_hide_path),
            Button('Show All Paths',on_show_all_paths,always_enabled=True),
            but_bbpath_visible,
        ),
        Label('Point:'),
        Button('Add',add_pt),
        Button('Delete',delete_pt),
        DropdownMenu(None,"Sprite",None,None,
            Button('Reset',on_reset_spr_img),
            Button('Flip',on_flip),
            [
                Button('Set Z:',on_setz),
                NumEntry('ptZ',4,_format='%.1f'),
            ],
            [
                Button('Rotate:',on_rot),
                NumEntry('rot',4,_format='%.1f')
            ],
            Button('Clone',clone_sprite),
        ),
        DropdownMenu(None,"Tracking",None,None,
            Button('Lock To',on_lock),
            Button('UnLock',on_unlock)
        ),
        DropdownMenu(None,"Parameters",None,None,
            RangeParamControl('cycles','Cycles',width=6,
                _format='%.2f',lb=0,hb=None,incr=1,
                onchange=on_dd_params_apply),
            RangeParamControl('frames_per_cell','Frames per cell',width=4,
                _format='%d',lb=0,hb=None,incr=1,
                onchange=on_dd_params_apply),
            RangeParamControl('reverb','Fade frames', width=4,
                _format='%d',lb=0,hb=None,incr=1,
                onchange=on_dd_params_apply),
            Hr(),
            Checkbox('cb_jig','Jiggle',onchange=on_cb_jig),
            RangeParamControl('jig_amount','Amount',width=4,
                _format='%.2f',lb=0,hb=None,incr=0.05,
                onchange=on_dd_params_apply),
            RangeParamControl('jig_speed','Speed',width=4,
                _format='%.2f',lb=0,hb=None,incr=0.05,
                onchange=on_dd_params_apply),
            Checkbox('cb_smear','Smear'),
            Hr(),
            Button("Apply",on_dd_params_apply)
        )
    ]]
