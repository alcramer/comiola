# Copyright Al Cramer 2021 ac2.71828@gmail.com
import scripts
from tkalt import *

import display
import uiglobals

# clipboard
clipboard = None

def validate_view():
    disable_widget(ui.sequences)
    if not scripts.script_open() or scripts.cnt_shots() == 0:
        return
    ui.seq_range.set(None)
    ui.add_after_seq.set(None)
    if display.ixshot != -1:
        ix = display.ixshot+1
        ui.seq_range.set((ix,ix))
        ui.add_after_seq.set(ix)
        enable_widget(on_cut,on_copy,ui.seq_range,ui.add_after_seq)
        if clipboard is not None:
            enable_widget(on_paste)
        enable_widget(on_cp_bg, on_cp_lighting)
        ani = display.sel_ani
        if ani is not None and ani.kind == 'spr':
            enable_widget(on_cp_ani)

def get_range():
    _range = ui.seq_range.get('Shot range',required=True,
            vmin=1, vmax = scripts.cnt_shots())
    if _range is None:
        return None
    return ( _range[0]-1, _range[1]-1 )

def on_cut():
    global clipboard
    SE = get_range()
    if SE is None:
        return
    (S,E) = SE
    clipboard = scripts.delete_shots(S,E)
    ix = S -1
    if ix < 0:
        ix = 0
    display.edit_shot(ix)

def on_copy():
    global clipboard
    SE = get_range()
    if SE is None:
        return
    (S,E) = SE
    clipboard = scripts.copy_shots(S,E)

def on_paste():
    # Convention: no "ui.add_after" value means insert at end
    # Also: ui uses a 1 based enumeration, but we use a 0-based. 
    ixafter = ui.add_after_seq.get('after',required=True,
            vmin=0,vmax=scripts.cnt_shots())
    if ixafter is None:
        return
    ixafter -= 1
    seq = []
    for s in clipboard:
        seq.append(s.clone())
    scripts.extend_script(seq,ixafter)
    display.edit_shot(ixafter+1)

def cp_item(func):
    SE = get_range()
    if SE is None:
        return
    (S,E) = SE
    for ix in range(S,E+1):
        func(scripts.get_shot(ix))

def on_cp_ani():
    def cp(shot):
        cl = display.sel_ani.clone()
        shot.anis.append(cl)

    cp_item(cp)
    display.make_cntrls()
    uiglobals.validate_view()

def on_cp_bg():
    def cp(shot):
        shot.bgspec = display.shot.bgspec

    cp_item(cp)
    uiglobals.validate_view()

def on_cp_lighting():
    lighting = display.shot.fxdct.get('lighting')
    def cp(shot):
        if lighting is None:
            try:
                del shot.fxdct['lighting']
            except:
                pass
        else:
            shot.fxdct['lighting'] = lighting.copy()

    cp_item(cp)
    uiglobals.validate_view()

def get_ui():

    return [
        [
            Range('seq_range',6,_format='%d',label='Shot Range:'),
        ],
        Hr(),
        Label('Shots:'),
        [
            Button('Cut',on_cut),
            Button('Copy',on_copy),
            Button('Paste',on_paste),
            NumEntry('add_after_seq',4,_format='%d',label='after')
        ],
        Hr(),
        Label('Copy and Paste:'),
        [
            Button('Selected Sprite',on_cp_ani),
        ],
        [
            Button('Current Background',on_cp_bg),
        ],
        [
            Button('Current Lighting',on_cp_lighting),
        ],
    ]

