# Copyright Al Cramer 2021 ac2.71828@gmail.com
import scripts
from scripts import Ani,Pt,Shot
import images
import resources as res
from tkalt import *
from colors import *
import animate

import uiglobals
import display

def validate_view():
    ui.panel_split.set(False)
    ui.panel_trim.set(False)
    ui.animation_msg.peer['text'] = 'Animation:'
    ui.shot_bgcolor.set_default('#ffffff')
    ui.add_blank_color.set_default('#ffffff')

    ui.cb_fade.set(False)
    ui.fx_secs.set(None)
    ui.add_after.set(None)
    ui.add_blank_after.set(None)
    ui.shot_secs.set(None)
    ui.play_speed.set(None)
    ui.scale_factor.set(None)
    ui.ptoolspr_show.set('hide')

    # disable all
    disable_widget(ui.shots)
    if not scripts.script_open():
        return

    if scripts.cnt_shots() == 0:
        ui.play_range.set(None)
        # enable add-shot items
        enable_widget(add_shots, add_blank_shot,
            ui.add_after,
            ui.panel_split, ui.panel_trim,
            ui.add_blank_after, ui.add_blank_color)
    else:
        # enable all & disable specific items as needed
        enable_widget(ui.shots)
        scr = scripts.script
        shot = display.shot
        ix = 1 + display.ixshot
        ui.add_after.set(ix)
        ui.add_blank_after.set(ix)
        _range = ui.play_range.get('Play range',required=False)
        if _range is None or _range[0] != ix:
            ui.play_range.set((ix,ix))
        else:
            _max = scripts.cnt_shots()
            if _range[1] > _max:
                ui.play_range.set((ix,_max))
        ui.shot_secs.set(shot.secs)
        if shot.has_bg_clip():
            disable_widget(ui.shot_secs,set_shot_secs)
        ui.scale_factor.set_default(1.0)
        ui.play_speed.set_default(1.0)
        ui.playall_speed.set_default(1.0)
        if shot.has_bg_illo():
            disable_widget(set_shot_bgcolor,ui.shot_bgcolor)
        else:
            disable_widget(set_shot_bgimg)
            ui.shot_bgcolor.set(shot.bgspec)
            ui.add_blank_color.set(shot.bgspec)
        (minutes,secs) = scr.get_time()
        if minutes == 0:
            msg = 'Animation: (%.1f secs)' % secs
        else:
            msg = 'Animation: (%d min %.1f secs)' % (minutes,secs)
        ui.animation_msg.peer['text'] = msg
        fadespec = shot.fxdct.get('fade')
        if fadespec is not None:
            secs = fadespec['secs'] 
            ui.cb_fade.set(True)
            ui.fx_secs.set(secs)
            enable_widget(ui.fx_secs)
        disable_widget(ui.ptoolspr_show,on_ptoolspr_apply,on_ptoolspr_undo)
        if (display.sel_ani is not None and 
            display.sel_ani.kind == 'spr'):
            enable_widget(ui.ptoolspr_show)
            cntrl = display.sel_pt.hand.cntrl
            if cntrl.persp_is_visible():
                ui.ptoolspr_show.set('show')
                enable_widget(on_ptoolspr_apply,on_ptoolspr_undo)



def set_shot_secs():
    v = ui.shot_secs.get()
    if v is not None:
        display.shot.set_secs(v)

def set_fx_secs():
    v = ui.fx_secs.get()
    if v is not None:
       display.shot.fxdct['fade'] = {'secs':v}

def on_cb_fade():
    if ui.cb_fade.get():
        ui.fx_secs.set_default(0.5)
        enable_widget(set_fx_secs,ui.fx_secs)
        set_fx_secs()
    else:
        ui.fx_secs.set(None)
        disable_widget(set_fx_secs,ui.fx_secs)
        del display.shot.fxdct['fade']

def scale_secs():
    factor = ui.scale_factor.get('Scale Secs')
    if factor is None:
        return
    scripts.scale_secs(factor)
    uiglobals.validate_view()

def clone_shot():
    ix = display.ixshot
    if ix != -1:
        scripts.clone_shot(ix)
        display.edit_shot(ix + 1)

def continue_shot():
    ix = display.ixshot
    if ix != -1:
        scripts.continue_shot(ix)
        display.edit_shot(ix + 1)

def on_play_shots_fini(state):
    if ui.cb_loop.get() and not state.halted:
        play_shots()
    else:
        # return to editing current shot
        display.edit_shot(display.ixshot)

def on_play_script_fini(state):
    if ui.cb_loop.get() and not state.halted:
        play_script()
    else:
        # return to editing current shot
        display.edit_shot(display.ixshot)

def play_shots():
    _range = ui.play_range.get('Play range',required=True,
            vmin=1, vmax = scripts.cnt_shots())
    if _range is None:
        return
    speed = ui.play_speed.get('Play')
    if speed is None:
        return
    # S,E should follow python style range conventions
    animate.animate_shots(display.can,
            _range[0]-1, _range[1], speed,on_play_shots_fini)

def play_script():
    N = scripts.cnt_shots()
    if N == 0:
        return
    speed = ui.playall_speed.get('speed')
    if speed is None:
        return
    # S,E should follow python style range conventions
    animate.animate_shots(display.can,
            0,N, speed,on_play_script_fini)

def halt_play():
    animate.halt_play()

def post_status(msg):
    ui.vid_status_msg.peer['text'] = msg
    ui.vid_status_msg.peer.update()


def create_video():
    N = scripts.cnt_shots()
    if N == 0:
        return
    animate.make_mp4(display.can,post_status)

def add_sprite():
    names = askopenfilenames( initialdir=scripts.proj_dir,
            filetypes=
            [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG')])

    if len(names) == 0:
        return
    # write image to project dir as needed; get fn roots
    roots = []
    for fn in names:
        res.install_image(fn)
        (head,root,ext) = res.parse_fp(fn)
        roots.append(root)
    ani = scripts.add_ani('spr',display.ixshot,fnlst=roots)
    # create initial point.  position is upper left corner
    # Must compute w,h attributes
    dim = int(.33 * min(display.w_img,display.h_img))
    ar = res.get_img_ar(roots[0])
    if ar <= 1.0:
        w = dim
        h = int(.5 + ar*w)
    else:
        h = dim
        w = int(.5 + h/ar)
    pt = Pt(w/2, h/2,0.0, 0.0,0.0,w,h)
    ani.add_pt(pt)
    # rebuild controls: new ani and pt become selected
    display.make_cntrls()
    display.set_selected(ani,pt)
    #uiglobals.validate_view()

def delete_sprite():
    if display.sprite_selected():
        scripts.delete_ani(display.ixshot,display.sel_ani)
        display.set_selected(None,None)
        display.make_cntrls()
        uiglobals.validate_view()

def set_shot_bgcolor():
    c = ui.shot_bgcolor.get('Background color')
    if c is None:
        return
    display.shot.bgspec = c
    uiglobals.validate_view()

def set_shot_bgimg():
    fp = askopenfilename( 
            initialdir=scripts.proj_dir, filetypes=
            [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG .mp4')])
    if fp is None:
        return
    if fp.lower().endswith('.mp4'):
        (installed,fnroot,w_dst,h_dst) = res.install_mp4(fp,None)
        if not installed:
            return
        bgspec = '%s.mp4' % fnroot
    else:
        bgspec = res.install_image(fp)
    display.shot.bgspec = bgspec
    display.edit_shot(display.ixshot)
    #uiglobals.validate_view()

def get_ixafter(entry):
    # Convention: no "ui.add_after" value means insert at end
    # Also: ui uses a 1 based enumeration, but we use a 0-based. 
    ix = scripts.cnt_shots()
    if not entry.is_empty():
        ix = entry.get('after',required=False,vmin=0,vmax=ix)
        if ix is None:
            return None
    return ix-1

def add_shots():
    fplst = askopenfilenames( 
            initialdir=scripts.proj_dir, filetypes=
            [('Image Files','.png .PNG .jpg .JPG .jpeg .JPEG .mp4')])
    if len(fplst) == 0:
        return

    ixafter = get_ixafter(ui.add_after)
    if ixafter is None:
        return

    split = ui.panel_split.get()
    trim = ui.panel_trim.get()

    imrecs = []
    for fp in fplst:
        if fp.endswith('.mp4'):
            (ok,root,w,h) = res.install_mp4(fp,post_status)
            if not ok:
                showerror('Could not read "%s.mp4"' % root) 
                continue
            imrecs.append(['%s.mp4' % root, w, h])
        else:
            (_dir,root,ext) = res.parse_fp(fp)
            images.write_panels(fp,root, scripts.proj_dir, imrecs,
                    dim=1200, split_pan=split, trim_mar=trim)
    shots = []
    for imr in imrecs:
        (fn,im_w,im_h) = imr
        cam = Ani('cam',0.0,1.0,1.0,4)
        ar_shot = res.ar_shot
        ar_img = im_h/float(im_w)
        if ar_shot > ar_img:
            cam_h = im_h
            cam_w = int(cam_h/ar_shot)
        else:
            cam_w = im_w
            cam_h = int(cam_w * ar_shot)
        xc = int(im_w/2)
        yc = int(im_h/2)
        cam.path.append(Pt(xc,yc, 0.0, 0.0,0.0,cam_w,cam_h))
        # Default shot length is 3 secs: for clips, must compute
        secs = 3.0
        if fn.endswith('.mp4'):
            secs = res.get_secs_clip(res.get_fpart(fn))
        shots.append(Shot(secs,fn,cam))

    # cleanup: if src file is "proj_dir/xxx.jpg" and we created
    # the file "proj_dir/xxx.png", delete the src file
    (head,root,ext) = res.parse_fp(fp)
    if len(imrecs) == 1 and ext.lower() in ['jpg','jpeg','tiff']:
        try:
            os.remove(fp)
        except:
            pass
    # rebuild the shot list, including the new shots
    scripts.extend_script(shots,ixafter)
    display.edit_shot(ixafter+1)

def add_blank_shot():
    ixafter = get_ixafter(ui.add_blank_after)
    if ixafter is None:
        return
    color = ui.add_blank_color.get('Color')
    if color is None:
        return
    scripts.add_blank_shot(ixafter,color)
    display.edit_shot(ixafter+1)

def on_ptoolspr_show_change():
    if display.sel_pt is not None:
        cntrl = display.sel_pt.cntrl
        if ui.ptoolspr_show.get() == 'show':
            cntrl.set_mode('persp')
        else:
            cntrl.set_mode('spr')
        validate_view()
        display.draw_edit()

def on_ptoolspr_apply():
    p = display.sel_pt
    cntrl = p.hand.cntrl
    if not cntrl.persp_is_visible():
        return
    (p.pxfrm_w,p.pxfrm_h,p.pxfrm_fall,p.h) =  cntrl.get_persp_params()
    ar = 1.0/p.pxfrm_w
    p.w = p.h/ar
    cntrl.set_mode('spr')
    validate_view()
    display.draw_edit()

def on_ptoolspr_undo():
    p = display.sel_pt
    # Need to undo the tweak to p.h made when we did the
    # perspective transform: see xfrm in xfrmpersp
    p.h *= ((1+p.pxfrm_h)/2)
    # reset aspect ratio to that of original image
    fnspr = p.ani.fnlst[0]
    if p.fnspr != '':
        fnspr = p.fnspr
    ar = res.get_img_ar(fnspr)
    p.w = p.h/ar
    # These parameters mean no-perspective xfrm.
    (p.pxfrm_w,p.pxfrm_h,p.pxfrm_fall) =  (1.0, 1.0, 0.0)
    # reset control mode
    cntrl = p.hand.cntrl
    if cntrl.persp_is_visible():
        cntrl.set_mode('spr')
    validate_view()
    display.draw_edit()
    

# make the ui
def get_ui():

    return [
        [
            Button('Add Shot',add_shots),
            NumEntry('add_after',4,_format='%d',label='after'),
            Checkbox('panel_split','split'),
            Checkbox('panel_trim','trim')
        ],
        [
            Button('Add Blank Shot',add_blank_shot),
            NumEntry('add_blank_after',4,_format='%d',label='after'),
            ColorControl('add_blank_color'),
        ],
        Hr(),
        Label("Current Shot:"),
        [
            Button('Continue', continue_shot),
            Button('Clone', clone_shot),
        ],
        [
            Button('Set Secs:', set_shot_secs),
            RangeParamControl('shot_secs',None,width=6,hb=None,incr=.25,
                onchange=set_shot_secs)

        ],
        [Button('Reset Bg. Image',set_shot_bgimg)],
        [
            Button('Reset Bg. Color:',set_shot_bgcolor),
            ColorControl('shot_bgcolor'),
        ],
        Label("Sprites:"),
        [
            Button('Add',add_sprite),
            Button('Delete',delete_sprite)
        ],
        Label('Perspective Tool'),
        [Radiobutton('ptoolspr_show','show','show',
            onchange=on_ptoolspr_show_change),
         Radiobutton('ptoolspr_show','hide','hide'),
         Button('Apply',on_ptoolspr_apply),
         Button('Undo',on_ptoolspr_undo),
        ],
        Label("FX:"),
        [
            Checkbox('cb_fade','Fade',
                onchange=on_cb_fade),
            Button('Set Secs:',set_fx_secs),
            RangeParamControl('fx_secs',None,width=6,hb=None,incr=.1,
                onchange=set_shot_secs)
        ],
        Hr(),
        Label("Animation:",id='animation_msg'),
        [
            Button('Scale Secs',scale_secs),
            NumEntry('scale_factor',6,_format='%.2f'),
        ],
        [
            Button('Play',play_shots),
            Range('play_range',6,_format='%d'),
            NumEntry('play_speed',6,_format='%.2f',label='speed:')
        ],
        [
            Button('Play All',play_script),
            NumEntry('playall_speed',6,_format='%.2f',label='speed:')
        ],
        [
            Checkbox('cb_loop','Loop'),
            Button('Halt',halt_play),
        ],
        Button('Create Video',create_video),
        Label('',id='vid_status_msg')
        ]
