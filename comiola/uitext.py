# Copyright Al Cramer 2021 ac2.71828@gmail.com
import scripts
from scripts import TextEl,Pt
from tkalt import *
from colors import *
from resources import get_res

import uiglobals
import display

def get_sel_te():
    # get text-element for currently selected pt
    if display.sel_ani is not None and display.sel_ani.kind == 'txt':
        if display.sel_pt is not None:
            return display.sel_ani.te

def validate_view():
    if not scripts.script_open() or scripts.cnt_shots() == 0:
        disable_widget(ui.text)
        return
    enable_widget(ui.text)
    te = get_sel_te()
    if te is None:
        ui.fontcolor.set_default('#000000')
        ui.fontname.set('comics')
        ui.fontsize.set('24pt')
        ui.colorbg.set_default('#ffffff')
        ui.textbox.set('')
        disable_widget(delete_te,delete_bg)
    else:
        ui.fontcolor.set(te.fontcolor)
        ui.fontname.set(te.fontname)
        ui.fontsize.set(te.fontsize)
        ui.textbox.set(te.get_text())
        enable_widget(delete_te,delete_bg)

def add_te(text):
    # create a new txtAni and select. A text element with no text
    # is legal: we make one when users clicks on a word balloon
    # and no te element is selected (they can then drop a sprite 
    # on top of the balloon).
    fontcolor = '#000000'
    if text != '':
        fontcolor = ui.fontcolor.get('Text color')
        if fontcolor is None:
            return
    te = TextEl(ui.fontname.get(),ui.fontsize.get(),fontcolor)
    te.set_text(text)
    ani = scripts.add_ani('txt',display.ixshot,te=te)
    pt = Pt( .5 * display.w_img, .333 * display.h_img)
    ani.add_pt(pt)
    display.make_cntrls()
    display.set_selected(ani,pt)
    return te

def text_apply():
    if display.ixshot == -1:
        return
    text = ui.textbox.get().strip()
    te = get_sel_te()
    if te is None:
        if text == '':
            return
        add_te(text)
    else:
        # apply to selected
        color = ui.fontcolor.get('Color')
        if color is None:
            return
        te.fontname = ui.fontname.get()
        te.fontsize = ui.fontsize.get()
        te.fontcolor = color
        te.set_text(text)
    uiglobals.validate_view()

def on_bal_select(fn):
    te = get_sel_te()
    if te is None:
        te = add_te('')
    te.bgspec = fn
    uiglobals.validate_view()

def on_colorbg():
    te = get_sel_te()
    if te is None:
        te = add_te('')
    c = ui.colorbg.get('Background color')
    if c is None:
        return
    te.bgspec = c
    uiglobals.validate_view()


def delete_te():
    te = get_sel_te()
    if te is not None:
        scripts.delete_ani(display.ixshot,display.sel_ani)
        display.make_cntrls()
        display.deselect_all()

def delete_bg():
    te = get_sel_te()
    if te is not None:
        if te.get_text() == '':
            # delete the te
            delete_te()
        else:
            # just delete bg.
            te.bgspec = 'null'
            uiglobals.validate_view()

def get_ui():
    return [
        Textbox('textbox',30,10),
        [
            Label('Font:'),
            OptionMenu('fontname',
                ('comics','serif','sans-serif'),
            ),
            OptionMenu('fontsize',
                ('12pt','16pt','24pt','32pt','48pt'),
            )
        ],
        [
            Label('Color:'),
            ColorControl('fontcolor'),
            Button('Apply',text_apply)
        ],
        Label('Background:'),
        [
            ImageButton(get_res('lspeach1.s'),lambda: on_bal_select('lspeach1')),
            ImageButton(get_res('lspeach2.s'),lambda: on_bal_select('lspeach2')),
            ImageButton(get_res('lthought1.s'),lambda: on_bal_select('lthought1')),
            ImageButton(get_res('lthought2.s'),lambda: on_bal_select('lthought2')) 
        ],
        [
            ImageButton(get_res('rspeach1.s'),lambda: on_bal_select('rspeach1')),
            ImageButton(get_res('rspeach2.s'),lambda: on_bal_select('rspeach2')),
            ImageButton(get_res('rthought1.s'),lambda: on_bal_select('rthought1')),
            ImageButton(get_res('rthought2.s'),lambda: on_bal_select('rthought2')) 
        ],
        [
            ImageButton(get_res('colorbg'),on_colorbg),
            ColorControl('colorbg'), 
            Button('Delete',delete_bg)
        ],
        [Hr()],
        Button('Delete Selected',delete_te)
    ]

