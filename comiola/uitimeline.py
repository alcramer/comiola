# Copyright Al Cramer 2021 ac2.71828@gmail.com
from colors import *

import uiglobals
import display

# our canvas, and its width
can = None
wcan = 10

# dimension of control handle
dim = 12

def to_xtl(v):
    return int(v*(wcan-dim))

def from_xtl(xtl):
    return float(xtl)/(wcan-dim)

class Handle:
    def __init__(self,v,color,is_selected):
        self.xtl = to_xtl(v)
        if is_selected:
            self.tkid = can.create_oval(
                self.xtl, 2, self.xtl+dim, 2+ dim, fill=color,outline='#ff0000')
        else:
            self.tkid = can.create_rectangle(
                self.xtl, 2, self.xtl+dim, 2+ dim, fill=color,outline='#ff0000')


# the handles
handles = []

# selected handle
sel_h = None

def validate_view():
    global wcan,handles
    can.delete('all')
    handles = []
    can.update()
    wcan = can.winfo_width()
    ani = display.sel_ani
    if ani is None:
        return
    if len(ani.path) == 1:
        # in general, there's no timeline. Exception is
        # 1-pt sprite animation with multiple cells
        if not (ani.kind =='spr' and len(ani.fnlst) > 1):
                return
    yline = int(dim/2)
    can.create_line(0,yline,wcan,yline,fill='#808080')
    path = ani.path
    # number of handles needed. In general there's one handle
    # for each path point; but if only 1 pt, we need 2
    # (this is the pure cell animation case).
    nhandles = 2 if len(path) == 1 else len(path)
    # first handle: tS for animation
    handles.append(Handle(display.sel_ani.tS,
        col_interpolate('red',0,nhandles),
        display.sel_pt == path[0] ))
    # handles showing param values
    for i in range(1,len(path)-1):
        handles.append(Handle(path[i].param,
            col_interpolate('red',i,nhandles),
            display.sel_pt == path[i] ))
    # last handle: tE for animation
    ix =  nhandles - 1
    if len(path) == 1:
        is_selected = True
    else:
        is_selected = display.sel_pt == path[ix]
    handles.append(Handle(display.sel_ani.tE,
        col_interpolate('red',ix,nhandles),
        is_selected ))


def on_mousedown(ev):
    global sel_h
    sel_h = None
    for h in handles:
        if ev.x >= h.xtl and ev.x <= h.xtl + dim:
            sel_h = h
            sel_h.xtl_dragS = sel_h.xtl
            sel_h.md_xtl_dragS = ev.x

def on_mouseup(ev):
    global sel_h
    sel_h = None
    if display.sel_ani is not None:
        uiglobals.validate_view()

def on_mousemove(ev):
    if sel_h is None:
        return
    dx = ev.x - sel_h.md_xtl_dragS
    xnew = sel_h.xtl_dragS + dx
    if xnew < 0 or xnew > wcan - dim:
        # out of range
        return
    # can't cross neighbors
    sel_ix = handles.index(sel_h)
    ix = sel_ix-1
    if ix >= 0:
        if xnew < handles[ix].xtl + dim:
            return
    ix = sel_ix+1
    if ix < len(handles):
        if xnew + dim > handles[ix].xtl:
            return
    # accept the move: tweak handle position
    sel_h.xtl = xnew
    can.coords(sel_h.tkid,xnew,2,xnew+dim,2+dim)
    # reset corresponding parameter
    ani = display.sel_ani
    if sel_ix == 0:
        ani.tS = from_xtl(sel_h.xtl)
    elif sel_ix == len(handles)-1:
        ani.tE = from_xtl(sel_h.xtl)
    else:
        # interior path pt: adjust param
        ani.path[sel_ix].param = from_xtl(sel_h.xtl)
    # TODO: remove
    # checking tS ot tE may change the cycles for the ani:
    # reset as needed
    # ani.cycles = display.shot.get_ani_cycles(ani)
