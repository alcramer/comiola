from PIL import Image,ImageTk, ImageOps
import math

# This module implements perspective transforms.
# The math behind the xfrm is described in the image file
# "xfrmpersp.gif". That slide describes a mapping from points in
# a source image, to points in the destination image.
#
# Naively implememting that mapping gives poor results (missing
# pixels in the dst image) because points are real values and
# pixel addresses are integers (so round-off issues cause trouble).
# The implementation used here is:
# 1. compute the width & height of the dst image.
# 2. For each pixel address in the dst image, find the corresponding
#    address in the source image, then transfer the pixel from source
#    to dst.
# This works well, but the reverse mapping (dst address -> src address)
# hits a singularity when left-to-right perspective shrinkage 
# approaches 1. So we catch these cases and use a simple isometric transform.
#
# Note that the xfrm described in "xfrmpersp.gif' is just one of many ways
# to compute perspective transforms. Other ways are possible but all
# involve some distortion: this one seems the best compromise.

def get_dst_coords(w,h,fall,x,y):
    if h >= .95:
        # use isometric perspective
        return (w*x, y+x*fall)
    B = (2*w)/(h+1.0)
    A = w - B
    x_ = A*x*x + B*x

    sc = 1.0 + (h-1)*(x_/w)
    y_ = y*sc + fall*(x_/w)
    return (x_,y_)

def get_src_coords(w,h,fall,x_,y_):
    if h >= .95:
        # use isometric perspective
        x = x_/w
        y = y_ - x*fall
        if y < 0 or y > 1.0:
            return None
        else:
            return (x,y)
    # compute x
    B = (2*w)/(h+1.0)
    A = w - B
    # Relation is: A*x*x + B*x + (-x_) = 0
    # So set C to -x_ and use quadratic formula A*x*x + B*x + C = 0
    C = -x_
    term = B*B - 4*A*C
    if term < 0:
        return None
    x = (-B + math.sqrt(term))/(2*A)

    # compute y
    sc = 1.0 + (h-1)*(x_/w)
    y = (y_ - fall*(x_/w))/sc

    if ( x<0.0 or x>1.0 or y<0.0 or y>1.0):
        return None
    
    return (x,y)

def scale_duple(v,sc0,sc1):
    return (int(v[0]*sc0), int(v[1]*sc1))

# Perspective transform for Comiola. For documentation of the 
# parameters, see the slide "xfrmpersp.gif".
def xfrm(img_src,w,h,fall,dim):
    if h > 1:
        # flip src
        img_src = ImageOps.mirror(img_src)
        # transform, renormalizing params
        img = xfrm(img_src,w/h,1.0/h,-fall/h,dim)
        # flip back
        img = ImageOps.mirror(img)
        return img

    img_src = img_src.convert("RGBA")
    (wsrc,hsrc) = img_src.size

    # get (x,y) in dst coord's for top-right and botton-right pts: 
    # needed to create the dst image to draw upon

    (topx,topy) = scale_duple(get_dst_coords(w,h,fall, 1.0,0),wsrc,hsrc)
    (botx,boty) = scale_duple(get_dst_coords(w,h,fall, 1.0, 1.0),wsrc,hsrc)

    # set w of dst image
    w_img_dst = w*wsrc

    # get range of ydst and the h of dst image
    ydst_min = min(0,topy)
    ydst_max = max(hsrc,boty)

    h_img_dst = ydst_max - ydst_min

    ycorrect = 0
    if ydst_min < 0:
        ycorrect = -ydst_min

    # allocate dst image
    w_img_dst = int(w_img_dst)
    h_img_dst = int(h_img_dst)
    img_dst = Image.new("RGBA",(w_img_dst,h_img_dst), (255,255,255,0))

    #print('wsrc:%d hsrc:%d' % (wsrc,hsrc))
    #print('w_img_dst:%d h_img_dst:%d' % (w_img_dst,h_img_dst))

    # load pixels
    pix_src = img_src.load()
    pix_dst = img_dst.load()
    # walk the dst pixels, replacing with pixels from src
    for x in range(0,w_img_dst):
        xnorm = float(x)/wsrc
        for y in range(ydst_min,ydst_max):
            ynorm = float(y)/hsrc
            XYsrc = get_src_coords(w,h,fall,xnorm,ynorm)
            if XYsrc is not None:
                (xsrc, ysrc) = scale_duple(XYsrc,wsrc,hsrc)
                #print('xsrc: %d ysrc: %d' % (xsrc,ysrc))
                try:
                    pix_dst[x,y+ycorrect] = pix_src[xsrc,ysrc]
                    pass
                except IndexError:
                    pass

    # scale image dst so height matches src.
    scale = dim/float(h_img_dst)
    # without this tweak, the image appears to shrink too much when we
    # apply the transform.
    scale /= ((1+h)/2)
    # end try code
    img_dst = img_dst.resize(
            (int(scale*w_img_dst), int(scale*h_img_dst)), Image.ANTIALIAS)
    return img_dst

def ut():
    img_src = Image.open('res/box.png')
    w = .5
    h = .5
    fall = .0
    img = xfrm(img_src,w,h,fall) 
    img.show()

if __name__ == '__main__':
    ut()
