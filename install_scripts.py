import subprocess
import shutil
import sys

# Install comiola scripts on users's machine. If you run this script
# from your home directory, it will create a sub-directory named
# "comiola". To start the app, enter  
# "py comiola" (windows command prompt)
# or 
# "python3 comiola" (mac terminal command)

# pip-install required packages
subprocess.run(['pip3','install', 'Pillow'])
subprocess.run(['pip3','install', 'imageio'])
subprocess.run(['pip3','install', 'imageio-ffmpeg'])

# clone the scripts from bitbucket
try:
    shutil.rmtree('./comiola')
except:
    pass
subprocess.run(['git','clone',
    'https://alcramer@bitbucket.org/alcramer/comiola.git'])

# move the clone to "comiola.git", then copy its "comiola" subdirectory
# into this directory.
shutil.move('./comiola','./comiola_git')
shutil.copytree('./comiola_git/comiola', './comiola')

# remove "comiola_git". We should be able to use "shutil.rmtree",
# but this fails on windows, so we make a system call.
platform = sys.platform.lower()
if platform in ['darwin','linux']:
    subprocess.run('rm -fr comiola_git',shell=True)
else:
    subprocess.run('rmdir comiola_git /s /q',shell=True)




